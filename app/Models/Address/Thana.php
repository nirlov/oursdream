<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;

class Thana extends Model
{
    protected $table = 'upazilas';

    public function city()
    {
        return $this->belongsTo('App\Models\Address\District', 'district_id');
    }
}
