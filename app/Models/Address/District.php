<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    public function thana()
    {
        return $this->hasMany('App\Models\Address\Thana', 'district_id');
    }
}
