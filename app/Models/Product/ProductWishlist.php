<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductWishlist extends Model
{
    protected $table = 'product_wishlists';

    public function productId()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }

    public function userId()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
