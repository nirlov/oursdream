<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $table = 'product_reviews';

    public function productId()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }

    public function userId()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
