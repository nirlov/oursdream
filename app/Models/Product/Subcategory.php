<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategories';

    public function categoryId()
    {
        return $this->belongsTo('App\Models\Product\Category', 'category_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Admin', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Admin', 'updated_by');
    }
}
