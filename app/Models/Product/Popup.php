<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    protected $table = 'popups';
}
