<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function categoryId()
    {
        return $this->belongsTo('App\Models\Product\Category', 'category_id');
    }

    public function subcategoryId()
    {
        return $this->belongsTo('App\Models\Product\Subcategory', 'subcategory_id');
    }

    public function discountId()
    {
        return $this->belongsTo('App\Models\Product\Discount', 'discount_id');
    }

    public function colorId()
    {
        return $this->belongsTo('App\Models\Product\Color', 'color_id');
    }

    public function sizeId()
    {
        return $this->belongsTo('App\Models\Product\Size', 'size_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Admin', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Admin', 'updated_by');
    }
}
