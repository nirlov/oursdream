<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';

    public function createdBy()
    {
        return $this->belongsTo('App\Admin', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Admin', 'updated_by');
    }
}
