<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function userId()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
