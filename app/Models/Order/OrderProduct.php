<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_products';

    public function productId()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }

}
