<?php

namespace App\Models\Charge;

use Illuminate\Database\Eloquent\Model;

class ShippingCharge extends Model
{
    protected $table = 'shipping_charges';
}
