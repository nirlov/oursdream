<?php

namespace App;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Auth;

//Model
use App\Admin;
use App\User;
use App\Models\Order\Order;

class AdminInformation
{
    
    public function getInfo()
    {
    	$admin = Auth::user();
    	return $admin;
    }

    public function notification()
    {
    	$notification = Order::where('notification' , 0)->get();
    	return $notification;
    }

    public function customer()
    {
    	$date = date('Y-m-d');
    	$customer = User::whereDate('created_at' ,$date)->get();
    	return $customer;
    }

}