<?php

namespace App\Modules\Userpanel\Http\Controllers\OtherController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WebController extends Controller
{

    public function index()
    {
        return view('userpanel::other.about_us');
    }

    
    public function contact()
    {
        return view('userpanel::other.contact_us');
    }

    
    public function comming()
    {
        return view('userpanel::other.comming_soon');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
