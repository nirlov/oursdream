<?php

namespace App\Modules\Userpanel\Http\Controllers\ProductController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Product\Product;
use App\Models\Product\ProductReview;

use Session;

class WebController extends Controller
{
    
    public function index()
    {
        //
    }

    public function allProduct()
    {
        $product =product::paginate(20);
        $category = Category::all();
        $feature = Product::where('featured' , 1)->latest()->take(3)->get();

        foreach($product as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        foreach($feature as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        return view('userpanel::product.all_product' , compact('product' , 'category' , 'feature'));
    }

    public function subcategory($id)
    {
        $product = Product::where('subcategory_id' , $id)->paginate(20);
        $subcategory = Subcategory::find($id);
        $category = Category::all();
        $feature = Product::where('featured' , 1)->latest()->take(3)->get();

        foreach($product as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        foreach($feature as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        return view('userpanel::product.subcategory_product' , compact('product' , 'subcategory' , 'category' , 'feature'));
    }

    public function category($id)
    {
        $product = Product::where('category_id' , $id)->paginate(20);
        $category = Category::find($id);
        $category_all = Category::all();
        $feature = Product::where('featured' , 1)->latest()->take(3)->get();

        foreach($product as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        foreach($feature as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }
        
        return view('userpanel::product.category_product' , compact('product' , 'category_all' , 'category' , 'feature'));
    }

    public function cartAdd(Request $request,$id)
    {
        $cart_product=Session('cart_product');
        $cart_product[]=$id;
        Session::put('cart_product',$cart_product);

        return back()->with('alert.message' , 'Cart Product Successfully');
    }

    public function cartRemove()
    {
        Session::forget('cart_product');

        return back()->with('alert.message' , 'All Cart Product Remove Successfully');
    }
   
    public function create()
    {
        $user = Auth::user()->id;

        $product = ProductReview::where('user_id' , $user)->get();

        return view('userpanel::account.review' , compact('product'));
    }
   
    public function store(Request $request)
    {
        //
    }
    
    public function show($id)
    {
        $product = Product::find($id);
        $product_review = ProductReview::where('product_id' , $id)->get();
        
        $category = Product::where('category_id' , $product->category_id)->take(5)->get();
        $subcategory = Product::where('subcategory_id' , $product->subcategory_id)->latest()->take(3)->get();

        if($product->discountId != Null){
            $discount           = $product->discountId->amount;
            $discount_price     = (integer)(($product->sales_price * $discount)/100);
            $new_price          = ($product->sales_price - $discount_price);

            $product->discount_price = $new_price;
        }

        foreach($category as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        foreach($subcategory as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        return view('userpanel::product.product_details' , compact('product' , 'category' , 'subcategory' , 'product_review'));
    }
  
    public function edit($id)
    {
        //
    }
    
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
            'summary' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new ProductReview;

        $insert->summary            = $input['summary'];
        $insert->review             = $input['review'];
        $insert->rating             = $input['rating'];
        $insert->product_id         = $id;
        $insert->user_id            = $user;

        $insert->save();

        $review = (integer)(ProductReview::where('product_id' , $id)->avg('rating'));

        $product = Product::find($id);
        $product->rating = $review;
        $product->update();

        return Redirect::route('product_details',$id)->with('alert.message' , 'Product review successful');
    }

    public function destroy($id)
    {
        //
    }
}
