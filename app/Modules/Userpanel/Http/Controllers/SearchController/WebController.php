<?php

namespace App\Modules\Userpanel\Http\Controllers\SearchController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Models
use App\Models\Product\Product;
use App\Models\Product\Category;

class WebController extends Controller
{
    public function index($search)
    {
        $product = Product::where('name', 'LIKE', '%'.$search.'%')->get();

        return Response::json($product);
    }

    public function create(Request $request)
    {
        $search = $request->search;
        $product = Product::where('name', 'LIKE', '%'.$search.'%')->paginate(20);
        $category = Category::all();
        $feature = Product::where('featured' , 1)->latest()->take(3)->get();

        foreach($product as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        foreach($feature as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        return view('userpanel::product.search_product' , compact('product' , 'category' , 'feature'));
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
