<?php

namespace App\Modules\Userpanel\Http\Controllers\HomeController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Models
use App\Models\Product\Subcategory;
use App\Models\Product\Product;
use App\Models\Product\Slider;
use App\Models\Blog\Blog;
use App\Models\Product\Popup;
use App\Models\Profile\Profile;

class WebController extends Controller
{

    public function index()
    {
        $subcategory = Subcategory::take(3)->get();
        $product = Product::latest()->take(6)->get();
        $blog = Blog::take(3)->get();
        $popup = Popup::first();
        $profile = Profile::first();

        foreach($product as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        $slider_1 = Slider::find(1);
        $slider_2 = Slider::find(2);
        $new = Product::where('new' , 1)->latest()->take(6)->get();

        foreach($new as $all){
            if($all->discountId != Null){
                $discount           = $all->discountId->amount;
                $discount_price     = (integer)(($all->sales_price * $discount)/100);
                $new_price          = ($all->sales_price - $discount_price);

                $all->discount_price = $new_price;
            }
            
        }

        return view('userpanel::home.home' , compact('subcategory' , 'product' , 'slider_1' , 'slider_2' , 'new' , 'blog' , 'popup' , 'profile'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
