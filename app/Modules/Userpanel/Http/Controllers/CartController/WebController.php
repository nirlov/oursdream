<?php

namespace App\Modules\Userpanel\Http\Controllers\CartController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Product\Product;

class WebController extends Controller
{
    
    public function index()
    {
        
        if(session('cart_product')){
            $cart_product = Session('cart_product'); 
            $total_cart_product = Session('cart_product'); 
            $cart_product = array_unique($cart_product);
            sort($cart_product);
            $product=[];

            foreach($cart_product as $all)
            {
                $quantity = count(array_keys($total_cart_product, $all));
                $products = Product::find($all);

                if($products->discountId != Null){
                    $discount           = $products->discountId->amount;
                    $discount_price     = (integer)(($products->sales_price * $discount)/100);
                    $new_price          = ($products->sales_price - $discount_price);
                }
                else{
                    $new_price          = $products->sales_price;
                }

                $products->unit_price   = $new_price;
                $products->quantity     = $quantity;
                $products->sub_total    = ($new_price * $quantity);
                $product[]              = $products;
            }

            $sum = array_sum(array_column($product, 'sub_total'));

            $extra = Product::all()->random(4);

            foreach($extra as $all){
                if($all->discountId != Null){
                    $discount           = $all->discountId->amount;
                    $discount_price     = (integer)(($all->sales_price * $discount)/100);
                    $new_price          = ($all->sales_price - $discount_price);

                    $all->discount_price = $new_price;
                }
                
            }

            return view('userpanel::product.cart_product' , compact('product' , 'sum' , 'extra'));
        }
        else{
            return view('userpanel::product.cart_product')->with('alert.message' , 'No Product Cart');
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {
        $array = Input::all();

        Session::forget('cart_product');

        $cart_product=Session('cart_product');

        for($i=0; $i<count($array['id']);$i++){
            for($j=0; $j<$array['quantity'][$i]; $j++){
                $cart_product[]=$array['id'][$i];
                Session::put('cart_product',$cart_product);
            }
        }

        return back()->with('alert.message' , 'Cart Product Updated Successfully');
    }

    public function destroy($id)
    {
        $cart_product=Session('cart_product');
        foreach ($cart_product as $key => $value) {
            if($cart_product[$key]==$id){
                unset($cart_product[$key]);
            }
        }
        if(!empty($cart_product)){
            Session::put('cart_product',$cart_product);
        }
        else{
            Session::forget('cart_product');
        }
        
        return back()->with('alert.message' , 'Cart Product Removed Successfully');
    }
}
