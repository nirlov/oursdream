<?php

namespace App\Modules\Userpanel\Http\Controllers\CheckoutController;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Product\Product;
use App\Models\Address\District;
use App\Models\Address\Thana;
use App\Models\Charge\Vat;
use App\Models\Charge\ShippingCharge;
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session('cart_product')){
            $cart_product = Session('cart_product'); 
            $total_cart_product = Session('cart_product'); 
            $cart_product = array_unique($cart_product);
            sort($cart_product);
            $product=[];

            foreach($cart_product as $all)
            {
                $quantity = count(array_keys($total_cart_product, $all));
                $products = Product::find($all);

                if($products->discountId != Null){
                    $discount           = $products->discountId->amount;
                    $discount_price     = (integer)(($products->sales_price * $discount)/100);
                    $new_price          = ($products->sales_price - $discount_price);
                }
                else{
                    $new_price          = $products->sales_price;
                }

                $products->unit_price   = $new_price;
                $products->quantity     = $quantity;
                $products->sub_total    = ($new_price * $quantity);
                $product[]              = $products;
            }

            $sum = array_sum(array_column($product, 'sub_total'));

            $user = Auth::user();
            $city = District::all();
            $thana = Thana::all();
            
            //Vat
            $vat = Vat::first();
            $total_vat = (integer)(($sum * $vat->vat)/100);

            //Shipping Charge
            $shipping = ShippingCharge::first();
            if($user->city == 1){
                $shipping_charge = $shipping->in_dhaka;
            }
            else{
                $shipping_charge = $shipping->out_dhaka;
            }

            //Grand Total
            $grand_total = (integer)($sum + $total_vat + $shipping_charge);

            //dd($total_vat);

            return view('userpanel::product.checkout' , compact('product' , 'sum' , 'user' , 'city' , 'thana' , 'vat', 'total_vat' , 'shipping_charge' , 'grand_total'));
        }
        else{
            return view('userpanel::product.checkout')->with('alert.message' , 'No Product Cart');
        }
    }

    public function thana($id)
    {
        $thana = Thana::where('district_id', $id)->get();
        return Response::json($thana);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required',
            'address'   => 'required',
            'district'  => 'required',
            'thana'     => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //Input
        $data = $request->all();

        //Cart Product

        if(session('cart_product')){
            $cart_product = Session('cart_product'); 
            $total_cart_product = Session('cart_product'); 
            $cart_product = array_unique($cart_product);
            sort($cart_product);
            $product=[];

            foreach($cart_product as $all)
            {
                $quantity = count(array_keys($total_cart_product, $all));
                $products = Product::find($all);

                if($products->discountId != Null){
                    $discount           = $products->discountId->amount;
                    $discount_price     = (integer)(($products->sales_price * $discount)/100);
                    $new_price          = ($products->sales_price - $discount_price);
                }
                else{
                    $new_price          = $products->sales_price;
                }

                $products->unit_price   = $new_price;
                $products->quantity     = $quantity;
                $products->sub_total    = ($new_price * $quantity);
                $product[]              = $products;
            }

            $sum = array_sum(array_column($product, 'sub_total'));

            $user = Auth::user();
            
            //Vat
            $vat = Vat::first();
            $total_vat = (integer)(($sum * $vat->vat)/100);

            //Shipping Charge
            $shipping = ShippingCharge::first();
            if($data['district'] == 1){
                $shipping_charge = $shipping->in_dhaka;
            }
            else{
                $shipping_charge = $shipping->out_dhaka;
            }

            //Grand Total
            $grand_total = (integer)($sum + $total_vat + $shipping_charge);
        }

        

        //User Update

        $user->phone            = $data['phone'];
        $user->address          = $data['address'];
        $user->city             = $data['district'];
        $user->thana            = $data['thana'];
        $user->zip_code         = $data['zip_code'];
        $user->country          = $data['country'];
        $user->update();

        //Order
        $orders = Order::count('id');

        $order = new Order;

        if($orders == 0){
            $order->code            = 'SO-1000000';
        }
        else{
            $order->code            = 'SO-'.(1000000 + Order::all()->last()->id);
        }
        $order->status              = 0;
        $order->total_amount        = $grand_total;
        $order->shipping_charge     = $shipping_charge;
        $order->vat                 = $total_vat;
        $order->user_id             = $user->id;

        //Order Product
        if($order->save()){

            foreach($product as $all)
            {
                $order_product = new OrderProduct;

                $order_product->quantity        = $all->quantity;
                $order_product->amount          = $all->unit_price;
                $order_product->product_id      = $all->id;
                $order_product->order_id        = $order->id;
                $order_product->save();
            }
            
        Session::forget('cart_product');
        }
    
    return back()->with('alert.message' , 'Order Placed Successfully');    

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
