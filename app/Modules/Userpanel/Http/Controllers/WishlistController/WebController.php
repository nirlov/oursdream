<?php

namespace App\Modules\Userpanel\Http\Controllers\WishlistController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Product;
use App\Models\Product\ProductWishlist;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request,$id)
    {

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new ProductWishlist;

        $insert->product_id       = $id;
        $insert->user_id          = $user;

        $insert->save();

        return back()->with('alert.message' , 'Wishlist Product Added Successful');
    }

    public function show()
    {
        $user = Auth::user()->id;

        $wishlist = ProductWishlist::where('user_id' , $user)->get();

        foreach($wishlist as $all){
            if($all->productId['discountId'] != Null){
                $discount           = $all->productId['discountId']->amount;
                $discount_price     = (integer)(($all->productId['sales_price'] * $discount)/100);
                $new_price          = ($all->productId['sales_price'] - $discount_price);

                $all->discount_price = $new_price;
            }
            else{
                $all->discount_price = $all->productId['sales_price'];
            }
            
        }

        return view('userpanel::account.wishlist' , compact('wishlist'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
