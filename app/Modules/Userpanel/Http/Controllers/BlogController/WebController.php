<?php

namespace App\Modules\Userpanel\Http\Controllers\BlogController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Models

use App\Models\Blog\Blog;
use App\Models\Blog\BlogCategory;

class WebController extends Controller
{

    public function index()
    {
        $blog = Blog::paginate(5);
        $blog_category = BlogCategory::all()->random(5);
        $recent_blog = Blog::take(2)->get();

        return view('userpanel::blog.blog' , compact('blog' , 'blog_category' , 'recent_blog'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $blog = Blog::find($id);
        $blog_category = BlogCategory::all()->random(5);
        $recent_blog = Blog::take(2)->get();
        
        return view('userpanel::blog.blog_details' , compact('blog' , 'blog_category' , 'recent_blog'));
    }

    public function category($id)
    {
        $blog = Blog::where('category_id' , $id)->paginate(5);
        $blog_category_details = BlogCategory::find($id);
        $blog_category = BlogCategory::all()->random(5);
        $recent_blog = Blog::take(2)->get();

        return view('userpanel::blog.blog_category_details' , compact('blog' , 'blog_category' , 'recent_blog' , 'blog_category_details'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
