<?php

namespace App\Modules\Userpanel\Http\Controllers\AccountController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\User;
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;
use App\Models\Address\District;
use App\Models\Address\Thana;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $city = District::all();
        $thana = Thana::all();

        return view('userpanel::account.my_account' , compact('user' , 'city' , 'thana'));
    }

    public function dashboard()
    {
        $customer = Auth::user();
        $total_order = Order::where('user_id' , $customer->id)->count();
        $confirme_order = Order::where([['user_id', $customer->id],['status' , 1]])->count();
        $pendding_order = Order::where([['user_id', $customer->id],['status' , 0]])->count();
        
        return view('userpanel::account.dashboard' , compact('customer' , 'total_order' , 'confirme_order' , 'pendding_order'));
    }

    public function order()
    {
        $customer = Auth::user();
        $total_order = Order::where('user_id' , $customer->id)->count();
        $confirme_order = Order::where([['user_id', $customer->id],['status' , 1]])->count();
        $pendding_order = Order::where([['user_id', $customer->id],['status' , 0]])->count();
        $order = Order::where('user_id', $customer->id)->get();
        
        return view('userpanel::account.order' , compact('customer' , 'total_order' , 'confirme_order' , 'pendding_order' , 'order'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $order = Order::find($id);
        $order_product = OrderProduct::where('order_id' , $id)->get();

        return view('userpanel::account.order_details' , compact('order' , 'order_product'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $update = User::find($id);

        $update->name           = $input['name'];
        $update->phone          = $input['phone'];
        $update->address        = $input['address'];
        $update->city           = $input['district'];
        $update->thana          = $input['thana'];
        $update->zip_code       = $input['zip_code'];

        $update->update();

        return back()->with('alert.message' , 'Updated Successfully');
    }

    public function destroy($id)
    {
        //
    }
}
