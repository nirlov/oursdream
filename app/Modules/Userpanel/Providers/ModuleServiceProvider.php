<?php

namespace App\Modules\Userpanel\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'userpanel');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'userpanel');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'userpanel');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
