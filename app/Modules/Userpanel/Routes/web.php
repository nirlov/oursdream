<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController\WebController@index')->name('home');

Route::get('/products' , 'ProductController\WebController@allProduct')->name('all_product');
Route::get('/product-details/{id}' , 'ProductController\WebController@show')->name('product_details');
Route::get('/product-review/{id}' , 'ProductController\WebController@update')->name('product_review')->middleware('auth');
Route::get('/my-product-review' , 'ProductController\WebController@create')->name('my_product_review')->middleware('auth');
Route::get('/subcategory-products/{id}' , 'ProductController\WebController@subcategory')->name('subcategory_product');
Route::get('/category-products/{id}' , 'ProductController\WebController@category')->name('category_product');

Route::get('/cart/product/{id}' , 'ProductController\WebController@cartAdd')->name('cart_product');
Route::get('/cart/remove' , 'ProductController\WebController@cartRemove')->name('cart_all_remove');

Route::get('/cart' , 'CartController\WebController@index')->name('cart_index');
Route::post('/cart/update' , 'CartController\WebController@update')->name('cart_update');
Route::get('/cart/delete/{id}' , 'CartController\WebController@destroy')->name('cart_destroy_product');

Route::get('/checkout' , 'CheckoutController\WebController@index')->name('checkout_index');
Route::post('/checkout/store' , 'CheckoutController\WebController@store')->name('checkout_store');

Route::get('/wishlist-products' , 'WishlistController\WebController@show')->name('wishlist_show');
Route::get('/wishlist/{id}' , 'WishlistController\WebController@store')->name('wishlist_store');

//Ajax-Route

Route::get('/ajax-thana/{id}', 'CheckoutController\WebController@thana')->name('thana');

//Others page

Route::get('/about-us' , 'OtherController\WebController@index')->name('about_index');
Route::get('/contact-us' , 'OtherController\WebController@contact')->name('contact_index');

//Comming Soon Page
Route::get('/comming-soon' , 'OtherController\WebController@comming')->name('comminig_soon');

//Blog
Route::get('/blog' , 'BlogController\WebController@index')->name('blog_index');
Route::get('/blog-details/{id}' , 'BlogController\WebController@show')->name('blog_details');
Route::get('/blog-category-details/{id}' , 'BlogController\WebController@category')->name('blog_category_details');

//Account
Route::get('/my-account' , 'AccountController\WebController@index')->name('my_account_index');
Route::post('/my-account/{id}' , 'AccountController\WebController@update')->name('my_account_update');
Route::get('/my-account/dashboard' , 'AccountController\WebController@dashboard')->name('my_account_dashboard_index');

//Order
Route::get('/my-account/order' , 'AccountController\WebController@order')->name('my_account_order_index');
Route::get('/my-account/order-details/{id}' , 'AccountController\WebController@show')->name('my_account_order_details');

//Search
Route::get('/ajax-product-search/{search}' , 'SearchController\WebController@index')->name('search_all_product');
Route::post('/product-search' , 'SearchController\WebController@create')->name('search_products_index');