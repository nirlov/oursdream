@extends('layouts.app')

@section('title' , 'My Product Review')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">My Product Review</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">My Product Review</h1>
                
                <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                    <div class="box-content">
                        <table class="search-table table" style="font-size: 15px;">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="15%">Product Name</th>
                                <th width="10%">Review</th>
                                <th width="10%">Rating</th>
                                <th width="10%">Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($product as $all)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $all->productId['name'] }}</td>
                                <td>{{ $all->review }}</td>
                                <td>
                                    <div class="product-ratings">
                                        <div class="ratings-box">
                                            @if($all->rating == 0)
                                            <div class="rating" style="width:0%"></div>
                                            @elseif($all->rating == 1)
                                            <div class="rating" style="width:20%"></div>
                                            @elseif($all->rating == 2)
                                            <div class="rating" style="width:40%"></div>
                                            @elseif($all->rating == 3)
                                            <div class="rating" style="width:60%"></div>
                                            @elseif($all->rating == 4)
                                            <div class="rating" style="width:80%"></div>
                                            @else
                                            <div class="rating" style="width:100%"></div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td> <img src="{{ asset('uploads/product/'.$all->productId['thumbnail']) }}" style="width: 100px; height: auto;"> </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>


        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#review").addClass( "active" );
    </script>
@endsection