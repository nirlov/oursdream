@extends('layouts.app')

@section('title' , 'My Wishlist')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">My Wishlist</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">My Wishlist</h1>
                
                <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                    <div class="box-content">
                        <table class="search-table table" style="font-size: 15px;">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="15%">Product Name</th>
                                <th width="10%">Code</th>
                                <th width="10%">Amount</th>
                                <th width="10%">Image</th>
                                <th width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($wishlist as $all)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $all->productId['name'] }}</td>
                                <td>{{ $all->productId['code'] }}</td>
                                <td>{{ $all->discount_price }}</td>
                                <td> <img src="{{ asset('uploads/product/'.$all->productId['thumbnail']) }}" style="width: 100px; height: auto;"> </td>
                                <td>
                                    <a href="{{ route('product_details' , $all->id) }}" target="_blank"><button type="button" class="btn btn-info btn-sm">View</button></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>


        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#wishlist").addClass( "active" );
    </script>
@endsection