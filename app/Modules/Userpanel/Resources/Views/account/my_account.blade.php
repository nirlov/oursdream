@extends('layouts.app')

@section('title' , 'My Account')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">My Account</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">Edit Account Information</h1>
            
            <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                <div class="box-content">
                    <form action="{{ route('my_account_update' , $user->id) }}" method="post">
                        {{ csrf_field() }}
                        <h4 class="heading-primary text-uppercase mb-lg">ACCOUNT INFORMATION</h4>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">Email Address <span class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">Phone Number</label>
                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">Address</label>
                                    <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="font-weight-normal">City</label>
                                    <select class="form-control" name="district" id="district" required>
                                        <option value="">Please select city</option>
                                        @foreach($city as $all)
                                            @if(!empty($user->city) && ($all->id == $user->city))
                                                <option value="{{ $all->id }}" selected>{{ $all->name }}</option>
                                            @else
                                                <option value="{{ $all->id }}">{{ $all->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="font-weight-normal">Thana</label>
                                    <select class="form-control" name="thana" id="thana" required>
                                        @if(!empty($user->thana))
                                            @foreach($thana as $all)
                                                @if($all->id == $user->thana)
                                                    <option value="{{ $all->id }}" selected>{{ $all->name }}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            <option value="">Please select thana</option>
                                        @endif
                                        
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">ZIP Code</label>
                                    <input type="text" class="form-control" name="zip_code" value="{{ $user->zip_code }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="font-weight-normal">Country</label>
                                    <input type="text" class="form-control" value="Bangladesh" readonly>
                                </div>
                            </div>
                        </div>

                    

                        <div class="row">
                            <div class="col-xs-12">
                                <p class="required mt-lg mb-none">* Required Fields</p>

                                <div class="form-action clearfix mt-none">
                                    <a href="demo-shop-3-login.html" class="pull-left"><i class="fa fa-angle-double-left"></i> Back</a>

                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

<script type="text/javascript">
    $('#district').on('change',function(e){

        var dist_id = parseInt(e.target.value);

        //ajax
        $.get("{{ route('thana' , ['id'=>'']) }}/"+dist_id, function(data){
            //Suceess Data
            $('#thana').empty();
            $('#thana').append('<option value="">Please select thana</option>');
            $.each(data, function(index, thanaObj){
                    $('#thana').append('<option value="'+thanaObj.id+'">'+thanaObj.name+'</option>')
            });
        });
    });
</script>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#information").addClass( "active" );
    </script>
@endsection