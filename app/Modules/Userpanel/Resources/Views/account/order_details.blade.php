@extends('layouts.app')

@section('title' , 'My Order Details')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('my_account_order_index') }}">My Order</a></li>
            <li class="active">Order Details</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">My Order Details</h1>
            
            <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                <table class="search-table table" style="font-size: 15px;">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">Product</th>
                        <th width="10%">Name</th>
                        <th width="10%">Unit Price</th>
                        <th width="10%">Quantity</th>
                        <th width="15%">Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($order_product as $all)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td class="product-image-td">
                            <a href="{{ route('product_details' , $all->product_id) }}" title="{{ $all->productId->name }}">
                                <img src="{{ asset('uploads/product/'. $all->productId->thumbnail ) }}" alt="{{ $all->productId->name }}" height="50px">
                            </a>
                        </td>
                        <td class="product-name-td">
                            <h2 class="product-name"><a href="{{ route('product_details' , $all->product_id) }}" title="Product Name">{{ $all->productId->name }}</a></h2>
                        </td>
                        <td>{{ ($all->amount)}}</td>
                        <td>{{ $all->quantity }}</td>
                        <td>{{ ($all->amount * $all->quantity) }}</td>                           
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-right" colspan="5">Shipping Charge</td>
                            <td class="content">{{ $order->shipping_charge }}</td>
                        </tr>

                        <tr>
                            <td class="text-right" colspan="5">Vat</td>
                            <td class="content">{{ $order->vat }}</td>
                        </tr>

                        <tr>
                            <td class="text-right" colspan="5" style="font-weight: bold;">Total Amount</td>
                            <td class="content" style="font-weight: bold;">{{ $order->total_amount }}</td>
                        </tr>

                    </tfoot>
                </table>
            </div>

        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#order").addClass( "active" );
    </script>
@endsection