@extends('layouts.app')

@section('title' , 'My Dashboard')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">My Account</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">My Account</h1>
            
                <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                    <div class="box-content">                       
                        <div class="featured-boxes">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Total Order</h4>
                                            <p>{{ $total_order }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Confirm Order</h4>
                                            
                                            <p>{{ $confirme_order }}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Pendding Order</h4>
                                            
                                            <p>{{ $pendding_order }}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="tall">
                <div class="row">
                    <div class="col-md-12">
                        <blockquote class="with-borders" style="min-height: 237px;">
                        @if($customer->image != Null)
                            <img class="pull-right img-responsive" src="{{ asset('uploads/customer/'. $customer->image ) }}" style=" height:200px;" alt="">
                        @else
                            <img class="pull-right img-responsive" src="{{ asset('uploads/customer/profile_avater.jpg') }}" style="height:200px;" alt="">
                        @endif
                          <h3>{!! $customer->name !!}</h3>
                          <p>Customer Id</p>
                            <h3>TID-10000{!! $customer->id!!}</h3>

                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <blockquote class="with-borders">

                            <div class="">
                                <h2>Address</h2>
                                <p>Street: {{ isset($customer->adress)?$customer->adress:'' }}</p>
                                <p>City: {{ isset($customer->cityId->name)?$customer->cityId->name:'' }}</p>
                                <p>State: {{ isset($customer->thanaId->name)?$customer->thanaId->name:'' }}</p>
                                <p>Zip Code: {{ isset($customer->zip_code)?$customer->zip_code:'' }}</p>
                                <p>Country: {{ isset($customer->country)?$customer->country:'' }}</p>
                            </div>

                        </blockquote>
                    </div>
                </div>


        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#dashboard").addClass( "active" );
    </script>
@endsection