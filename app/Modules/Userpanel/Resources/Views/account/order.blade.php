@extends('layouts.app')

@section('title' , 'My Order')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">My Order</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <h1 class="h2 heading-primary font-weight-normal">My Order</h1>
            
                <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                    <div class="box-content">                       
                        <div class="featured-boxes">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Total Order</h4>
                                            <p>{{ $total_order }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Confirm Order</h4>
                                            
                                            <p>{{ $confirme_order }}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="featured-box featured-box-quaternary featured-box-effect-1 mt-xlg">
                                        <div class="box-content">
                                            <h4 class="text-uppercase">Pendding Order</h4>
                                            
                                            <p>{{ $pendding_order }}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="tall">
                
                <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                    <div class="box-content">
                        <table class="search-table table" style="font-size: 15px;">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="15%">Date</th>
                                <th width="10%">Code</th>
                                <th width="10%">Status</th>
                                <th width="10%">Notification</th>
                                <th width="10%">Amount</th>
                                <th width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($order as $all)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $all->created_at->format('jS M, Y h:i:s a') }}</td>
                                <td>{{ $all->code }}</td>
                                @if($all->status == 0)
                                <td style="color: red;">Pending</td>
                                @else
                                <td style="color: green;">Confirm</td>
                                @endif
                                @if($all->notification == 0)
                                <td style="text-align: center; color: red;"><i class="fa fa-eye-slash" aria-hidden="true" title="Unseen"></i></td>
                                @else
                                <td style="text-align: center; color: green;"><i class="fa fa-eye" aria-hidden="true" title="Seen"></i></td>
                                @endif
                                <td>{{ $all->total_amount }}</td>
                                <td>
                                    <a href="{{ route('my_account_order_details' , $all->id) }}"><button type="button" class="btn btn-info btn-sm">View</button></a>
                                    
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>


        </div>

        @include('partials.frontend.sidebar')

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#order").addClass( "active" );
    </script>
@endsection