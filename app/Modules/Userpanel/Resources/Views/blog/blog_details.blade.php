@extends('layouts.app')

@section('title' , 'Blog')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('blog_index') }}">Blog</a></li>
            <li class="active">{{ $blog->id }}</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post">
                    <div class="post-image">
                        
                            <div style="text-align: center;"> 
                                <div class="img-thumbnail" >
                                    <img class="img-responsive" src="{{ asset('uploads/blog/'. $blog->image ) }}" alt="Post" style="width: 500px; height: auto;">
                                </div>
                            </div>
                        
                    </div>

                    <div class="post-date">
                        <span class="day">{{ $blog->created_at->format('d') }}</span>
                        <span class="month">{{ $blog->created_at->format('M') }}</span>
                    </div>

                    <div class="post-content">

                        <h2><a href="javascript::void(0)">{{ $blog->title }}</a></h2>

                        <div class="post-meta">
                            <span><i class="fa fa-user"></i> By <a href="javascript::void(0);">{{ $blog->createdBy['name'] }}</a> </span>
                            <span><i class="fa fa-comments"></i> <a href="javascript::void(0);">12 Comments</a></span>
                        </div>

                        <p>{{ $blog->description }}</p>


                        <div class="post-block post-share">
                            <h3 class="h4 heading-primary"><i class="fa fa-share"></i>Share this post</h3>

                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_pinterest_pinit"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
                            <!-- AddThis Button END -->

                        </div>

                    </div>
                </article>

            </div>
        </div>

        <div class="col-md-3">
            <aside class="sidebar">

                <h4>Blog Categories</h4>
                <ul class="nav nav-list">
                    @foreach($blog_category as $all)
                    <li><a href="{{ route('blog_category_details' , $all->id) }}">{{ $all->title }}</a></li>
                    @endforeach
                </ul>

                <h4>Recent Posts</h4>
                <ul class="simple-post-list">
                    @foreach($recent_blog as $all)
                    <li>
                        <div class="post-image">
                            <div class="img-thumbnail">
                                <a href="{{ route('blog_details' , $all->id) }}">
                                    <img src="{{ asset('uploads/blog/'. $all->image ) }}" alt="Post">
                                </a>
                            </div>
                        </div>
                        <div class="post-info">
                            <a href="{{ route('blog_details' , $all->id) }}">{{ $all->title }}</a>
                            <div class="post-meta">
                                {{ $all->created_at->format('M d, Y') }}
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </aside>
        </div>
    </div>
</div>



@endsection