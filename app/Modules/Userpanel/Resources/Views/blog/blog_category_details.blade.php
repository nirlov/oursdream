@extends('layouts.app')

@section('title' , 'Blog Category Details')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('blog_index') }}">Blog</a></li>
            <li class="active">{{ $blog_category_details->title }}</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9">

            <div class="blog-posts">
                @foreach($blog as $all)
                <article class="post post-large">

                    <div class="post-date">
                        <span class="day">{{ $all->created_at->format('d') }}</span>
                        <span class="month">{{ $all->created_at->format('M') }}</span>
                    </div>

                    <div class="post-content">

                        <h2><a href="{{ route('blog_details' , $all->id) }}">{{ $all->title }}</a></h2>
                        <p>{{ $all->summary }}</p>

                        <a href="{{ route('blog_details' , $all->id) }}" class="btn btn-link">Read more</a>

                        <div class="post-meta">
                            <span><i class="fa fa-calendar"></i>{{ $all->created_at->format('M d, Y H:i:s') }}</span>
                            <span><i class="fa fa-user"></i> By <a href="javascript::void(0)">Admin</a> </span>
                            <span><i class="fa fa-tag"></i> <a href="javascript::void(0)">{{ $all->categoryId['title'] }}</a></span>
                        </div>

                    </div>
                </article>
                @endforeach
                
            </div>

            <div class="toolbar">
                <div class="sorter">
                    <ul class="pagination">
                        {{ $blog->links() }}
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <aside class="sidebar">

                <h4>Blog Categories</h4>
                <ul class="nav nav-list">
                    @foreach($blog_category as $all)
                    <li><a href="{{ route('blog_category_details' , $all->id) }}">{{ $all->title }}</a></li>
                    @endforeach
                </ul>

                <h4>Recent Posts</h4>
                <ul class="simple-post-list">
                    @foreach($recent_blog as $all)
                    <li>
                        <div class="post-image">
                            <div class="img-thumbnail">
                                <a href="{{ route('blog_details' , $all->id) }}">
                                    <img src="{{ asset('uploads/blog/'. $all->image ) }}" alt="Post">
                                </a>
                            </div>
                        </div>
                        <div class="post-info">
                            <a href="{{ route('blog_details' , $all->id) }}">{{ $all->title }}</a>
                            <div class="post-meta">
                                {{ $all->created_at->format('M d, Y') }}
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>

            </aside>
        </div>
    </div>
</div>


@endsection