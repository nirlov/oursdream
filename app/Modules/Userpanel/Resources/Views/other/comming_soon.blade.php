@extends('layouts.app')

@section('title' , 'Comming Soon Page')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Comming Soon</li>
        </ul>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12 center">
            <h1 class="mb-sm small">OUR WEBSITE IS COMING SOON</h1>
            <p class="mb-none lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p>
        </div>
    </div>
</div>

@endsection