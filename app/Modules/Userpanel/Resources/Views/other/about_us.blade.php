@extends('layouts.app')

@section('title' , 'About Us')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">About Us</li>
        </ul>
    </div>
</section>
<div class="container about-container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="h1 heading-primary"><strong>Project</strong> Details</h3>
            <p>
                <span class="alternative-font">1. Multi Auth System</span><br/>
                <span class="alternative-font">2. Cart Product</span><br/>
                <span class="alternative-font">3. Checkout Page</span><br/>
                <span class="alternative-font">4. Topbar Header Option Is Dynamic</span><br/>
                <span class="alternative-font">5. All Information Is Dynamic</span><br/>
                <span class="alternative-font">6. Blog</span><br/>
                <span class="alternative-font">7. Product Review Process</span><br/>
                <span class="alternative-font">8. Wishlist Any Product</span><br/>
                <span class="alternative-font">9. UserPanel</span><br/>
                <span class="alternative-font">10. Check User Information</span><br/>
                <span class="alternative-font">11. Check Order History</span><br/>
                <span class="alternative-font">12. Change Information</span><br/>
                <span class="alternative-font">13. Categorywise Filter</span><br/>
                <span class="alternative-font">14. Subcategorywise Filter</span><br/>
                <span class="alternative-font">15. Discount Offer</span><br/>
                <span class="alternative-font">16. Shipping Charge</span><br/>
            </p>
        </div>
    </div>
</div>

@endsection