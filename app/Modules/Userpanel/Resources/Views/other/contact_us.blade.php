@extends('layouts.app')

@section('title' , 'Contact Us')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Contact Us</li>
        </ul>
    </div>
</section>
<div class="container about-container">
    <div class="google-maps"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29218.671869707825!2d90.4007977!3d23.7354667!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b85c2756953d%3A0xb992402a17060127!2sFakirapool+Bus+Stop!5e0!3m2!1sen!2sbd!4v1496739308975" frameborder="0" height="350" style="border:0; width:100%;" allowfullscreen></iframe></div>


    <div class="section-contact-area">
        <div class="container">
            <div class="row">
                <div class="col-md-10">

                    <div class="alert alert-success hidden mt-lg" id="contactSuccess">
                        <strong>Success!</strong> Your message has been sent to us.
                    </div>

                    <div class="alert alert-danger hidden mt-lg" id="contactError">
                        <strong>Error!</strong> There was an error sending your message.
                        <span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
                    </div>

                    <h2 class="heading-text-color">Leave a <strong>Message</strong></h2>
                    <form id="contactForm" action="#" method="POST" onsubmit="retutn false;">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Name *</label>
                                    <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                </div>

                                <div class="form-group">
                                    <label>Email *</label>
                                    <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                </div>

                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group mb-lg">
                                    <label>Message *</label>
                                    <textarea maxlength="5000" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
                                </div>

                                <input type="submit" value="Send Message" class="btn btn-primary mb-xlg" data-loading-text="Loading...">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection