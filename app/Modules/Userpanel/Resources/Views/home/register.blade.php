@extends('layouts.app')

@section('title' , 'Register')

@section('content')

<section class="form-section register-form">
    <div class="container">
        <h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Create an Account</h1>

        <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
            <div class="box-content">
                <form method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <h4 class="heading-primary text-uppercase mb-lg">PERSONAL INFORMATION</h4>
                    <div class="row">
                        <div class="col-sm-8 col-md-7">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="font-weight-normal">First Name <span class="required">*</span></label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="font-weight-normal">Email Address <span class="required">*</span></label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <h4 class="heading-primary text-uppercase mb-lg">LOGIN INFORMATION</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="font-weight-normal">Password <span class="required">*</span></label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="font-weight-normal">Confirm Password <span class="required">*</span></label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="required mt-lg mb-none">* Required Fields</p>

                            <div class="form-action clearfix mt-none">
                                <a href="demo-shop-2-login.html" class="pull-left"><i class="fa fa-angle-double-left"></i> Back</a>

                                <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


@endsection