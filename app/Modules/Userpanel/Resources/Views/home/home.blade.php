@extends('layouts.app')

@section('title' , 'Home')

@section('content')

<div class="slider-container rev_slider_wrapper" style="height: 100vh;">
    <div id="revolutionSlider" class="slider rev_slider manual">
        <ul>
            <li data-transition="fade">
                <img src="{{ asset('uploads/slider/'.$slider_1->image) }}"  
                    alt="slide bg"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="-85"
                    data-start="500"
                    data-whitespace="nowrap"                         
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 26px; text-transform: uppercase; font-weight:600; line-height:1; color: #fff;"
                    data-mask_in="x:0px;y:0px;">New Arrivals</div>

                <div class="tp-caption"
                    data-x="12"
                    data-y="center" data-voffset="-31"
                    data-start="1000"
                    data-whitespace="nowrap"                         
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 80px; font-weight:800; line-height:1.4; color: #fff; letter-spacing: -6px; padding-right:10px;"
                    data-mask_in="x:0px;y:0px;">Fashion</div>

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="28"
                    data-start="1500"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 25px; font-weight: 300; line-height:1; color: #fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;">Up to <span style="font-weight:800;">Up to 70% OFF</span> in the new collection.</div>
                
                <div class="tp-caption tp-resizeme"
                    data-x="15"
                    data-y="center" data-voffset="85"
                    data-start="2000"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 16px; font-weight: 600; line-height:1;"
                    data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn btn-default">Shop now</a></div>
            </li>
            <li data-transition="fade">
                <img src="{{ asset('uploads/slider/'.$slider_2->image) }}"  
                    alt="slide bg"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="-85"
                    data-start="500"
                    data-whitespace="nowrap"                         
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 26px; text-transform: uppercase; font-weight:600; line-height:1; color: #fff;padding-right:10px"
                    data-mask_in="x:0px;y:0px;">Exclusive</div>

                <div class="tp-caption"
                    data-x="14"
                    data-y="center" data-voffset="-31"
                    data-start="1000"
                    data-whitespace="nowrap"                         
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 80px; font-weight:800; line-height:1.4; color: #fff; letter-spacing: -6px; padding-right:10px;"
                    data-mask_in="x:0px;y:0px;">Products</div>

                <div class="tp-caption"
                    data-x="225"
                    data-y="center" data-voffset="28"
                    data-start="1500"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 25px; font-weight: 300; line-height:1; color: #fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;">Incredible Prices</div>

                <div class="tp-caption"
                    data-x="320" 
                    data-y="center" data-voffset="70"
                    data-start="2000"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 16px; font-weight: 400; line-height:1; color:#fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn btn-default" style="text-transform: uppercase;">Shop now</a></div>
            </li>
        </ul>
    </div>
</div>

<div class="banners-container">
    <div class="container">
        <div class="row">
            @foreach($subcategory as $all)
            <div class="col-sm-4">
                <a href="{{  route('subcategory_product' , $all->id) }}" class="banner">
                    <img src="{{ asset('uploads/subcategory/'.$all->image) }}" alt="Banner">
                    <span class="ban-title">{{ $all->en_title }}</span>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <div class="container" style="text-align: center;">
        <a href="#" class="btn btn-default text-uppercase font-weight-semibold custom-text-color-1">See All</a>
    </div>
</div>

<div class="container mb-sm">
    <h2 class="slider-title v2 heading-primary text-center">
        WEEKLY LATEST PRODUCTS
    </h2>

    <div class="owl-carousel owl-theme manual featured-products-carousel">
        @foreach($product as $all)
        <div class="product">
            <figure class="product-image-area">
                <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                    <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                </a>

                <a href="{{ route('product_details' , $all->id) }}" class="product-quickview">
                    <i class="fa fa-share-square-o"></i>
                    <span>Quick View</span>
                </a>
                @if($all->discountId != Null)
                <div class="product-label"><span class="discount">-{{ $all->discountId->amount }}%</span></div>
                @endif
                @if($all->new == 1)
                <div class="product-label"><span class="new">New</span></div>
                @endif
            </figure>
            <div class="product-details-area">
                <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="Product Name">{{ $all->name }}</a>
                @if($all->featured == 1)
                <span class="tip tip-hot">Featured</span>
                @endif
                </h2>
                <div class="product-ratings">
                    <div class="ratings-box">
                        @if($all->rating == 0)
                        <div class="rating" style="width:0%"></div>
                        @elseif($all->rating == 1)
                        <div class="rating" style="width:20%"></div>
                        @elseif($all->rating == 2)
                        <div class="rating" style="width:40%"></div>
                        @elseif($all->rating == 3)
                        <div class="rating" style="width:60%"></div>
                        @elseif($all->rating == 4)
                        <div class="rating" style="width:80%"></div>
                        @else
                        <div class="rating" style="width:100%"></div>
                        @endif
                    </div>
                </div>

                <div class="product-price-box">
                    @if($all->discountId != Null)
                    <span class="old-price">৳{{ $all->sales_price }}</span>
                    <span class="product-price">৳{{ $all->discount_price }}</span>
                    @else
                    <span class="product-price">৳{{ $all->sales_price }}</span>
                    @endif 
                </div>

                <div class="product-actions">
                    <a href="{{ route('wishlist_store' , $all->id) }}" class="addtowishlist" title="Add to Wishlist">
                        <i class="fa fa-heart"></i>
                    </a>
                    <a href="{{ route('cart_product' , $all->id) }}" class="addtocart" title="Add to Cart">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Add to Cart</span>
                    </a>
                    <a href="#" class="comparelink" title="Add to Compare">
                        <i class="glyphicon glyphicon-signal"></i>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<section class="explore-section parallax section section-text-light section-parallax section-center section-overlay-opacity section-overlay-opacity-scale-8 mt-none" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="../img/demos/shop/bg2.jpg">
    <div class="container">
        <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
            <div>
                <h2>Special <strong>Promo</strong></h2>
                <p>Up tp <strong>70% OFF</strong> in the new collection.</p>
                <a href="#" class="btn btn-default text-uppercase font-weight-semibold custom-text-color-1">Purchase now</a>
            </div>
            <div>
                <h2>Special <strong>Promo</strong></h2>
                <p>Up tp <strong>70% OFF</strong> in the new collection.</p>
                <a href="#" class="btn btn-default text-uppercase font-weight-semibold custom-text-color-1">Purchase now</a>
            </div>
            <div>
                <h2>Special <strong>Promo</strong></h2>
                <p>Up tp <strong>70% OFF</strong> in the new collection.</p>
                <a href="#" class="btn btn-default text-uppercase font-weight-semibold custom-text-color-1">Purchase now</a>
            </div>
        </div>
    </div>
</section>

<div class="container mb-xs">
    <h2 class="slider-title v2 heading-primary text-center">
        New Arrivals
    </h2>

    <div class="owl-carousel owl-theme manual new-products-carousel hide-addtolinks">
        @foreach($new as $all)
        <div class="product">
            <figure class="product-image-area">
                <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                    <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                </a>

                <a href="{{ route('product_details' , $all->id) }}" class="product-quickview">
                    <i class="fa fa-share-square-o"></i>
                    <span>Quick View</span>
                </a>
                @if($all->discountId != Null)
                <div class="product-label"><span class="discount">-{{ $all->discountId->amount }}%</span></div>
                @endif
                @if($all->new == 1)
                <div class="product-label"><span class="new">New</span></div>
                @endif
            </figure>
            <div class="product-details-area">
                <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a>
                @if($all->featured == 1)
                <span class="tip tip-hot">Featured</span>
                @endif
                </h2>
                <div class="product-ratings">
                    <div class="ratings-box">
                        @if($all->rating == 0)
                        <div class="rating" style="width:0%"></div>
                        @elseif($all->rating == 1)
                        <div class="rating" style="width:20%"></div>
                        @elseif($all->rating == 2)
                        <div class="rating" style="width:40%"></div>
                        @elseif($all->rating == 3)
                        <div class="rating" style="width:60%"></div>
                        @elseif($all->rating == 4)
                        <div class="rating" style="width:80%"></div>
                        @else
                        <div class="rating" style="width:100%"></div>
                        @endif
                    </div>
                </div>

                <div class="product-price-box">
                    @if($all->discountId != Null)
                    <span class="old-price">৳{{ $all->sales_price }}</span>
                    <span class="product-price">৳{{ $all->discount_price }}</span>
                    @else
                    <span class="product-price">৳{{ $all->sales_price }}</span>
                    @endif 
                </div>

                <div class="product-actions">
                    <a href="{{ route('wishlist_store' , $all->id) }}" class="addtowishlist" title="Add to Wishlist">
                        <i class="fa fa-heart"></i>
                    </a>
                    <a href="{{ route('cart_product' , $all->id) }}" class="addtocart" title="Add to Cart">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Add to Cart</span>
                    </a>
                    <a href="#" class="comparelink" title="Add to Compare">
                        <i class="glyphicon glyphicon-signal"></i>
                    </a>
                </div>

            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="container">
    <h2 class="slider-title v2 heading-primary text-center mt-sm">
        FROM THE BLOG 
    </h2>

    <div class="owl-carousel owl-theme manual recent-posts-carousel">
        @foreach($blog as $all)
        <article class="post">
            <div class="row">
                <div class="col-sm-5">
                    <div class="post-image">
                        <div class="img-thumbnail">
                            <img class="img-responsive" src="{{ asset('uploads/blog/'. $all->image ) }}" alt="Post">
                        </div>
                    </div>
                </div>

                <div class="col-sm-7">
                    <div class="post-date">
                        <span class="day">{{ $all->created_at->format('d') }}</span>
                        <span class="month">{{ $all->created_at->format('M') }}</span>
                    </div>
                    <h2><a href="{{ route('blog_details' , $all->id) }}">{{ $all->title }}</a></h2>

                    <div class="post-content">

                        <p>{{ $all->summary }}</p>

                        <a href="{{ route('blog_details' , $all->id) }}" class="btn btn-link">Read more ></a>
                    </div>
                </div>
            </div>
        </article>
        @endforeach
    </div>
</div>

@if(!Session::has('cart_product'))
    <div class="newsletter-popup mfp-hide" id="newsletter-popup-form" style="background-image: url({{ asset('uploads/popup/'. $popup->image ) }}">
        <div class="newsletter-popup-content">
            <img src="{{ asset('uploads/logo/'. $profile->logo ) }}" alt="Logo" class="img-responsive">
            <h2 style="color:black;">{{ $popup->title }}</h2>
            <p style="color:black;">{{ $popup->summary }}</p>
        </div><!-- End .newsletter-popup-content -->
    </div><!-- End .newsletter-popup -->
@endif

@endsection