@extends('layouts.app')

@section('title' , 'Change Password')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Change Password</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 my-account form-section">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Change Password</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('change_password' , $user) }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                <label for="old_password" class="col-md-4 control-label">Old Password</label>

                                <div class="col-md-6">
                                    <input type="password" id="old_password" name="old_password" value="{{ old('old_password') }}" class="form-control">
                                        @if ($errors->has('old_password'))
                                            <span class="help-block text-danger" style="color:red !important">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                        @endif

                                        @if ($errors->has('error'))
                                            <span class="help-block text-danger" style="color:red !important">
                                                <strong>{{ $errors->first('error') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">New Password</label>

                                <div class="col-md-6">
                                    <input type="password" id="new_password" name="new_password" class="form-control">
                                        @if ($errors->has('new_password'))
                                            <span class="help-block text-danger" style="color:red !important">
                                                <strong>{{ $errors->first('new_password') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                                    @if ($errors->has('confirm_password'))
                                        <span class="help-block text-danger" style="color:red !important">
                                            <strong>{{ $errors->first('confirm_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.frontend.sidebar')
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#password").addClass( "active" );
    </script>
@endsection
