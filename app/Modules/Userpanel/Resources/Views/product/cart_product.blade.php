@extends('layouts.app')

@section('title' , 'Cart')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Cart</li>
        </ul>
    </div>
</section>

<div class="cart">
    <div class="container">
        @if(Session::has('cart_product'))
        <h1 class="h2 heading-primary mt-lg clearfix">
            <span>Shopping Cart</span>
            <a href="{{ route('checkout_index') }}" class="btn btn-primary pull-right">Proceed to Checkout</a>
        </h1>

        <div class="row">
            <div class="col-md-8 col-lg-9">
                {!! Form::open(['url' => route('cart_update'), 'method' => 'post']) !!}
                <div class="cart-table-wrap">
                    <table id="dt_individual_searchs" class="cart-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Product Name</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; $j=1; $k=1;?>
                            @foreach($product as $all)
                            <input type="text" name="id[]" value="{{ $all->id }}" hidden>
                            <tr>
                                <td class="product-action-td">
                                    <a href="{{ route('cart_destroy_product' , $all->id) }}" title="Remove product" class="btn-remove"><i class="fa fa-times"></i></a>
                                </td>
                                <td class="product-image-td">
                                    <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">
                                        <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                                    </a>
                                </td>
                                <td class="product-name-td">
                                    <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a></h2>
                                </td>
                                <td>৳{{ $all->unit_price }}</td>
                                <td>
                                    <div class="qty-holder">
                                        <button type="button" class="btn btn-default btn-xs" id="minus_button">
                                            <span class="glyphicon glyphicon-minus" aria-hidden="true" onclick="minus({{ $k++ }})"></span>
                                        </button>
                        
                                        <input id="quentity" name="quantity[]" class="qty-input" type="text" value="{{ $all->quantity }}" style="border-top: none; border-bottom: none; width: 30px; text-align: center;" readonly>

                                        <button type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true" onclick="plus({{ $j++ }})"></span>
                                        </button>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-primary">৳{{ $all->sub_total }}</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6" class="clearfix">
                                    <a href="{{ route('all_product') }}" class="btn btn-default hover-primary btn-continue">Continue Shopping</a>
                                    <button type="submit" class="btn btn-default hover-primary btn-update">Update Shopping Cart</button>
                                    <a href="{{ route('cart_all_remove') }}" class="btn btn-default hover-primary btn-clear">Clear Shopping Cart</a>
                                </td>
                            </tr> 
                        </tfoot>    
                    </table>
                </div>
                {!! Form::close() !!}
            </div>
            <aside class="col-md-4 col-lg-3 sidebar shop-sidebar">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" href="#panel-cart-total">
                                    Cart Totals
                                </a>
                            </h4>
                        </div>
                        <div id="panel-cart-total" class="accordion-body collapse in">
                            <div class="panel-body">
                                <table class="totals-table">
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td id="total_amount">৳{{ $sum }}</td>
                                        </tr>
                                        <tr>
                                            <td>Grand Total</td>
                                            <td id="grand_total">৳{{ $sum }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="totals-table-action">
                                    <a href="{{ route('checkout_index') }}" class="btn btn-primary btn-block">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>

        <div class="crosssell-products">
            <h2 class="h4"><strong>Based on your selection, you may be interested in the following items:</strong></h2>
            <div class="row">
                @foreach($extra as $all)
                <div class="col-sm-6 col-md-3">
                    <div class="product product-sm">
                        <figure class="product-image-area">
                            <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                            </a>
                        </figure>
                        <div class="product-details-area">
                            <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a></h2>

                            <div class="product-price-box">
                                @if($all->discountId != Null)
                                <span class="old-price">৳{{ $all->sales_price }}</span>
                                <span class="product-price">৳{{ $all->discount_price }}</span>
                                @else
                                <span class="product-price">৳{{ $all->sales_price }}</span>
                                @endif
                            </div>

                            <a href="{{ route('cart_product' , $all->id) }}" class="btn btn-default hover-primary">Add to Cart</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @else
            <div class="col-md-12 center">
                <h1 class="mb-sm small">No Cart Product</h1>
                <a href="{{ route('all_product') }}" class="btn btn-default hover-primary btn-continue">Continue Shopping</a>
            </div>
        @endif
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function plus(x){

        var unit_price_main = document.getElementById('dt_individual_searchs').rows[x].cells[3].innerHTML.substr(1);
        
        var unit_price = parseFloat(unit_price_main);
        var quantity = parseInt(document.getElementById('dt_individual_searchs').rows[x].cells[4].getElementsByTagName("input")[0].value);
        
        document.getElementById('dt_individual_searchs').rows[x].cells[4].getElementsByTagName("input")[0].value = (quantity + 1);

        var new_quantity = parseInt(document.getElementById('dt_individual_searchs').rows[x].cells[4].getElementsByTagName("input")[0].value);
        
        document.getElementById('dt_individual_searchs').rows[x].cells[5].innerHTML = '৳' + (unit_price * new_quantity).toFixed(2);

        var amount_main = document.getElementById('total_amount').innerHTML.substr(1);
        var amount = parseFloat(amount_main);

        document.getElementById('total_amount').innerHTML = '৳' + (amount + unit_price).toFixed(2);

        var grand_total_main = document.getElementById('grand_total').innerHTML.substr(1);
        var grand_total = parseFloat(grand_total_main);

        document.getElementById('grand_total').innerHTML = '৳' + (grand_total + unit_price).toFixed(2);

    }

    function minus(y){

        var unit_price_main = document.getElementById('dt_individual_searchs').rows[y].cells[3].innerHTML.substr(1);
       
        var unit_price = parseFloat(unit_price_main);
        var quantity = parseInt(document.getElementById('dt_individual_searchs').rows[y].cells[4].getElementsByTagName("input")[0].value);

        if(quantity<=1){
            return false;
        }
        
        document.getElementById('dt_individual_searchs').rows[y].cells[4].getElementsByTagName("input")[0].value = (quantity - 1);

        var new_quantity = parseInt(document.getElementById('dt_individual_searchs').rows[y].cells[4].getElementsByTagName("input")[0].value);
        
        document.getElementById('dt_individual_searchs').rows[y].cells[5].innerHTML = '৳' + (unit_price * new_quantity).toFixed(2);

        var amount_main = document.getElementById('total_amount').innerHTML.substr(1);
        var amount = parseFloat(amount_main);

        document.getElementById('total_amount').innerHTML = '৳' + (amount - unit_price).toFixed(2);

        var grand_total_main = document.getElementById('grand_total').innerHTML.substr(1);
        var grand_total = parseFloat(grand_total_main);

        document.getElementById('grand_total').innerHTML = '৳' + (grand_total - unit_price).toFixed(2);

    }
</script>

@endsection