@extends('layouts.app')

@section('title' , 'Product Details')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>

            <li><a href="#">Fashion</a></li>
            <li class="active">{{ $product->name }}</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="product-view">
                <div class="product-essential">
                    <div class="row">
                        <div class="product-img-box col-sm-5">
                            <div class="product-img-box-wrapper">
                                <div class="product-img-wrapper">
                                    <img id="product-zoom" src="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="Product main image">
                                </div>

                                <a href="#" class="product-img-zoom" title="Zoom">
                                    <span class="glyphicon glyphicon-search"></span>
                                </a>
                            </div>

                            <div class="owl-carousel manual" id="productGalleryThumbs">
                                <div class="product-img-wrapper">
                                    <a href="#" data-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" class="product-gallery-item">
                                        <img src="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="product">
                                    </a>
                                </div>
                                <div class="product-img-wrapper">
                                    <a href="#" data-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" class="product-gallery-item">
                                        <img src="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="product">
                                    </a>
                                </div>
                                <div class="product-img-wrapper">
                                    <a href="#" data-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" class="product-gallery-item">
                                        <img src="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="product">
                                    </a>
                                </div>
                                <div class="product-img-wrapper">
                                    <a href="#" data-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" class="product-gallery-item">
                                        <img src="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="product">
                                    </a>
                                </div>
                                <div class="product-img-wrapper">
                                    <a href="#" data-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" data-zoom-image="{{ asset('uploads/product/'. $product->thumbnail ) }}" class="product-gallery-item">
                                        <img src="{{ asset('uploads/product/'. $product->thumbnail ) }}" alt="product">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="product-details-box col-sm-7">
                            
                            <h1 class="product-name">
                                {{ $product->name }}
                            </h1>

                            <div class="product-rating-container">
                                <div class="product-ratings">
                                    <div class="ratings-box">
                                        @if($product->rating == 0)
                                        <div class="rating" style="width:0%"></div>
                                        @elseif($product->rating == 1)
                                        <div class="rating" style="width:20%"></div>
                                        @elseif($product->rating == 2)
                                        <div class="rating" style="width:40%"></div>
                                        @elseif($product->rating == 3)
                                        <div class="rating" style="width:60%"></div>
                                        @elseif($product->rating == 4)
                                        <div class="rating" style="width:80%"></div>
                                        @else
                                        <div class="rating" style="width:100%"></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="review-link">
                                    <a href="#" class="review-link-in" rel="nofollow"> <span class="count">1</span> customer review</a> | 
                                    <a href="#" class="write-review-link" rel="nofollow">Add a review</a>
                                </div>
                            </div>

                            <div class="product-short-desc">
                                <p>{{ $product->description }}</p>
                            </div>

                            <div class="product-detail-info">
                                <div class="product-price-box">
                                    @if($product->discountId != Null)
                                    <span class="old-price">৳{{ $product->sales_price }}</span>
                                    <span class="product-price">৳{{ $product->discount_price }}</span>
                                    @else
                                    <span class="product-price">৳{{ $product->sales_price }}</span>
                                    @endif
                            
                                </div>
                                <p class="availability">
                                    <span class="font-weight-semibold">Availability:</span>
                                    @if($product->availability == 1)
                                        <font color="green;">In Stock</font>
                                    @else
                                        <font color="red;">Stock Out</font>
                                    @endif
                                </p>
                                
                            </div>

                            <div class="product-detail-options">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label><span class="required">*</span>Color:<span class="text-primary">Black</span></label>
                                        <ul class="configurable-filter-list filter-list-color">
                                            <li>
                                                <a href="#">
                                                    <span style="background-color: #000"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span style="background-color: #21284f"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span style="background-color: #272725"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span style="background-color: #006b20"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span style="background-color: #68686a"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-6">
                                        <label><span class="required">*</span>Size:<span class="text-primary">S</span></label>
                                        <ul class="configurable-filter-list">
                                            <li>
                                                <a href="#">S</a>
                                            </li>
                                            <li>
                                                <a href="#">M</a>
                                            </li>
                                            <li>
                                                <a href="#">L</a>
                                            </li>
                                            <li>
                                                <a href="#">XL</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="product-actions">
                                <div class="product-detail-qty">
                                    <input type="text" value="1" class="vertical-spinner" id="product-vqty">
                                </div>
                                <a href="{{ route('cart_product' , $product->id) }}" class="addtocart" title="Add to Cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span>Add to Cart</span>
                                </a>
                                
                                <div class="actions-right">
                                    <a href="{{ route('wishlist_store' , $product->id) }}" class="addtowishlist" title="Add to Wishlist">
                                        <i class="fa fa-heart"></i>
                                    </a>
                                    <a href="#" class="comparelink" title="Add to Compare">
                                        <i class="glyphicon glyphicon-signal"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="product-share-box">
                                <div class="addthis_inline_share_toolbox"></div>
                                 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-group produt-panel" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                    Description
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1One" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="product-desc-area">
                                    <p>{{ $product->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
                                    Additional
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Two" class="accordion-body collapse">
                            <div class="panel-body">
                                <table class="product-table">
                                    <tbody>
                                        <tr>
                                            <td class="table-label">Color</td>
                                            <td>{{ $product->colorId['en_title'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="table-label">Size</td>
                                            <td>{{ $product->sizeId['en_title'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Three">
                                    Summary
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Three" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="product-tags-area">
                                    <p class="note">{{ $product->summary }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1Four">
                                    Reviews
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Four" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="collateral-box">
                                    <div class="product-details-area">
                                    
                                        @if(!empty($product_review))

                                            @foreach($product_review as $review)
                                            <h1 class="product-name">{{ $review->summary }}</h1>
                                            <div class="product-ratings">
                                                <div class="ratings-box">
                                                    @if($review->rating == 1)
                                                    <div class="rating" style="width:20%"></div>
                                                    @elseif($review->rating == 2)
                                                    <div class="rating" style="width:40%"></div>
                                                    @elseif($review->rating == 3)
                                                    <div class="rating" style="width:60%"></div>
                                                    @elseif($review->rating == 4)
                                                    <div class="rating" style="width:80%"></div>
                                                    @elseif($review->rating == 5)
                                                    <div class="rating" style="width:100%"></div>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            
                                            <h2 class="product-name">{{ $review->UserId->name}}</h2>
                                            <p class="product-name">{{ $review->review}}<p>
                                            <hr class="tall">
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                <div class="add-product-review">
                                    <h3 class="text-uppercase heading-text-color font-weight-semibold">WRITE YOUR OWN REVIEW</h3>
                                    <p>How do you rate this product? <span class="required">*</span></p>

                                    {{ Form::open(['url' => route('product_review',$product->id), 'method' => 'get', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'enctype' => 'multipart/form-data']) }}
                                        <table class="ratings-table">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>1 star</th>
                                                    <th>2 stars</th>
                                                    <th>3 stars</th>
                                                    <th>4 stars</th>
                                                    <th>5 stars</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Rating</td>
                                                    <td>
                                                        <input type="radio" name="rating" id="Quality_1" value="1" class="radio">
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="rating" id="Quality_2" value="2" class="radio">
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="rating" id="Quality_3" value="3" class="radio">
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="rating" id="Quality_4" value="4" class="radio">
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="rating" id="Quality_5" value="5" class="radio" checked>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>

                                        <div class="form-group">
                                            <label>Summary of Your Review<span class="required">*</span></label>
                                            <input type="text" class="form-control" name="summary" required>
                                            @if ($errors->has('summary'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('summary') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group mb-xlg">
                                            <label>Review</label>
                                            <textarea cols="5" rows="6" class="form-control" name="review"></textarea>
                                        </div>

                                        <div class="text-right">
                                            <input type="submit" class="btn btn-primary" value="Submit Review">
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="slider-title">
                <span class="inline-title">Also Purchased</span>
                <span class="line"></span>
            </h2>

            <div class="owl-carousel owl-theme" data-plugin-options="{'items':4, 'margin':20, 'nav':true, 'dots': false, 'loop': false}">
                @foreach($category as $all)
                <div class="product">
                    <figure class="product-image-area">
                        <a href="{{ route('product_details' , $all->id) }}" title="Product Name" class="product-image">
                            <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                            <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}" class="product-hover-image">
                        </a>

                        <a href="#" class="product-quickview">
                            <i class="fa fa-share-square-o"></i>
                            <span>Quick View</span>
                        </a>
                        @if($all->discountId != Null)
                        <div class="product-label"><span class="discount">-{{ $all->discountId->amount }}%</span></div>
                        @endif
                        @if($all->new == 1)
                        <div class="product-label"><span class="new">New</span></div>
                        @endif
                    </figure>
                    <div class="product-details-area">
                        <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a>
                            @if($all->featured == 1)
                            <span class="tip tip-hot">Featured</span>
                            @endif
                        </h2>
                        <div class="product-ratings">
                            <div class="ratings-box">
                                @if($all->rating == 0)
                                <div class="rating" style="width:0%"></div>
                                @elseif($all->rating == 1)
                                <div class="rating" style="width:20%"></div>
                                @elseif($all->rating == 2)
                                <div class="rating" style="width:40%"></div>
                                @elseif($all->rating == 3)
                                <div class="rating" style="width:60%"></div>
                                @elseif($all->rating == 4)
                                <div class="rating" style="width:80%"></div>
                                @else
                                <div class="rating" style="width:100%"></div>
                                @endif
                            </div>
                        </div>

                        <div class="product-price-box">
                            @if($all->discountId != Null)
                            <span class="old-price">৳{{ $all->sales_price }}</span>
                            <span class="product-price">৳{{ $all->discount_price }}</span>
                            @else
                            <span class="product-price">৳{{ $all->sales_price }}</span>
                            @endif 
                        </div>

                        <div class="product-actions">
                            <a href="{{ route('wishlist_store' , $all->id) }}" class="addtowishlist" title="Add to Wishlist">
                                <i class="fa fa-heart"></i>
                            </a>
                            <a href="{{ route('cart_product' , $all->id) }}" class="addtocart" title="Add to Cart">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Add to Cart</span>
                            </a>
                            <a href="#" class="comparelink" title="Add to Compare">
                                <i class="glyphicon glyphicon-signal"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <aside class="col-md-3 sidebar product-sidebar">
            <div class="feature-box feature-box-style-3">
                <div class="feature-box-icon">
                    <i class="fa fa-truck"></i>
                </div>
                <div class="feature-box-info">
                    <h4>FREE SHIPPING</h4>
                    <p class="mb-none">Free shipping on all orders over $99.</p>
                </div>
            </div>

            <div class="feature-box feature-box-style-3">
                <div class="feature-box-icon">
                    <i class="fa fa-dollar"></i>
                </div>
                <div class="feature-box-info">
                    <h4>MONEY BACK GUARANTEE</h4>
                    <p class="mb-none">100% money back guarantee.</p>
                </div>
            </div>

            <div class="feature-box feature-box-style-3">
                <div class="feature-box-icon">
                    <i class="fa fa-support"></i>
                </div>
                <div class="feature-box-info">
                    <h4>ONLINE SUPPORT 24/7</h4>
                    <p class="mb-none">Lorem ipsum dolor sit amet.</p>
                </div>
            </div>

            <hr class="mt-xlg">

            <div class="owl-carousel owl-theme" data-plugin-options="{'items':1, 'margin': 5}">
                <a href="#">
                    <img class="img-responsive" src="{{ asset('frontend/img/demos/shop/banners/banner1-black.jpg') }}" alt="Banner">
                </a>
                <a href="#">
                    <img class="img-responsive" src="{{ asset('frontend/img/demos/shop/banners/banner2-black.jpg') }}" alt="Banner">
                </a>
            </div>

            <hr class="mb-xlg">

            <h4>Related Products</h4>
            <p>Check items to add to the cart or <a href="#">Select all</a></p>
            <div class="owl-carousel owl-theme" data-plugin-options="{'items':1, 'margin': 5, 'dots': false, 'nav': true}">
                <div>
                    @foreach($subcategory as $all)
                    <div class="product product-sm">
                        <figure class="product-image-area">
                            <a href="{{ route('product_details' , $all->id) }}" title="Product Name" class="product-image">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}" class="product-hover-image">
                            </a>
                        </figure>
                        <div class="product-details-area">
                            <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="Product Name">{{ $all->name }}</a>
                            @if($all->featured == 1)
                            <span class="tip tip-hot">Featured</span>
                            @endif
                            </h2>
                            <div class="product-ratings">
                                <div class="ratings-box">
                                    @if($all->rating == 0)
                                    <div class="rating" style="width:0%"></div>
                                    @elseif($all->rating == 1)
                                    <div class="rating" style="width:20%"></div>
                                    @elseif($all->rating == 2)
                                    <div class="rating" style="width:40%"></div>
                                    @elseif($all->rating == 3)
                                    <div class="rating" style="width:60%"></div>
                                    @elseif($all->rating == 4)
                                    <div class="rating" style="width:80%"></div>
                                    @else
                                    <div class="rating" style="width:100%"></div>
                                    @endif
                                </div>
                            </div>

                            <div class="product-price-box">
                            @if($all->discountId != Null)
                            <span class="old-price">৳{{ $all->sales_price }}</span>
                            <span class="product-price">৳{{ $all->discount_price }}</span>
                            @else
                            <span class="product-price">৳{{ $all->sales_price }}</span>
                            @endif 
                        </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </aside>
    </div>
</div>


@endsection