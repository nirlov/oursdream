@extends('layouts.app')

@section('title' , 'Subcategory Products')

@section('content')

<div class="fullwidth-banner" style="background-image: url({{ asset('uploads/subcategory/'.$subcategory->image) }} )">
    <div>
        <h2>CATEGORY <strong>BANNER</strong></h2>
        <p>Set banners and description for any category of your website.</p>
    </div>
</div>
                

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="#">Subcategory</a></li>
            <li class="active">{{ $subcategory->en_title }}</li>
        </ul>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <div class="toolbar mb-none">
                <div class="sorter">
                    <div class="sort-by">
                        <label>Sort by:</label>
                        <select>
                            <option value="Position">Position</option>
                            <option value="Name">Name</option>
                            <option value="Price">Price</option>
                        </select>
                        <a href="#" title="Set Desc Direction">
                            <img src="../img/demos/shop/i_asc_arrow.gif" alt="Set Desc Direction">
                        </a>
                    </div>

                    <div class="view-mode">
                        <span title="Grid">
                            <i class="fa fa-th"></i>
                        </span>
                        <a href="#" title="List">
                            <i class="fa fa-list-ul"></i>
                        </a>
                    </div>
                </div>
            </div>

            <ul class="products-grid columns4">
                @foreach($product as $all)
                <li>
                    <div class="product">
                        <figure class="product-image-area">
                            <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}" class="product-hover-image">
                            </a>

                            <a href="{{ route('product_details' , $all->id) }}" class="product-quickview">
                                <i class="fa fa-share-square-o"></i>
                                <span>Quick View</span>
                            </a>
                            @if($all->discountId != Null)
                            <div class="product-label"><span class="discount">-{{ $all->discountId->amount }}%</span></div>
                            @endif
                            @if($all->new == 1)
                            <div class="product-label"><span class="new">New</span></div>
                            @endif
                        </figure>
                        <div class="product-details-area">
                            <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a>
                            @if($all->featured == 1)
                            <span class="tip tip-hot">Featured</span>
                            @endif
                            </h2>
                            <div class="product-ratings">
                                <div class="ratings-box">
                                    @if($all->rating == 0)
                                    <div class="rating" style="width:0%"></div>
                                    @elseif($all->rating == 1)
                                    <div class="rating" style="width:20%"></div>
                                    @elseif($all->rating == 2)
                                    <div class="rating" style="width:40%"></div>
                                    @elseif($all->rating == 3)
                                    <div class="rating" style="width:60%"></div>
                                    @elseif($all->rating == 4)
                                    <div class="rating" style="width:80%"></div>
                                    @else
                                    <div class="rating" style="width:100%"></div>
                                    @endif
                                </div>
                            </div>

                            <div class="product-price-box">
                                @if($all->discountId != Null)
                                <span class="old-price">৳{{ $all->sales_price }}</span>
                                <span class="product-price">৳{{ $all->discount_price }}</span>
                                @else
                                <span class="product-price">৳{{ $all->sales_price }}</span>
                                @endif
                            </div>

                            <div class="product-actions">
                                <a href="{{ route('wishlist_store' , $all->id) }}" class="addtowishlist" title="Add to Wishlist">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <a href="{{ route('cart_product' , $all->id) }}" class="addtocart" title="Add to Cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span>Add to Cart</span>
                                </a>
                                <a href="#" class="comparelink" title="Add to Compare">
                                    <i class="glyphicon glyphicon-signal"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>

            <div class="toolbar-bottom">
                <div class="toolbar">
                    <div class="sorter">
                        <ul class="pagination">
                            {{ $product->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <aside class="col-md-3 col-md-pull-9 sidebar shop-sidebar">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-category">
                                Categories
                            </a>
                        </h4>
                    </div>
                    <div id="panel-filter-category" class="accordion-body collapse in">
                        <div class="panel-body">
                            <ul>
                                @foreach($category as $all)
                                <li><a href="{{ route('category_product' , $all->id) }}">{{ $all->en_title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-price">
                                Price
                            </a>
                        </h4>
                    </div>
                    <div id="panel-filter-price" class="accordion-body collapse in">
                        <div class="panel-body">
                            <div class="filter-price">
                                <div id="price-slider"></div>
                                <div class="filter-price-details">
                                    <span>from</span>
                                    <input type="text" id="price-range-low" class="form-control" placeholder="Min">
                                    <span>to</span>
                                    <input type="text" id="price-range-high" class="form-control" placeholder="Max">
                                    <a href="#" class="btn btn-primary">FILTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-size">
                                Size
                            </a>
                        </h4>
                    </div>
                    <div id="panel-filter-size" class="accordion-body collapse in">
                        <div class="panel-body">
                            <ul class="configurable-filter-list">
                                <li>
                                    <a href="#">S</a>
                                </li>
                                <li>
                                    <a href="#">M</a>
                                </li>
                                <li>
                                    <a href="#">L</a>
                                </li>
                                <li>
                                    <a href="#">XL</a>
                                </li>
                                <li>
                                    <a href="#">2XL</a>
                                </li>
                                <li>
                                    <a href="#">3XL</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-color">
                                color
                            </a>
                        </h4>
                    </div>
                    <div id="panel-filter-color" class="accordion-body collapse in">
                        <div class="panel-body">
                            <ul class="configurable-filter-list filter-list-color">
                                <li>
                                    <a href="#">
                                        <span style="background-color: #000"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #21284f"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #272725"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #006b20"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #68686a"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #1736a9"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #f6edd1"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #a69172"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #d8c7a7"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #fd9904"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #f56ab8"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #442937"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #bd1721"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #1226ad"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #cbcbcb"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #c7b89a"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span style="background-color: #fff"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <h4>Featured</h4>
            <div class="owl-carousel owl-theme" data-plugin-options="{'items':1, 'margin': 5, 'dots': false, 'nav': true}">
                <div>
                    @foreach($feature as $all)
                    <div class="product product-sm">
                        <figure class="product-image-area">
                            <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                                <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}" class="product-hover-image">
                            </a>
                        </figure>
                        <div class="product-details-area">
                            <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}">{{ $all->name }}</a></h2>
                            <div class="product-ratings">
                                <div class="ratings-box">
                                    @if($all->rating == 0)
                                    <div class="rating" style="width:0%"></div>
                                    @elseif($all->rating == 1)
                                    <div class="rating" style="width:20%"></div>
                                    @elseif($all->rating == 2)
                                    <div class="rating" style="width:40%"></div>
                                    @elseif($all->rating == 3)
                                    <div class="rating" style="width:60%"></div>
                                    @elseif($all->rating == 4)
                                    <div class="rating" style="width:80%"></div>
                                    @else
                                    <div class="rating" style="width:100%"></div>
                                    @endif
                                </div>
                            </div>

                            <div class="product-price-box">
                                @if($all->discountId != Null)
                                <span class="old-price">৳{{ $all->sales_price }}</span>
                                <span class="product-price">৳{{ $all->discount_price }}</span>
                                @else
                                <span class="product-price">৳{{ $all->sales_price }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </aside>
    </div>
</div>


@endsection