@extends('layouts.app')

@section('title' , 'Checkout')

@section('content')

<section class="page-header">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Checkout</li>
        </ul>
    </div>
</section>

<div class="checkout">
    <div class="container">
        @if(Session::has('cart_product'))
        <div class="col-md-12 center">
            <h1 class="mb-sm small">
                Checkout
            </h1>
        </div>

        {{ Form::open(['url' => route('checkout_store'), 'method' => 'post', 'class' => 'uk-form-stacked', 'id' => 'user_edit_form']) }}
        <div class="checkout-menu clearfix">

            <div class="dropdown pull-right checkout-review-dropdown">
                <button class="btn btn-primary mb-sm" id="reviewTable" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-shopping-cart"></i>
                    ৳{{ $grand_total }}
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-col">
                    <h3>Name &amp; Address</h3>

                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label>Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="name" value="{{ $user->name }}" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group wide">
                                <label>Email<span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label>Telephone<span class="required">*</span></label>
                                <input type="number" class="form-control" name="phone" value="{{ $user->phone?$user->phone:'' }}" required>
                                <span style="color:red; position: relative;">
                                {{ $errors->has('phone')?$errors->first('phone'):'' }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group wide">
                                <label>Address<span class="required">*</span></label>
                                <input type="text" class="form-control" name="address" value="{{ $user->address?$user->address:'' }}" required>
                                <span style="color:red; position: relative;">
                                {{ $errors->has('address')?$errors->first('address'):'' }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <div class="form-group">
                                <label>City<span class="required">*</span></label>
                                <select class="form-control" name="district" id="district" required>
                                    <option value="">Please select city</option>
                                    @foreach($city as $all)
                                        @if(!empty($user->city) && ($all->id == $user->city))
                                            <option value="{{ $all->id }}" selected>{{ $all->name }}</option>
                                        @else
                                            <option value="{{ $all->id }}">{{ $all->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span style="color:red; position: relative;">
                                {{ $errors->has('district')?$errors->first('district'):'' }}
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <div class="form-group">
                                <label>Thana<span class="required">*</span></label>
                                <select class="form-control" name="thana" id="thana" required>
                                    @if(!empty($user->thana))
                                        @foreach($thana as $all)
                                            @if($all->id == $user->thana)
                                                <option value="{{ $all->id }}" selected>{{ $all->name }}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option value="">Please select thana</option>
                                    @endif
                                    
                                </select>
                                <span style="color:red; position: relative;">
                                {{ $errors->has('thana')?$errors->first('thana'):'' }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <div class="form-group">
                                <label>Zip/Postal Code</label>
                                <input type="text" class="form-control" name="zip_code" value="{{ $user->zip_code?$user->zip_code:'' }}">
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control" name="country" value="Bangladesh" readonly>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-col">
                    <h3>Shipping Method</h3>
                    <div class="ship-list">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($product as $all)
                                <tr>
                                    <td>{{$all->name}}</td>
                                    <td class="text-center">{{$all->quantity}}</td>
                                    <td class="text-right">{{$all->sub_total}}</td>
                                </tr>
                                @endforeach

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td class="text-right" colspan="2">Subtotal</td>

                                    <td class="text-right">
                                        {{ $sum }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">Vat ({{ $vat->vat }}% inclusive)</td>

                                    <td class="text-right">
                                        {{ $total_vat }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">Shipping Charge</td>

                                    <td class="text-right">
                                        {{ (integer)($shipping_charge) }}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                    <div class="checkout-review-action">
                        <h3>Grand Total <span>৳{{ $grand_total }}</span></h3>
                        <button type="submit" class="btn btn-primary">Place Order now</button>
                    </div>
                
            </div>
        </div>

        {{ Form::close() }}
        @else
            <div class="col-md-12 center">
                <h1 class="mb-sm small">Order Placed Successfully</h1>
                <a href="{{ route('all_product') }}" class="btn btn-default hover-primary btn-continue">Continue Shopping</a>
            </div>
        @endif
    </div>
</div>

<script type="text/javascript">
    $('#district').on('change',function(e){

        var dist_id = parseInt(e.target.value);

        //ajax
        $.get("{{ route('thana' , ['id'=>'']) }}/"+dist_id, function(data){
            //Suceess Data
            $('#thana').empty();
            $('#thana').append('<option value="">Please select thana</option>');
            $.each(data, function(index, thanaObj){
                    $('#thana').append('<option value="'+thanaObj.id+'">'+thanaObj.name+'</option>')
            });
        });
    });
</script>


@endsection