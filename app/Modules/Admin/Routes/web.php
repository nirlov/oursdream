<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

 //Ajax-select category subcategory

 Route::get('/ajax-product/{id}' , 'SearchController\WebController@index')->name('search_product');

 Route::group(['prefix' => 'admin'], function () {

    Route::get('/', 'DashboardController\WebController@index')->name('admin_index');
    // Route::get('create', 'Admin\WebController@create')->name('web_profile_create');
    // Route::post('store', 'Admin\WebController@store')->name('web_profile_store');
    // Route::get('{id}/show', 'Admin\WebController@show')->name('web_profile_show');
    // Route::get('{id}/edit', 'Admin\WebController@edit')->name('web_profile_edit');
    // Route::post('{id}/update', 'Admin\WebController@update')->name('web_profile_update');
    // Route::get('{id}/destroy', 'Admin\WebController@destroy')->name('web_profile_destroy');

});

 //Category

  Route::group(['prefix' => 'admin/category'], function () {

  	Route::get('/' , 'CategoryController\WebController@index')->name('admin_category_index');
  	Route::get('/create' , 'CategoryController\WebController@create')->name('admin_category_create');
  	Route::post('/store' , 'CategoryController\WebController@store')->name('admin_category_store');
  	Route::get('/edit/{id}' , 'CategoryController\WebController@edit')->name('admin_category_edit');
  	Route::post('/update/{id}' , 'CategoryController\WebController@update')->name('admin_category_update');
  	Route::get('/destroy/{id}' , 'CategoryController\WebController@destroy')->name('admin_category_destroy');

  });

   //Subcategory

  Route::group(['prefix' => 'admin/subcategory'], function () {

  	Route::get('/' , 'SubcategoryController\WebController@index')->name('admin_subcategory_index');
  	Route::get('/create' , 'SubcategoryController\WebController@create')->name('admin_subcategory_create');
  	Route::post('/store' , 'SubcategoryController\WebController@store')->name('admin_subcategory_store');
  	Route::get('/edit/{id}' , 'SubcategoryController\WebController@edit')->name('admin_subcategory_edit');
  	Route::post('/update/{id}' , 'SubcategoryController\WebController@update')->name('admin_subcategory_update');
  	Route::get('/destroy/{id}' , 'SubcategoryController\WebController@destroy')->name('admin_subcategory_destroy');

  });

  //Product

  Route::group(['prefix' => 'admin/product'], function () {

  	Route::get('/' , 'ProductController\WebController@index')->name('admin_product_index');
  	Route::get('/create' , 'ProductController\WebController@create')->name('admin_product_create');
  	Route::post('/store' , 'ProductController\WebController@store')->name('admin_product_store');
  	Route::get('/edit/{id}' , 'ProductController\WebController@edit')->name('admin_product_edit');
  	Route::post('/update/{id}' , 'ProductController\WebController@update')->name('admin_product_update');
  	Route::get('/destroy/{id}' , 'ProductController\WebController@destroy')->name('admin_product_destroy');

  });

  //Color

  Route::group(['prefix' => 'admin/color'], function () {

  	Route::get('/' , 'ColorController\WebController@index')->name('admin_color_index');
  	Route::get('/create' , 'ColorController\WebController@create')->name('admin_color_create');
  	Route::post('/store' , 'ColorController\WebController@store')->name('admin_color_store');
  	Route::get('/edit/{id}' , 'ColorController\WebController@edit')->name('admin_color_edit');
  	Route::post('/update/{id}' , 'ColorController\WebController@update')->name('admin_color_update');
  	Route::get('/destroy/{id}' , 'ColorController\WebController@destroy')->name('admin_color_destroy');

  });

  //Size

  Route::group(['prefix' => 'admin/size'], function () {

  	Route::get('/' , 'SizeController\WebController@index')->name('admin_size_index');
  	Route::get('/create' , 'SizeController\WebController@create')->name('admin_size_create');
  	Route::post('/store' , 'SizeController\WebController@store')->name('admin_size_store');
  	Route::get('/edit/{id}' , 'SizeController\WebController@edit')->name('admin_size_edit');
  	Route::post('/update/{id}' , 'SizeController\WebController@update')->name('admin_size_update');
  	Route::get('/destroy/{id}' , 'SizeController\WebController@destroy')->name('admin_size_destroy');

  });

  //Discount

  Route::group(['prefix' => 'admin/discount'], function () {

  	Route::get('/' , 'DiscountController\WebController@index')->name('admin_discount_index');
  	Route::get('/create' , 'DiscountController\WebController@create')->name('admin_discount_create');
  	Route::post('/store' , 'DiscountController\WebController@store')->name('admin_discount_store');
  	Route::get('/edit/{id}' , 'DiscountController\WebController@edit')->name('admin_discount_edit');
  	Route::post('/update/{id}' , 'DiscountController\WebController@update')->name('admin_discount_update');
  	Route::get('/destroy/{id}' , 'DiscountController\WebController@destroy')->name('admin_discount_destroy');

  });

  //Order Pedding

  Route::group(['prefix' => 'admin/order/pendding'], function () {

  	Route::get('/' , 'PenddingOrderController\WebController@index')->name('admin_order_pendding_index');
  	Route::get('/show/{id}' , 'PenddingOrderController\WebController@show')->name('admin_order_pendding_show');
  	Route::get('/update/{id}' , 'PenddingOrderController\WebController@update')->name('admin_order_pendding_update');
  	Route::get('/destroy/{id}' , 'PenddingOrderController\WebController@destroy')->name('admin_order_pendding_destroy');

  });

  //Order Confirm

    Route::group(['prefix' => 'admin/order/confirm'], function () {

  	Route::get('/' , 'ConfirmOrderController\WebController@index')->name('admin_order_confirm_index');
  	Route::get('/show/{id}' , 'ConfirmOrderController\WebController@show')->name('admin_order_confirm_show');
  	Route::get('/update/{id}' , 'ConfirmOrderController\WebController@update')->name('admin_order_confirm_update');
  	Route::get('/destroy/{id}' , 'ConfirmOrderController\WebController@destroy')->name('admin_order_confirm_destroy');

  });

  //Customer

    Route::group(['prefix' => 'admin/customer'], function () {

  	Route::get('/' , 'CustomerController\WebController@index')->name('admin_customer_index');
  	Route::get('/show/{id}' , 'CustomerController\WebController@show')->name('admin_customer_show');

  });

  //Blog Category

  Route::group(['prefix' => 'admin/blog/category'], function () {

  	Route::get('/' , 'BlogCategoryController\WebController@index')->name('admin_blog_category_index');
  	Route::get('/create' , 'BlogCategoryController\WebController@create')->name('admin_blog_category_create');
  	Route::post('/store' , 'BlogCategoryController\WebController@store')->name('admin_blog_category_store');
  	Route::get('/edit/{id}' , 'BlogCategoryController\WebController@edit')->name('admin_blog_category_edit');
  	Route::post('/update/{id}' , 'BlogCategoryController\WebController@update')->name('admin_blog_category_update');
  	Route::get('/destroy/{id}' , 'BlogCategoryController\WebController@destroy')->name('admin_blog_category_destroy');

  });

  //Blog

  Route::group(['prefix' => 'admin/blog'], function () {

  	Route::get('/' , 'BlogController\WebController@index')->name('admin_blog_index');
  	Route::get('/create' , 'BlogController\WebController@create')->name('admin_blog_create');
  	Route::post('/store' , 'BlogController\WebController@store')->name('admin_blog_store');
  	Route::get('/edit/{id}' , 'BlogController\WebController@edit')->name('admin_blog_edit');
  	Route::post('/update/{id}' , 'BlogController\WebController@update')->name('admin_blog_update');
  	Route::get('/destroy/{id}' , 'BlogController\WebController@destroy')->name('admin_blog_destroy');

  });

  //Profie

  Route::group(['prefix' => 'admin/profile'], function () {

  	Route::get('/{id}' , 'ProfileController\WebController@index')->name('admin_profile_index');
  	Route::post('/update/{id}' , 'ProfileController\WebController@update')->name('admin_profile_update');

  });

  //Charge

  Route::group(['prefix' => 'admin/charge'], function () {

    Route::get('/shipping-charge' , 'ChargeController\WebController@shipping')->name('admin_shipping_index');
    Route::post('/shipping-charge-update/{id}' , 'ChargeController\WebController@shippingUpdate')->name('admin_shipping_update');

    Route::get('/vat' , 'ChargeController\WebController@vat')->name('admin_vat_index');
    Route::post('/vat-update/{id}' , 'ChargeController\WebController@vatUpdate')->name('admin_vat_update');

  });

  //Pop-Up

  Route::group(['prefix' => 'admin/popup'], function () {

    Route::get('/' , 'PopUpController\WebController@index')->name('admin_popup_index');
    Route::post('/update/{id}' , 'PopUpController\WebController@update')->name('admin_popup_update');

  });