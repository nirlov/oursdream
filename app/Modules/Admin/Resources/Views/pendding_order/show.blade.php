@extends('layouts.admin')

@section('title' , 'Order Show')

@section('content')
<!-- Page Header-->
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Order Show</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Order Show</li>
  </ul>
</div>
<section class="tables">   
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Order Code : {{ $order->code }}</h3>
            <pre>            </pre>
            <h3 class="h4">User Name : {{ $order->userId['name'] }}</h3>
            <pre>       </pre>
            <h3 class="h4">Date : {{ $order->created_at->format('d M,Y') }}</h3>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Unit Price</th>
                  <th>Sub Total</th>
                  <th>Image</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                @foreach($order_product as $all)
                <tr>
                  <th scope="row">{{ $i++ }}</th>
                  <td>{{ $all->productId['name'] }}</td>
                  <td>{{ $all->quantity }}</td>
                  <td>{{ $all->amount }}</td>
                  <td>{{ ($all->amount * $all->quantity) }}</td>
                  <td><img src="{{ asset('uploads/product/'.$all->productId['thumbnail']) }}" style="width: 80px; height: auto;"></td>
                </tr>
                @endforeach
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Sub Total</th>
                  <th>{{ $sum }}</th>
                  <th></th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Shipping Charge</th>
                  <th>{{ $order->shipping_charge }}</th>
                  <th></th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Vat</th>
                  <th>{{ $order->vat }}</th>
                  <th></th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Total Amount</th>
                  <th>{{ $order->total_amount }}</th>
                  <th></th>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="form-group row">
                <div class="col-sm-4 offset-sm-7">
                  <a href="{{ route('admin_order_pendding_index') }}" class="btn btn-secondary">Back</a>
                  <a href="{{ route('admin_order_pendding_update' , $order->id) }}" class="btn btn-primary">Confirme Order</a>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#order").addClass( "active" );
  </script>
@endsection