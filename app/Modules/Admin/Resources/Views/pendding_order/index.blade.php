@extends('layouts.admin')

@section('title' , 'Pendding Order')

@section('content')
<!-- Page Header-->
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Pendding Order</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Pendding Order</li>
  </ul>
</div>
<section class="tables">   
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Pendding Order Table</h3>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Code</th>
                  <th>Amount</th>
                  <th>User Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                @foreach($order as $all)
                <tr>
                  <th scope="row">{{ $i++ }}</th>
                  <td>{{ $all->code }}</td>
                  <td>{{ $all->total_amount }} BDT</td>
                  <td>{{ $all->userId['name'] }}</td>
                  <td>
                    <a href="{{ route('admin_order_pendding_update' , $all->id) }}"><i class="fa fa-check" aria-hidden="true" title="Confirm"></i></a>
                    <a href="{{ route('admin_order_pendding_show' , $all->id) }}"><i class="fa fa-eye" aria-hidden="true" title="Show"></i></a>
                    <a href="{{ route('admin_order_pendding_destroy' , $all->id) }}"><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#order").addClass( "active" );
  </script>
@endsection