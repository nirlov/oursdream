@extends('layouts.admin')

@section('title' , 'Vat')

@section('content')

<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Vat Create</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Vat</li>
  </ul>
</div>
<!-- Forms Section-->
<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      <!-- Form Elements -->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Create</h3>
          </div>
          <div class="card-body">
              {{ Form::open(['url' => route('admin_vat_update' , $vat->id), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'enctype' => 'multipart/form-data']) }}
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">In Dhaka<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="vat" value="{{ $vat->vat }}" required>
                  @if ($errors->has('vat'))
                      <span class="help-block">
                          <strong>{{ $errors->first('vat') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              
              <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <a href="{{ route('admin_category_index') }}" class="btn btn-secondary">Cancel</a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#vat").addClass( "active" );
  </script>
@endsection