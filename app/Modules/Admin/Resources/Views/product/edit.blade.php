@extends('layouts.admin')

@section('title' , 'Product')

@section('content')

<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Product Edit</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Product</li>
  </ul>
</div>
<!-- Forms Section-->
<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      <!-- Form Elements -->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Edit</h3>
          </div>
          <div class="card-body">
              {{ Form::open(['url' => route('admin_product_update' , $product->id), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'enctype' => 'multipart/form-data']) }}
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Category<span style="color:red;">*</span></label>
                <div class="col-sm-3">
                  <select id="category_id" name="category_id" class="form-control" required>
                    <option value="">Select Category</option>
                    @foreach($category as $all)
                      @if($all->id == $product->category_id)
                        <option value="{{ $all->id }}" selected>{{ $all->en_title }}</option>
                      @else
                        <option value="{{ $all->id }}">{{ $all->en_title }}</option>
                      @endif
                    @endforeach
                  </select>
                  @if ($errors->has('category_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('category_id') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Subcategory<span style="color:red;">*</span></label>
                <div class="col-sm-3">
                  <select id="subcategory_id" name="subcategory_id" class="form-control" required>
                    <option value="">Select Subcategory</option>
                    @foreach($subcategory as $all)
                      @if($all->id == $product->subcategory_id)
                        <option value="{{ $all->id }}" selected>{{ $all->en_title }}</option>
                      @else
                        <option value="{{ $all->id }}">{{ $all->en_title }}</option>
                      @endif
                    @endforeach
                  </select>
                  @if ($errors->has('subcategory_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('subcategory_id') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Name<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="name" value="{{ $product->name }}" required>
                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Code<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="code" value="{{ $product->code }}" required>
                  @if ($errors->has('code'))
                      <span class="help-block">
                          <strong>{{ $errors->first('code') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Description</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="4" cols="50" name="description">{{ $product->description }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Summary</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="2" cols="50" name="summary">{{ $product->summary }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Specification</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="4" cols="50" name="specification">{{ $product->specification }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Image<span style="color:red;">*</span></label>
                <div class="col-sm-5">
                  <input type="file" class="form-control" name="image" required>
                  <span class="help-block-none">Please select 300X346 images</span>
                </div>
                <div class="col-sm-3">
                  <img src="{{ asset('uploads/product/'.$product->thumbnail) }}" style="width: 250px; height: auto;">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Price<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="sales_price" value="{{ $product->sales_price }}" required>
                  @if ($errors->has('sales_price'))
                      <span class="help-block">
                          <strong>{{ $errors->first('sales_price') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Featured</label>
                <div class="col-sm-9">
                  <div class="i-checks">
                    <input id="featured1" type="radio" value="1" name="featured" class="radio-template" {{ ($product->featured == '1')?'checked':'' }}/>
                    <label for="featured1">Yes</label>
                  </div>
                  <div class="i-checks">
                    <input id="featured2" type="radio" value="0" name="featured" class="radio-template" {{ ($product->featured == '0')?'checked':'' }}/>
                    <label for="featured2">No</label>
                  </div>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">New</label>
                <div class="col-sm-9">
                  <div class="i-checks">
                    <input id="new1" type="radio" value="1" name="new" class="radio-template" {{ ($product->new == '1')?'checked':'' }}>
                    <label for="new1">Yes</label>
                  </div>
                  <div class="i-checks">
                    <input id="new2" type="radio" value="0" name="new" class="radio-template" {{ ($product->new == '0')?'checked':'' }}>
                    <label for="new2">No</label>
                  </div>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Availability</label>
                <div class="col-sm-9">
                  <div class="i-checks">
                    <input id="availability1" type="radio" value="1" name="availability" class="radio-template" {{ ($product->availability == '1')?'checked':'' }}>
                    <label for="availability1">In Stock</label>
                  </div>
                  <div class="i-checks">
                    <input id="availability2" type="radio" value="0" name="availability" class="radio-template" {{ ($product->availability == '0')?'checked':'' }}>
                    <label for="availability2">Out Stock</label>
                  </div>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Rating</label>
                <div class="col-sm-9">
                  <div class="i-checks">
                    <input id="rating1" type="radio" value="1" name="rating" class="radio-template" {{ ($product->rating == '1')?'checked':'' }}>
                    <label for="rating1">
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                    </label>
                  </div>
                  <div class="i-checks">
                    <input id="rating2" type="radio" value="2" name="rating" class="radio-template" {{ ($product->rating == '2')?'checked':'' }}>
                    <label for="rating2">
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                    </label>
                  </div>
                  <div class="i-checks">
                    <input id="rating3" type="radio" value="3" name="rating" class="radio-template" {{ ($product->rating == '3')?'checked':'' }}>
                    <label for="rating3">
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                    </label>
                  </div>
                  <div class="i-checks">
                    <input id="rating4" type="radio" value="4" name="rating" class="radio-template" {{ ($product->rating == '4')?'checked':'' }}>
                    <label for="rating4">
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                    </label>
                  </div>
                  <div class="i-checks">
                    <input id="rating5" type="radio" value="5" name="rating" class="radio-template" {{ ($product->rating == '5')?'checked':'' }}>
                    <label for="rating5">
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                      <i class="fa fa-star fa-lg" aria-hidden="true"></i>
                    </label>
                  </div>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Color</label>
                <div class="col-sm-3">
                  <select name="color_id" class="form-control">
                    <option value="">Select Color</option>
                    @foreach($color as $all)
                      @if($all->id == $product->color_id)
                        <option value="{{ $all->id }}" selected>{{ $all->en_title }}</option>
                      @else
                        <option value="{{ $all->id }}">{{ $all->en_title }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Size</label>
                <div class="col-sm-3">
                  <select name="size_id" class="form-control">
                    <option value="">Select Size</option>
                    @foreach($size as $all)
                      @if($all->id == $product->size_id)
                        <option value="{{ $all->id }}" selected>{{ $all->en_title }}</option>
                      @else
                        <option value="{{ $all->id }}">{{ $all->en_title }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Discount</label>
                <div class="col-sm-3">
                  <select name="discount_id" class="form-control">
                    <option value="">No Discount</option>
                    @foreach($discount as $all)
                      @if($all->id == $product->discount_id)
                        <option value="{{ $all->id }}" selected>{{ $all->en_title }}</option>
                      @else
                        <option value="{{ $all->id }}">{{ $all->en_title }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <a href="{{ route('admin_product_index') }}" class="btn btn-secondary">Cancel</a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#category").addClass( "active" );
  </script>

  <script type="text/javascript">
    $('#category_id').on('change',function(e){

        var category_id = parseInt(e.target.value);

        //ajax
        $.get("{{ route('search_product' , ['id'=>'']) }}/"+category_id, function(data){
            //Suceess Data
            console.log(data);
            $('#subcategory_id').empty();
            $('#subcategory_id').append('<option value="">Select Subcategory</option>');
            $.each(data, function(index, thanaObj){
                    $('#subcategory_id').append('<option value="'+thanaObj.id+'">'+thanaObj.en_title+'</option>')
            });
        });
    });
  </script>
@endsection