@extends('layouts.admin')

@section('title' , 'Profile')

@section('content')

<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Profile Edit</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Profile</li>
  </ul>
</div>
<!-- Forms Section-->
<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      <!-- Form Elements -->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Edit</h3>
          </div>
          <div class="card-body">
              {{ Form::open(['url' => route('admin_profile_update' , $profile->id), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'enctype' => 'multipart/form-data']) }}
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Display Name<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="display_name" value="{{ $profile->display_name }}" required>
                  @if ($errors->has('display_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('display_name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Company Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="company_name" value="{{ $profile->company_name }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Address</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="4" cols="50" name="address">{{ $profile->address }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Website</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="website" value="{{ $profile->website }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Number</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="number" value="{{ $profile->number }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Email</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="email" value="{{ $profile->email }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Facebook Link</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="facebook_link" value="{{ $profile->facebook_link }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Twitter Link</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="twitter_link" value="{{ $profile->twitter_link }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Google Link</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="google_link" value="{{ $profile->google_link }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">About Us</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="2" cols="50" name="about_us">{{ $profile->about_us }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Working Hour</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="working_hour" value="{{ $profile->working_hour }}">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Logo</label>
                <div class="col-sm-5">
                  <input type="file" class="form-control" name="image">
                  <span class="help-block-none">Please select 164X82 images</span>
                </div>
                <div class="col-sm-3">
                  <img src="{{ asset('uploads/logo/'.$profile->logo) }}" style="width: 250px; height: auto;">
                </div>
              </div>
              <div class="line"></div>
            
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <a href="{{ route('admin_index') }}" class="btn btn-secondary">Cancel</a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#profile").addClass( "active" );
  </script>
@endsection