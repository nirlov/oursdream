@extends('layouts.admin')

@section('title' , 'Popup')

@section('content')

<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Popup Create</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Popup</li>
  </ul>
</div>
<!-- Forms Section-->
<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      <!-- Form Elements -->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Create</h3>
          </div>
          <div class="card-body">
              {{ Form::open(['url' => route('admin_popup_update' , $popup->id), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'enctype' => 'multipart/form-data']) }}
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Title<span style="color:red;">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="title" value="{{ $popup->title }}" required>
                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Summary</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="4" cols="50" name="summary">{{ $popup->summary }}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Image<span style="color:red;">*</span></label>
                <div class="col-sm-5">
                  <input type="file" class="form-control" name="image">
                  <span class="help-block-none">Please select 700X320 images</span>
                </div>
                <div class="col-sm-3">
                  <img src="{{ asset('uploads/popup/'.$popup->image) }}" style="width: 250px; height: auto;">
                </div>
              </div>
              
              <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <a href="{{ route('admin_category_index') }}" class="btn btn-secondary">Cancel</a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#popup").addClass( "active" );
  </script>
@endsection