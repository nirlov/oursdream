<!DOCTYPE html>
<html>
  <head>
    
    <title>PDF</title>
    
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('backend/css/fontastic.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('backend/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('backend/css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('uploads/logo/icon.png') }}">
    
  </head>
  <body>
<div class="page home-page">
	<div class="page-content d-flex align-items-stretch">
		<div class="content-inner">
			<!-- Page Header-->
			<header class="page-header">
			  <div class="container-fluid">
			    <h1 class="no-margin-bottom">Hi, {{ $order->userId['name'] }}</h1>
			  </div>
			  <div class="container-fluid" style="position: fixed; top: 0; right: 0; text-align: right;">
			    <h4 class="no-margin-bottom">Your's Dream Becomes Our's Dream.......</h4>
			  </div>
			  <div class="container-fluid" style="position: fixed; top: 0; right: 0; text-align: center;">
			    <img src="{{ asset('uploads/logo/'.$profile->logo) }}" style="width: 100px; height: auto;">
			  </div>
			</header>
			<!-- Breadcrumb-->
			<div class="breadcrumb-holder container-fluid">
			  <ul class="breadcrumb">
			    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">{{ $profile->company_name }}</a></li>
			    <li class="breadcrumb-item active">Order Details</li>
			  </ul>
			</div>
			<br>
			<section class="tables">   
			  <div class="container-fluid">
			    <div class="row">
			      <div class="col-lg-12">
			        <div class="card">
			          <div class="card-header d-flex align-items-center">
			          	<h3 class="h3" style="text-decoration: underline;">Order Date : {{ $order->created_at->format('d M,Y') }}</h3>
			            <h3 class="h4">Order Code : {{ $order->code }}</h3>
			            <h3 class="h4">Delivary Date : {{ $today }}</h3>
			          </div>
			          <div class="">
			            <h3 class="h3">Address : {{ $order->userId['address'].', '.$order->userId->thanaId['name'].', '.$order->userId->cityId['name'].'-'.$order->userId['zip_code'].', '.$order->userId->country }}</h3>
			            <h3>Mobile No : {{ $order->userId['phone'] }}</h3>
			          </div>
			          <div class="card-body">
			            <table class="table">
			              <thead>
			                <tr>
			                  <th>#</th>
			                  <th>Product Name</th>
			                  <th>Code</th>
			                  <th>Quantity</th>
			                  <th>Unit Price</th>
			                  <th>Sub Total</th>
			                  <th>Image</th>
			                </tr>
			              </thead>
			              <tbody>
			                <?php $i=1; ?>
			                @foreach($order_product as $all)
			                <tr>
			                  <th scope="row">{{ $i++ }}</th>
			                  <td>{{ $all->productId['name'] }}</td>
			                  <td>{{ $all->productId['code'] }}</td>
			                  <td>{{ $all->quantity }}</td>
			                  <td>{{ $all->amount }}</td>
			                  <td>{{ ($all->amount * $all->quantity) }}</td>
			                  <td><img src="{{ asset('uploads/product/'.$all->productId['thumbnail']) }}" style="width: 80px; height: auto;"></td>
			                </tr>
			                @endforeach
			                <tr>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th>Sub Total</th>
			                  <th>{{ $sum }}</th>
			                  <th></th>
			                </tr>
			                <tr>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th>Shipping Charge</th>
			                  <th>{{ $order->shipping_charge }}</th>
			                  <th></th>
			                </tr>
			                <tr>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th>Vat</th>
			                  <th>{{ $order->vat }}</th>
			                  <th></th>
			                </tr>
			                <tr>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th></th>
			                  <th>Total Amount</th>
			                  <th>{{ $order->total_amount }}</th>
			                  <th></th>
			                </tr>
			              </tbody>
			            </table>
			          </div>
			          <hr>
				        <div class="" style="text-align: right;">
							<h3 class="no-margin-bottom" style="text-decoration: overline;">Customer Signature</h3>
						</div>
			        </div>
			      </div>
			    </div>
			  </div>
			</section>
		</div>
	</div>
</div>

  </body>
</html>
