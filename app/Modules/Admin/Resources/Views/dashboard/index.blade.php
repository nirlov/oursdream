@extends('layouts.admin')

@section('title' , 'Dashboard')

@section('content')
  <!-- Page Header-->
  <header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom">Dashboard</h2>
    </div>
  </header>
  <!-- Dashboard Counts Section-->
  <section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
      <div class="row bg-white has-shadow">
        <!-- Item -->
        <div class="col-xl-3 col-sm-6">
          <div class="item d-flex align-items-center">
            <div class="icon bg-violet"><i class="icon-user"></i></div>
            <div class="title"><span>New<br>Clients</span>
              <div class="progress">
                <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
              </div>
            </div>
            <div class="number"><strong>{{ $new_customer }}</strong></div>
          </div>
        </div>
        <!-- Item -->
        <div class="col-xl-3 col-sm-6">
          <div class="item d-flex align-items-center">
            <div class="icon bg-red"><i class="icon-padnote"></i></div>
            <div class="title"><span>Total<br>Orders</span>
              <div class="progress">
                <div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
              </div>
            </div>
            <div class="number"><strong>{{ $total_order }}</strong></div>
          </div>
        </div>
        <!-- Item -->
        <div class="col-xl-3 col-sm-6">
          <div class="item d-flex align-items-center">
            <div class="icon bg-green"><i class="icon-bill"></i></div>
            <div class="title"><span>Pendding<br>Orders</span>
              <div class="progress">
                <div role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
              </div>
            </div>
            <div class="number"><strong>{{ $total_pendding_order }}</strong></div>
          </div>
        </div>
        <!-- Item -->
        <div class="col-xl-3 col-sm-6">
          <div class="item d-flex align-items-center">
            <div class="icon bg-orange"><i class="icon-check"></i></div>
            <div class="title"><span>Confirm<br>Orders</span>
              <div class="progress">
                <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
              </div>
            </div>
            <div class="number"><strong>{{ $total_confirm_order }}</strong></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Dashboard Header Section    -->
  <section class="dashboard-header">
    <div class="container-fluid">
      <div class="row">
        <!-- Statistics -->
        <div class="statistics col-lg-3 col-12">
          <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-red"><i class="fa fa-tasks"></i></div>
            <div class="text"><strong>{{ $product }}</strong><br><small>Total Product</small></div>
          </div>
          <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-green"><i class="fa fa-calendar-o"></i></div>
            <div class="text"><strong>{{ $category }}</strong><br><small>Total Category</small></div>
          </div>
          <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-orange"><i class="fa fa-paper-plane-o"></i></div>
            <div class="text"><strong>{{ $subcategory }}</strong><br><small>Total Subcategory</small></div>
          </div>
        </div>
        <!-- Line Chart            -->
        <div class="chart col-lg-6 col-12">
          <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
            <canvas id="lineCahrt"></canvas>
          </div>
        </div>
        <div class="chart col-lg-3 col-12">
          <!-- Bar Chart   -->
          <div class="bar-chart has-shadow bg-white">
            <div class="title"><strong class="text-violet">95%</strong><br><small>Current Server Uptime</small></div>
            <canvas id="barChartHome"></canvas>
          </div>
          <!-- Numbers-->
          <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-green"><i class="fa fa-line-chart"></i></div>
            <div class="text"><strong>99.9%</strong><br><small>Success Rate</small></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Projects Section-->
  <section class="projects no-padding-top">
    <div class="container-fluid">
      <!-- Project-->
      @foreach($pendding_order as $all)
      <div class="project">
        <div class="row bg-white has-shadow">
          <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
            <div class="project-title d-flex align-items-center">
              <div class="image has-shadow"><img src="img/project-1.jpg" alt="..." class="img-fluid"></div>
              <div class="text">
                <h3 class="h4">{{ $all->code }}</h3><small>{{ $all->userId['name'] }}</small>
              </div>
            </div>
            <div class="project-date"><span class="hidden-sm-down">{{ $all->created_at->diffForHumans() }}</span></div>
          </div>
          <div class="right-col col-lg-6 d-flex align-items-center">
            <div class="time"><i class="fa fa-clock-o"></i>{{ $all->created_at->format('jS M, Y g:i A') }}</div>
            <div class="comments"><i class="fa fa-credit-card-alt"></i>{{ $all->total_amount }}</div>

          </div>
        </div>
      </div>
      @endforeach
    </div>
  </section>
  <!-- Client Section-->
  
  <!-- Feeds Section-->
  
  <!-- Updates Section                                                -->
  <section class="updates no-padding-top">
    <div class="container-fluid">
      <div class="row">
        <!-- Recent Updates-->
        <div class="col-lg-4">
          <div class="recent-updates card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard6" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4">Recent Products</h3>
            </div>
            <div class="card-body no-padding">
              <!-- Item-->
              @foreach($products as $all)
              <div class="item d-flex justify-content-between">
                <div class="info d-flex">
                  <div class="icon"><i class="icon-rss-feed"></i></div>
                  <div class="title">
                    <h5>{{ $all->name }}</h5>
                    <p>{{ $all->summary }}</p>
                  </div>
                </div>
                <div class="date text-right"><strong>{{ $all->created_at->format('d') }}</strong><span>{{ $all->created_at->format('M') }}</span></div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <!-- Daily Feeds -->
        <div class="col-lg-4">
          <div class="recent-updates card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard6" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4">Recent Category</h3>
            </div>
            <div class="card-body no-padding">
              <!-- Item-->
              @foreach($categories as $all)
              <div class="item d-flex justify-content-between">
                <div class="info d-flex">
                  <div class="icon"><i class="icon-rss-feed"></i></div>
                  <div class="title">
                    <h5>{{ $all->en_title }}</h5>
                    <p>{{ $all->en_description }}</p>
                  </div>
                </div>
                <div class="date text-right"><strong>{{ $all->created_at->format('d') }}</strong><span>{{ $all->created_at->format('M') }}</span></div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <!-- Recent Activities -->
        <div class="col-lg-4">
          <div class="recent-activities card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard8" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4">Recent Blogs</h3>
            </div>
            <div class="card-body no-padding">
              @foreach($blog as $all)
              <div class="item">
                <div class="row">
                  <div class="col-4 date-holder text-right">
                    <div class="icon"><i class="icon-clock"></i></div>
                    <div class="date"> <span>{{ $all->created_at->format('g:i A') }}</span><span class="text-info">{{ $all->created_at->diffForHumans() }}</span></div>
                  </div>
                  <div class="col-8 content">
                    <h5>{{ $all->categoryId['title'] }}</h5>
                    <p>{{ $all->title }}</p>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Page Footer-->
  
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#dashboard").addClass( "active" );
  </script>
@endsection