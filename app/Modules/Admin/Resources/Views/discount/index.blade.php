@extends('layouts.admin')

@section('title' , 'Discount')

@section('content')
<!-- Page Header-->
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Discount</h2>
  </div>
</header>
<!-- Breadcrumb-->
<div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin_index') }}">Home</a></li>
    <li class="breadcrumb-item active">Discount</li>
  </ul>
</div>
<section class="tables">   
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Discount Table</h3>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Amount</th>
                  <th>Created By</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                @foreach($discount as $all)
                <tr>
                  <th scope="row">{{ $i++ }}</th>
                  <td>{{ $all->en_title }}</td>
                  <td>{{ $all->amount }}%</td>
                  <td>{{ $all->createdBy['name'] }}</td>
                  <td>
                    <a href="{{ route('admin_discount_edit' , $all->id) }}"><i class="fa fa-pencil" aria-hidden="true" title="Edit"></i></a>
                    <span></span>
                    <a href="{{ route('admin_discount_destroy' , $all->id) }}"><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div id="create_icon">
              <a href="{{ route('admin_discount_create') }}"><i class="fa fa-plus-circle fa-5x"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
        $("#product").addClass( "active" );
  </script>
@endsection