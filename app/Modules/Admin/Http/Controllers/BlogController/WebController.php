<?php

namespace App\Modules\Admin\Http\Controllers\BlogController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Lib\FileUpload;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Blog\Blog;
use App\Models\Blog\BlogCategory;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $blog = Blog::latest()->get();
        return view('admin::blog.index' , compact('blog'));
    }

    public function create()
    {
        $blog_category = BlogCategory::all();
        return view('admin::blog.create' , compact('blog_category'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $file   = $request->file('image');
        $prefix = 'blog';
        $path   = 'uploads/blog';

        $file_upload = new FileUpload;
        $upload = $file_upload->upload($file, $prefix, $path);

        if($upload['status'] == true)
        {
            $image_name = $upload['file_name'];

            $insert = new Blog;

            $insert->category_id            = $input['category_id'];
            $insert->title                  = $input['title'];
            $insert->summary                = $input['summary'];
            $insert->description            = $input['description'];
            $insert->tag                    = $input['tag'];
            $insert->image                  = $image_name;
            $insert->created_by             = $user;
            $insert->updated_by             = $user;

            $insert->save();
        }

        return Redirect::route('admin_blog_index')->with(['alert_msg' => 'Blog inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        $blog_category = BlogCategory::all();
        return view('admin::blog.edit' , compact('blog' , 'blog_category'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        $insert = Blog::find($id);
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'blog';
            $path   = 'uploads/blog';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];

                $insert->category_id            = $input['category_id'];
                $insert->title                  = $input['title'];
                $insert->summary                = $input['summary'];
                $insert->description            = $input['description'];
                $insert->tag                    = $input['tag'];
                $insert->image                  = $image_name;
                $insert->updated_by             = $user;

                $insert->update();
            }
        }
        else{
            $insert->category_id            = $input['category_id'];
            $insert->title                  = $input['title'];
            $insert->summary                = $input['summary'];
            $insert->description            = $input['description'];
            $insert->tag                    = $input['tag'];
            $insert->updated_by             = $user;

            $insert->update();
        }

        return Redirect::route('admin_blog_index')->with(['alert_msg' => 'Blog updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Blog::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Blog deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
