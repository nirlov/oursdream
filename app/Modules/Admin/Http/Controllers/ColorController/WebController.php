<?php

namespace App\Modules\Admin\Http\Controllers\ColorController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Color;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $color = Color::latest()->get();
        return view('admin::color.index' , compact('color'));
    }

    public function create()
    {
        return view('admin::color.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new Color;

        $insert->en_title               = $input['en_title'];
        $insert->en_description         = $input['en_description'];
        $insert->status                 = $input['status'];
        $insert->created_by             = $user;
        $insert->updated_by             = $user;

        $insert->save();

        return Redirect::route('admin_color_index')->with(['alert_msg' => 'Color inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $color = Color::find($id);
        return view('admin::color.edit' , compact('color'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = Color::find($id);

        $insert->en_title               = $input['en_title'];
        $insert->en_description         = $input['en_description'];
        $insert->status                 = $input['status'];
        $insert->updated_by             = $user;

        $insert->update();

        return Redirect::route('admin_color_index')->with(['alert_msg' => 'Color updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Color::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Color deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
