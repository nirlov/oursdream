<?php

namespace App\Modules\Admin\Http\Controllers\PenddingOrderController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $order = Order::where('status' , 0)->latest()->get();
        return view('admin::pendding_order.index' , compact('order'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $order = Order::find($id);
        $order->notification    = 1;
        $order->update();

        $order_product = OrderProduct::where('order_id' , $id)->get();
        $sum = OrderProduct::where('order_id' , $id)->sum('amount');

        return view('admin::pendding_order.show' , compact('order' , 'order_product' , 'sum'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->status          = 1;
        $order->notification    = 1;
        $order->update();

        return Redirect::route('admin_order_pendding_index')->with(['alert_msg' => 'Order confirmed successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Order::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Order deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
