<?php

namespace App\Modules\Admin\Http\Controllers\ConfirmOrderController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Auth;

//Models
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;
use App\Models\Profile\Profile;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $order = Order::where('status' , 1)->latest()->get();
        return view('admin::confirm_order.index' , compact('order'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $order = Order::find($id);
        $order_product = OrderProduct::where('order_id' , $id)->get();
        $sum = OrderProduct::where('order_id' , $id)->sum('amount');
        $profile = Profile::first();
        $today = date('d M,Y');

        $pdf = PDF::loadView('admin::confirm_order.show',compact('order' , 'order_product' , 'sum' , 'profile' , 'today'));
        return $pdf->download($order->code.'.pdf');

        //return view('admin::pendding_order.show' , compact('order' , 'order_product' , 'sum'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $delete = Order::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Order deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
