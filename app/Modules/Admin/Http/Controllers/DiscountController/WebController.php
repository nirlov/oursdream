<?php

namespace App\Modules\Admin\Http\Controllers\DiscountController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Discount;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $discount = Discount::latest()->get();
        return view('admin::discount.index' , compact('discount'));
    }

    public function create()
    {
        return view('admin::discount.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new Discount;

        $insert->en_title               = $input['en_title'];
        $insert->amount                 = $input['amount'];
        $insert->created_by             = $user;
        $insert->updated_by             = $user;

        $insert->save();

        return Redirect::route('admin_discount_index')->with(['alert_msg' => 'Discount inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $discount = Discount::find($id);
        return view('admin::discount.edit' , compact('discount'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = Discount::find($id);

        $insert->en_title               = $input['en_title'];
        $insert->amount                 = $input['amount'];
        $insert->updated_by             = $user;

        $insert->update();

        return Redirect::route('admin_discount_index')->with(['alert_msg' => 'Discount updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Discount::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Discount deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
