<?php

namespace App\Modules\Admin\Http\Controllers\BlogCategoryController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Blog\BlogCategory;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $category = BlogCategory::latest()->get();
        return view('admin::blog_category.index' , compact('category'));
    }

    public function create()
    {
        return view('admin::blog_category.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new BlogCategory;

        $insert->title               = $input['title'];
        $insert->description         = $input['description'];
        $insert->created_by          = $user;
        $insert->updated_by          = $user;

        $insert->save();

        return Redirect::route('admin_blog_category_index')->with(['alert_msg' => 'Blog Category inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = BlogCategory::find($id);
        return view('admin::blog_category.edit' , compact('category'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = BlogCategory::find($id);

        $insert->title               = $input['title'];
        $insert->description         = $input['description'];
        $insert->updated_by          = $user;

        $insert->update();

        return Redirect::route('admin_blog_category_index')->with(['alert_msg' => 'Blog Category updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = BlogCategory::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Blog Category deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
