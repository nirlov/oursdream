<?php

namespace App\Modules\Admin\Http\Controllers\CategoryController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Lib\FileUpload;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Category;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $category = Category::latest()->get();
        return view('admin::category.index' , compact('category'));
    }

    public function create()
    {
        return view('admin::category.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $file   = $request->file('image');
        $prefix = 'category';
        $path   = 'uploads/category';

        $file_upload = new FileUpload;
        $upload = $file_upload->upload($file, $prefix, $path);

        if($upload['status'] == true)
        {
            $image_name = $upload['file_name'];

            $insert = new Category;

            $insert->en_title               = $input['en_title'];
            $insert->en_description         = $input['en_description'];
            $insert->status                 = $input['status'];
            $insert->image                  = $image_name;
            $insert->created_by             = $user;
            $insert->updated_by             = $user;

            $insert->save();
        }

        return Redirect::route('admin_category_index')->with(['alert_msg' => 'Category inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin::category.edit' , compact('category'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        $insert = Category::find($id);
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'category';
            $path   = 'uploads/category';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];

                $insert->en_title               = $input['en_title'];
                $insert->en_description         = $input['en_description'];
                $insert->status                 = $input['status'];
                $insert->image                  = $image_name;
                $insert->updated_by             = $user;

                $insert->update();
            }
        }
        else{
            $insert->en_title               = $input['en_title'];
            $insert->en_description         = $input['en_description'];
            $insert->status                 = $input['status'];
            $insert->updated_by             = $user;

            $insert->update();
        }

        return Redirect::route('admin_category_index')->with(['alert_msg' => 'Category updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Category::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Category deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
