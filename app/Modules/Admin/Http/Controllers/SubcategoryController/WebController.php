<?php

namespace App\Modules\Admin\Http\Controllers\SubcategoryController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Lib\FileUpload;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $subcategory = Subcategory::latest()->get();
        return view('admin::subcategory.index' , compact('subcategory'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin::subcategory.create' , compact('category'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'en_title' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $file   = $request->file('image');
        $prefix = 'subcategory';
        $path   = 'uploads/subcategory';

        $file_upload = new FileUpload;
        $upload = $file_upload->upload($file, $prefix, $path);

        if($upload['status'] == true)
        {
            $image_name = $upload['file_name'];

            $insert = new Subcategory;

            $insert->category_id            = $input['category_id'];
            $insert->en_title               = $input['en_title'];
            $insert->en_description         = $input['en_description'];
            $insert->status                 = $input['status'];
            $insert->image                  = $image_name;
            $insert->created_by             = $user;
            $insert->updated_by             = $user;

            $insert->save();
        }

        return Redirect::route('admin_subcategory_index')->with(['alert_msg' => 'Subcategory inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $subcategory = Subcategory::find($id);
        $category = Category::all();
        return view('admin::subcategory.edit' , compact('category' , 'subcategory'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        $insert = Subcategory::find($id);
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'subcategory';
            $path   = 'uploads/subcategory';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];

                $insert->category_id            = $input['category_id'];
                $insert->en_title               = $input['en_title'];
                $insert->en_description         = $input['en_description'];
                $insert->status                 = $input['status'];
                $insert->image                  = $image_name;
                $insert->updated_by             = $user;

                $insert->update();
            }
        }
        else{
            $insert->category_id            = $input['category_id'];
            $insert->en_title               = $input['en_title'];
            $insert->en_description         = $input['en_description'];
            $insert->status                 = $input['status'];
            $insert->updated_by             = $user;

            $insert->update();
        }

        return Redirect::route('admin_subcategory_index')->with(['alert_msg' => 'Subcategory updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Subcategory::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Subcategory deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
