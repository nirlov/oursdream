<?php

namespace App\Modules\Admin\Http\Controllers\SearchController;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Product\Product;

class WebController extends Controller
{
    public function index($id)
    {
        $subcategory = Subcategory::where('category_id', $id)->get();
        return Response::json($subcategory);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
