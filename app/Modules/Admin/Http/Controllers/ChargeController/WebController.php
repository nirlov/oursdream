<?php

namespace App\Modules\Admin\Http\Controllers\ChargeController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Charge\ShippingCharge;
use App\Models\Charge\Vat;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function shipping()
    {
        $shipping = ShippingCharge::first();
        return view('admin::charge.shipping' , compact('shipping'));
    }

    
    public function shippingUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'in_dhaka' => 'required',
            'out_dhaka' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = ShippingCharge::find($id);

        $insert->in_dhaka               = $input['in_dhaka'];
        $insert->out_dhaka              = $input['out_dhaka'];

        $insert->save();

        return back()->with(['alert_msg' => 'Shipping Charge inserted successfully!' , 'alert_status' => 'success']);
    }

    
    public function vat()
    {
        $vat = Vat::first();
        return view('admin::charge.vat' , compact('vat'));
    }

    public function vatUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'vat' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = Vat::find($id);

        $insert->vat               = $input['vat'];

        $insert->save();

        return back()->with(['alert_msg' => 'Vat inserted successfully!' , 'alert_status' => 'success']);
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
