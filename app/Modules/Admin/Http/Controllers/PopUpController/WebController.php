<?php

namespace App\Modules\Admin\Http\Controllers\PopUpController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Lib\FileUpload;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Popup;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $popup = Popup::first();
        return view('admin::popup.create' , compact('popup'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        $insert = Popup::find($id);
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'popup';
            $path   = 'uploads/popup';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];

                $insert->title                  = $input['title'];
                $insert->summary                = $input['summary'];
                $insert->image                  = $image_name;
                $insert->updated_by             = $user;

                $insert->update();
            }
        }
        else{
            $insert->title                  = $input['title'];
            $insert->summary                = $input['summary'];
            $insert->updated_by             = $user;

            $insert->update();
        }

        return back()->with(['alert_msg' => 'Popup updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        //
    }
}
