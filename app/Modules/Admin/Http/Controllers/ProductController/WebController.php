<?php

namespace App\Modules\Admin\Http\Controllers\ProductController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Lib\FileUpload;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Product\Product;
use App\Models\Product\Color;
use App\Models\Product\Size;
use App\Models\Product\Discount;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $product = Product::latest()->get();
        return view('admin::product.index' , compact('product'));
    }

    public function create()
    {
        $category = Category::all();
        $subcategory = Subcategory::all();
        $color = Color::all();
        $size = Size::all();
        $discount = Discount::all();
        return view('admin::product.create' , compact('category' , 'subcategory' , 'color' , 'size' , 'discount'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'name' => 'required',
            'code' => 'required|unique:products',
            'image' => 'required',
            'sales_price' => 'required',
            'availability' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $file   = $request->file('image');
        $prefix = 'product';
        $path   = 'uploads/product';

        $file_upload = new FileUpload;
        $upload = $file_upload->upload($file, $prefix, $path);

        if($upload['status'] == true)
        {
            $image_name = $upload['file_name'];

            $insert = new Product;

            $insert->category_id            = $input['category_id'];
            $insert->subcategory_id         = $input['subcategory_id'];
            $insert->name                   = $input['name'];
            $insert->code                   = $input['code'];
            $insert->description            = $input['description'];
            $insert->summary                = $input['summary'];
            $insert->specification          = $input['specification'];
            $insert->sales_price            = $input['sales_price'];
            $insert->featured               = $input['featured'];
            $insert->new                    = $input['new'];
            $insert->availability           = $input['availability'];
            $insert->rating                 = $input['rating'];
            $insert->color_id               = $input['color_id'];
            $insert->size_id                = $input['size_id'];
            $insert->discount_id            = $input['discount_id'];
            $insert->thumbnail              = $image_name;
            $insert->created_by             = $user;
            $insert->updated_by             = $user;

            $insert->save();
        }

        return Redirect::route('admin_product_index')->with(['alert_msg' => 'Product inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::all();
        $subcategory = Subcategory::all();
        $color = Color::all();
        $size = Size::all();
        $discount = Discount::all();
        return view('admin::product.edit' , compact('product' , 'category' , 'subcategory' , 'color' , 'size' , 'discount'));
    }

    public function update(Request $request, $id)
    {
        $insert = Product::find($id);

        if($request->code == $insert->code){
                $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'name' => 'required',
                'image' => 'required',
                'sales_price' => 'required',
                'availability' => 'required',
            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'name' => 'required',
                'code' => 'required|unique:products',
                'image' => 'required',
                'sales_price' => 'required',
                'availability' => 'required',
            ]);
        }
         

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'product';
            $path   = 'uploads/product';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];

                $insert->category_id            = $input['category_id'];
                $insert->subcategory_id         = $input['subcategory_id'];
                $insert->name                   = $input['name'];
                $insert->code                   = $input['code'];
                $insert->description            = $input['description'];
                $insert->summary                = $input['summary'];
                $insert->specification          = $input['specification'];
                $insert->sales_price            = $input['sales_price'];
                $insert->featured               = $input['featured'];
                $insert->new                    = $input['new'];
                $insert->availability           = $input['availability'];
                $insert->rating                 = $input['rating'];
                $insert->color_id               = $input['color_id'];
                $insert->size_id                = $input['size_id'];
                $insert->discount_id            = $input['discount_id'];
                $insert->thumbnail              = $image_name;
                $insert->updated_by             = $user;

                $insert->update();
            }
        }
        else{
            $insert->category_id            = $input['category_id'];
            $insert->subcategory_id         = $input['subcategory_id'];
            $insert->name                   = $input['name'];
            $insert->code                   = $input['code'];
            $insert->description            = $input['description'];
            $insert->summary                = $input['summary'];
            $insert->specification          = $input['specification'];
            $insert->sales_price            = $input['sales_price'];
            $insert->featured               = $input['featured'];
            $insert->new                    = $input['new'];
            $insert->availability           = $input['availability'];
            $insert->rating                 = $input['rating'];
            $insert->color_id               = $input['color_id'];
            $insert->size_id                = $input['size_id'];
            $insert->discount_id            = $input['discount_id'];
            $insert->updated_by             = $user;

            $insert->update();
        }

        return Redirect::route('admin_product_index')->with(['alert_msg' => 'Product updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Product::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Product deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
