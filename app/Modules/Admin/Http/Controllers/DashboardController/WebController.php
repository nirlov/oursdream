<?php

namespace App\Modules\Admin\Http\Controllers\DashboardController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Admin;
use App\User;
use App\Models\Order\Order;
use App\Models\Product\Product;
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use App\Models\Blog\Blog;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $date = date('Y-m-d');
        $today = date('Y-m-d',strtotime($date . "+1 days"));
        $month = date('Y-m-d',strtotime($date . "-30 days"));

        $new_customer = User::whereBetween('created_at' , [$month,$today])->count();
        $total_order = Order::all()->count('id');
        $total_pendding_order = Order::where('status',0)->count('id');
        $total_confirm_order = Order::where('status',1)->count('id');

        $pendding_order = Order::where('status',0)->get();

        $product = Product::all()->count();
        $category = Category::all()->count();
        $subcategory = Subcategory::all()->count();

        $products = Product::latest()->take(5)->get();
        $categories = Category::latest()->take(5)->get();
        $subcategories = Subcategory::latest()->take(5)->get();

        $blog = Blog::latest()->take(5)->get();
        //dd($products);
        return view('admin::dashboard.index' , compact('total_order' , 'total_pendding_order' , 'total_confirm_order' , 'new_customer' , 'product' , 'category' , 'subcategory' , 'pendding_order' , 'products' , 'categories' , 'subcategories' , 'blog'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
