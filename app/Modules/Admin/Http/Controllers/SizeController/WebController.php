<?php

namespace App\Modules\Admin\Http\Controllers\SizeController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Product\Size;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $size = Size::latest()->get();
        return view('admin::size.index' , compact('size'));
    }

    public function create()
    {
        return view('admin::size.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = new Size;

        $insert->en_title               = $input['en_title'];
        $insert->en_description         = $input['en_description'];
        $insert->status                 = $input['status'];
        $insert->created_by             = $user;
        $insert->updated_by             = $user;

        $insert->save();

        return Redirect::route('admin_size_index')->with(['alert_msg' => 'Size inserted successfully!' , 'alert_status' => 'success']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $size = Size::find($id);
        return view('admin::size.edit' , compact('size'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'en_title' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;

        $input = $request->all();

        $insert = Size::find($id);

        $insert->en_title               = $input['en_title'];
        $insert->en_description         = $input['en_description'];
        $insert->status                 = $input['status'];
        $insert->updated_by             = $user;

        $insert->update();

        return Redirect::route('admin_size_index')->with(['alert_msg' => 'Size updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        $delete = Size::find($id);

        if($delete->delete())
        {
            return Redirect::back()->with(['alert_msg' => 'Size deleted successfully!' , 'alert_status' => 'danger']);
        }
    }
}
