<?php

namespace App\Modules\Admin\Http\Controllers\ProfileController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Profile\Profile;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id)
    {
        $profile = Profile::first();
        return view('admin::profile.edit' , compact('profile'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'display_name' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Auth::user()->id;
        $insert = Profile::first();
        $input = $request->all();

        if(!empty($input['image'])){
            $file   = $request->file('image');
            $prefix = 'category';
            $path   = 'uploads/category';

            $file_upload = new FileUpload;
            $upload = $file_upload->upload($file, $prefix, $path);

            if($upload['status'] == true)
            {
                $image_name = $upload['file_name'];
                $insert->logo                  = $image_name;
            }
        }
        
            $insert->display_name         = $input['display_name'];
            $insert->company_name         = $input['company_name'];
            $insert->address              = $input['address'];
            $insert->website              = $input['website'];
            $insert->number               = $input['number'];
            $insert->email                = $input['email'];
            $insert->facebook_link        = $input['facebook_link'];
            $insert->twitter_link         = $input['twitter_link'];
            $insert->google_link          = $input['google_link'];
            $insert->about_us             = $input['about_us'];
            $insert->working_hour         = $input['working_hour'];
            $insert->address              = $input['address'];
            $insert->address              = $input['address'];

            $insert->update();
        

        return Redirect::back()->with(['alert_msg' => 'Profile updated successfully!' , 'alert_status' => 'success']);
    }

    public function destroy($id)
    {
        //
    }
}
