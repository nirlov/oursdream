<?php

namespace App;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

//Model
use App\Models\Profile\Profile;
use App\Models\Product\Category;
use App\Models\Product\Product;

class Information
{
    
    public function getInfo()
    {
    	return Profile::first();
    }

    public function getCategory()
    {
        return Category::all();
    }

    public function cartProduct()
    {
        if(session('cart_product')){
            $cart_product = Session('cart_product'); 
            $total_cart_product = Session('cart_product'); 
            $cart_product = array_unique($cart_product);
            sort($cart_product);
            $product=[];

            foreach($cart_product as $all)
            {
                $quantity = count(array_keys($total_cart_product, $all));
                $products = Product::find($all);

                if($products->discountId != Null){
                    $discount           = $products->discountId->amount;
                    $discount_price     = (integer)(($products->sales_price * $discount)/100);
                    $new_price          = ($products->sales_price - $discount_price);
                }
                else{
                    $new_price          = $products->sales_price;
                }

                $products->unit_price   = $new_price;
                $products->quantity     = $quantity;
                $products->sub_total    = ($new_price * $quantity);
                $product[]              = $products;
            }

            $sum = array_sum(array_column($product, 'sub_total'));
            
            return $product;
        }
    }

}