<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>@yield('title')</title>    

        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Porto - Responsive HTML5 Template">
        <meta name="author" content="okler.net">

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('uploads/logo/icon.png') }}" type="image/x-icon" />
        <link rel="apple-touch-icon" href="{{ asset('uploads/logo/icon.png') }}">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/animate/animate.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/simple-line-icons/css/simple-line-icons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/owl.carousel/assets/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/magnific-popup/magnific-popup.min.css') }}">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/css/theme.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/theme-elements.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/theme-blog.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/theme-shop.css') }}">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/vendor/rs-plugin/css/settings.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/rs-plugin/css/layers.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/vendor/rs-plugin/css/navigation.css') }}">

        <!-- Skin CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/css/skins/skin-shop-2.css') }}"> 

        <!-- Demo CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/css/demos/demo-shop-2.css') }}">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">

        <!-- Head Libs -->
        <script src="{{ asset('frontend/vendor/modernizr/modernizr.min.js') }}"></script>

        <!-- Ajax -->
         <script src="{{ asset('frontend/jquery/jquery.min.js') }}"></script>

         <style type="text/css">
             #result {
               position: absolute;
               width: 23%;
               max-width:870px;
               cursor: pointer;
               overflow-y: auto;
               max-height: 400px;
               box-sizing: border-box;
               z-index: 1001;
               right:170px;
              }
         </style>

        @yield('styles')

    </head>
    <body>

        <div class="body">
           
            @include('partials.frontend.header')

            @include('partials.frontend.mobilenav')
            
            <div id="mobile-menu-overlay"></div>

            <div role="main" class="main">
            
            <!-- Content -->

            @yield('content')

            <!-- End Content -->

            @include('partials.frontend.notification')

            </div>

            @include('partials.frontend.footer')


        </div>

        <!-- Vendor -->
        <script src="{{ asset('frontend/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/common/common.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/isotope/jquery.isotope.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/vide/vide.min.js') }}"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('frontend/js/theme.js') }}"></script>


        <script src="{{ asset('frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{ asset('frontend/js/views/view.contact.js') }}"></script>


        <script src="{{ asset('frontend/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.js') }}"></script>
        <script src="{{ asset('frontend/vendor/elevatezoom/jquery.elevatezoom.js') }}"></script>
  

        <!-- Demo -->
        <script src="{{ asset('frontend/js/demos/demo-shop-2.js') }}"></script>

        
        <!-- Theme Custom -->
        <script src="{{ asset('frontend/js/custom.js') }}"></script>
        
        <!-- Theme Initialization Files -->
        <script src="{{ asset('frontend/js/theme.init.js') }}"></script>

        <script src="{{ asset('frontend/vendor/nouislider/nouislider.min.js') }}"></script>

        <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-581b726c069c6315"></script>

        <script type="text/javascript">
            $("#search_product").keyup(function(e){
                $('#result').show();
                var search = e.target.value;
                $('#result').html('');
                
                $.get("{{ route('search_all_product' , ['search'=>'']) }}/"+search , function(data){
                    //console.log(data);
                    
                    $.each(data , function(index , value){

                        $('#result').append('<li class="list-group-item"><a href="{!! url("product-details/'+value.id+'") !!}"><img src="{!! url("/uploads/product/'+value.thumbnail+'") !!}" height="40" width="40" class="img-thumbnail" /><font color="gray">  '+ value.name +'</font></a></li>')

                    });
                    
                }); 
            });
        </script>

        @yield('scripts')



        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->


    </body>
</html>
