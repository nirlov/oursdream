@inject('info' , 'App\AdminInformation')

<header class="header">
  <nav class="navbar">
    <!-- Search Box-->
    <div class="search-box">
      <button class="dismiss"><i class="icon-close"></i></button>
      <form id="searchForm" action="#" role="search">
        <input type="search" placeholder="What are you looking for..." class="form-control">
      </form>
    </div>
    <div class="container-fluid">
      <div class="navbar-holder d-flex align-items-center justify-content-between">
        <!-- Navbar Header-->
        <div class="navbar-header">
          <!-- Navbar Brand --><a href="{{ route('admin_index') }}" class="navbar-brand">
            <div class="brand-text brand-big"><span>Our's Dream </span><strong> Dashboard</strong></div>
            <div class="brand-text brand-small"><strong>Dream's Here</strong></div></a>
          <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
        </div>
        <!-- Navbar Menu -->
        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
          <!-- Search-->
          <li class="nav-item d-flex align-items-center"><a id="search" href="#"><i class="icon-search"></i></a></li>
          <!-- Notifications-->
          <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell-o"></i><span class="badge bg-red">{{ count($info->notification()) }}</span></a>
            <ul aria-labelledby="notifications" class="dropdown-menu">
              @foreach($info->notification() as $all)
                <li><a rel="nofollow" href="{{ route('admin_order_pendding_show' , $all->id) }}" class="dropdown-item"> 
                  <div class="notification">
                    <div class="notification-content"><i class="fa fa-upload bg-green"></i>Order Code - {{ $all->code}} || ৳{{ $all->total_amount }}</div>
                    <div class="notification-time"><small>{{ $all->created_at->diffForHumans() }}</small></div>
                  </div></a>
                </li>
              @endforeach
              <li><a rel="nofollow" href="{{ route('admin_order_pendding_index') }}" class="dropdown-item all-notifications text-center"> <strong>View all pendding order                                            </strong></a></li>
            </ul>
          </li>
          <!-- Messages                        -->
          <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-user-plus"></i><span class="badge bg-red">{{ count($info->customer()) }}</span></a>
            <ul aria-labelledby="notifications" class="dropdown-menu">
              @foreach($info->customer() as $all)
                <li><a rel="nofollow" href="{{ route('admin_customer_show' , $all->id) }}" class="dropdown-item"> 
                  <div class="notification">
                    <div class="notification-content"><i class="fa fa-address-card bg-green"></i>{{ $all->name }} , {{ $all->country }}   <img src="{{ asset('uploads/customer/'.$all->image) }}" style="width: 30px; height: auto;"></div>
                    <div class="notification-time"><small>{{ $all->created_at->diffForHumans() }}</small></div>
                  </div></a>
                </li>
              @endforeach
              <li><a rel="nofollow" href="{{ route('admin_customer_index') }}" class="dropdown-item all-notifications text-center"> <strong>View all customes                                            </strong></a></li>
            </ul>
          </li>
          <!-- Logout    -->
          <li class="nav-item"><a href="{{ route('admin_logout') }}" class="nav-link logout" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">Logout<i class="fa fa-sign-out"></i></a></li>
          <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        </ul>
      </div>
    </div>
  </nav>
</header>