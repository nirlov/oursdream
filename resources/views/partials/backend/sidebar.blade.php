@inject('info' , 'App\AdminInformation')

<nav class="side-navbar">
          <!-- Sidebar Header-->
  <div class="sidebar-header d-flex align-items-center">
    <div class="avatar"><img src="{{ asset('uploads/admin/' . $info->getInfo()->image) }}" alt="..." class="img-fluid rounded-circle"></div>
    <div class="title">
      <h1 class="h4">{{ $info->getInfo()->name }}</h1>
      <p>Web Developer</p>
    </div>
  </div>
  <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
  <ul class="list-unstyled">
    <li id="dashboard"> <a href="{{ route('admin_index') }}"><i class="icon-home"></i>Home</a></li>
    <li id="product"><a href="#dashvariants" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Product </a>
      <ul id="dashvariants" class="collapse list-unstyled">
        <li><a href="{{ route('admin_category_index') }}">Category</a></li>
        <li><a href="{{ route('admin_subcategory_index') }}">Subcategory</a></li>
        <li><a href="{{ route('admin_product_index') }}">Product</a></li>
        <li><a href="{{ route('admin_color_index') }}">Color</a></li>
        <li><a href="{{ route('admin_size_index') }}">Size</a></li>
        <li><a href="{{ route('admin_discount_index') }}">Discount</a></li>
      </ul>
    </li>
    <li id="order"><a href="#ordermain" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-bar-chart"></i>Order </a>
      <ul id="ordermain" class="collapse list-unstyled">
        <li><a href="{{ route('admin_order_pendding_index') }}">Pendding Order</a></li>
        <li><a href="{{ route('admin_order_confirm_index') }}">Confirme Order</a></li>
      </ul>
    </li>
    <li id="customer"> <a href="{{ route('admin_customer_index') }}"> <i class="fa fa-user"></i>Customer </a></li>

  </ul>
  <span class="heading">Extras</span>
  <ul class="list-unstyled">
    <li id="blog"><a href="#blog2" aria-expanded="false" data-toggle="collapse"> <i class="icon-padnote"></i>Blog </a>
      <ul id="blog2" class="collapse list-unstyled">
        <li><a href="{{ route('admin_blog_category_index') }}">Blog Category</a></li>
        <li><a href="{{ route('admin_blog_index') }}">Blog</a></li>
      </ul>
    </li>
    <li id="profile"> <a href="{{ route('admin_profile_index',1) }}"> <i class="fa fa-address-book-o"></i>Profile </a></li>
    <li id="popup"> <a href="{{ route('admin_popup_index') }}"> <i class="fa fa-file-image-o"></i>Popup Image </a></li>
  </ul>
  <span class="heading">Charge</span>
  <ul class="list-unstyled">
    <li id="shipping"> <a href="{{ route('admin_shipping_index') }}"> <i class="fa fa-truck"></i>Shipping Charge </a></li>
    <li id="vat"> <a href="{{ route('admin_vat_index') }}"> <i class="fa fa-money"></i>Vat </a></li>
  </ul>
</nav>