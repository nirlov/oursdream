<footer class="main-footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <p>Our's Dream &copy; 2017-2019</p>
      </div>
      <div class="col-sm-6 text-right">
        <p>Developed by <a href="javascript::void(0)" class="external">@Raj</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
</footer>