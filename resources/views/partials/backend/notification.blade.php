@if(Session::has('alert_msg') && Session::has('alert_status'))
<div class="alert alert-{{ Session::get('alert_status') }} alert-dismissable">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	<strong>{{ Session::get('alert_msg') }}</strong>
</div>
@endif