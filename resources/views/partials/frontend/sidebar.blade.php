<div class="col-md-3 col-md-pull-9">
    <aside class="sidebar">

        <h4>My Account</h4>
        <ul class="nav nav-list">
            <li id="dashboard"><a href="{{ route('my_account_dashboard_index') }}">Account Dashboard</a></li>
            <li id="information"><a href="{{ route('my_account_index') }}">Account Information</a></li>
            <li id="order"><a href="{{ route('my_account_order_index') }}">My Orders</a></li>
            <li id="password"><a href="{{ route('change_my_password') }}">Change Password</a></li>
            <li id="wishlist"><a href="{{ route('wishlist_show') }}">My Wishlist</a></li>
            <li id="review"><a href="{{ route('my_product_review') }}">My Product Reviews</a></li>
            <li><a href="{{ route('comminig_soon') }}">Recurring Profiles</a></li>
            <li><a href="{{ route('comminig_soon') }}">Address Book</a></li>   
            <li><a href="{{ route('comminig_soon') }}">My Tags</a></li>
            <li><a href="{{ route('comminig_soon') }}">My Applications</a></li>
            <li><a href="{{ route('comminig_soon') }}">Newsletter Subscriptions</a></li>
            <li><a href="{{ route('comminig_soon') }}">My Downloadable Products</a></li>
        </ul>

    </aside>
</div>