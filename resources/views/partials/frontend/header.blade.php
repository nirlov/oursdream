@inject('info' , 'App\Information')

<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 106, 'stickySetTop': '0', 'stickyChangeLogo': false}" class="header-full-width">
    <div class="header-body">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="{{ route('home') }}">
                            <img alt="Porto" width="111" height="50" src="{{ asset('uploads/logo/'. $info->getInfo()->logo ) }}">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="row">
                        <div class="header-nav-main">
                            <nav>
                                <ul class="nav nav-pills" id="mainNav">
                                    <li class="dropdown">
                                        <a class="" href="{{ route('home') }}">
                                            Home
                                        </a>
                                        
                                    </li>
                                    <li class="dropdown dropdown-mega-small">
                                        <a href="demo-shop-2-category-4col.html" class="dropdown-toggle">
                                            Fashion <span class="tip tip-new">New</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <div class="dropdown-mega-content dropdown-mega-content-small">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="row">
                                                                @foreach($info->getCategory() as $all)
                                                                <div class="col-md-6">
                                                                    <a href="{{ route('category_product' , $all->id) }}" class="dropdown-mega-sub-title">{{ $all->en_title }}<span class="tip tip-hot">Hot!</span></a>
                                                                    <ul class="dropdown-mega-sub-nav">
                                                                        @foreach($all->subcategoryId as $sub)
                                                                        <li><a href="{{ route('subcategory_product' , $sub->id) }}">{{ $sub->en_title }}</a></li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div class="dropdowns-container">

                            <div class="header-dropdown lang-dropdown">
                                <a href="#">English <i class="fa fa-caret-down"></i></a>

                                <ul class="header-dropdownmenu">
                                    <li><a href="javascript:void(0);">English</a></li>
                                    <li><a href="javascript:void(0);">Bangla</a></li>
                                </ul>
                            </div>
                            
                            <div class="cart-dropdown">
                                <a href="{{ route('cart_index') }}" class="cart-dropdown-icon">
                                    <i class="minicart-icon"></i>
                                    <span class="cart-info">
                                        <span class="cart-qty">
                                            @if(Session::has('cart_product'))
                                                {{ count(session('cart_product')) }}
                                            @else
                                                0
                                            @endif
                                        </span>
                                    </span>
                                </a>

                                @if(Session::has('cart_product'))
                                <div class="cart-dropdownmenu right">
                                    <div class="dropdownmenu-wrapper">
                                        <div class="cart-products" style="height: 301px;overflow-y: scroll;">
                                            @foreach($info->cartProduct() as $all)
                                            <div class="product product-sm">
                                                <figure class="product-image-area">
                                                    <a href="{{ route('product_details' , $all->id) }}" title="{{ $all->name }}" class="product-image">
                                                        <img src="{{ asset('uploads/product/'. $all->thumbnail ) }}" alt="{{ $all->name }}">
                                                    </a>
                                                </figure>
                                                <div class="product-details-area">
                                                    <h2 class="product-name"><a href="{{ route('product_details' , $all->id) }}" title="Product Name">{{ $all->name }}</a></h2>

                                                    <div class="cart-qty-price">
                                                        {{ $all->quantity }} X 
                                                        <span class="product-price">৳{{ $all->unit_price }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>

                                        <div class="cart-actions">
                                            <a href="{{ route('cart_index') }}" class="btn btn-primary">View Cart</a>
                                            <a href="{{ route('checkout_index') }}" class="btn btn-primary">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>  

                        <a href="#" class="mmenu-toggle-btn" title="Toggle menu">
                            <i class="fa fa-bars"></i>
                        </a>

                        <div class="header-search">
                            <a href="#" class="search-toggle"><i class="fa fa-search"></i></a>
                            {{ Form::open(['url' => route('search_products_index'), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'user_edit_form' , 'onsubmit' => 'return searchProduct();']) }}
                                <div class="header-search-wrapper">
                                    <input type="text" class="form-control" name="search" id="search_product" placeholder="Search..." autocomplete="off" required>
                                    
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            {{ Form::close() }}
                        </div>

                        <div class="top-menu-area">
                            <a href="#">Links <i class="fa fa-caret-down"></i></a>
                            <ul class="top-menu">
                                <li><font color="white">Welcome To {{ $info->getInfo()->display_name }}</font></li>
                                <li><a href="{{ route('my_account_index') }}">My Account</a></li>
                                <li><a href="{{ route('comminig_soon') }}">Daily Deal</a></li>
                                <li><a href="{{ route('wishlist_show') }}">My Wishlist</a></li>
                                <li><a href="{{ route('blog_index') }}">Blog</a></li>
                                @if (Route::has('login'))
                                    @auth
                                    <li><a href="{{ route('my_account_dashboard_index') }}">Hi, {{ Auth::user()->name }}</a></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Logout</a></li>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    @else
                                    <li><a href="{{ route('login') }}">Log in</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                    @endauth
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<ul class="list-group" id="result" hidden></ul>
