@if(session()->has('alert.message'))
	<div style="position: fixed; bottom: 15px; right: 0;">
	    <div class="alert alert-tertiary alert-dismissible" role="alert">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	        <strong>{{ session('alert.message') }}.</strong>
	    </div>
	</div>
@endif
