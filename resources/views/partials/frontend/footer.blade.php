@inject('info' , 'App\Information')

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4>My Account</h4>
                <ul class="links">
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="{{ route('about_index') }}">About Us</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="{{ route('contact_index') }}">Contact Us</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="{{ route('comminig_soon') }}">My account</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="{{ route('comminig_soon') }}">Orders history</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="{{ route('comminig_soon') }}">Advanced search</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="contact-details">
                    <h4>Contact Information</h4>
                    <ul class="contact">
                        <li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong><br> {{ $info->getInfo()->address }}</p></li>
                        <li><p><i class="fa fa-phone"></i> <strong>Phone:</strong><br> {{ $info->getInfo()->number }}</p></li>
                        <li><p><i class="fa fa-envelope-o"></i> <strong>Email:</strong><br> <a href="mailto:{{ $info->getInfo()->email }}">{{ $info->getInfo()->email }}</a></p></li>
                        <li><p><i class="fa fa-clock-o"></i> <strong>Working Days/Hours:</strong><br> {{ $info->getInfo()->working_hour }}</p></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h4>Main Features</h4>
                <ul class="features">
                    <li>
                        <i class="fa fa-check"></i>
                        <a href="#">Super Fast Template</a>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <a href="#">1st Seller Template</a>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <a href="#">19 Unique Shop Layouts</a>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <a href="#">Powerful Template Features</a>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <a href="#">Mobile &amp; Retina Optimized</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="newsletter">
                    <h4>Be the First to Know</h4>
                    <p class="newsletter-info">Get all the latest information on Events,<br> Sales and Offers. Sign up for newsletter today.</p>

                    <div class="alert alert-success hidden" id="newsletterSuccess">
                        <strong>Success!</strong> You've been added to our email list.
                    </div>

                    <div class="alert alert-danger hidden" id="newsletterError"></div>


                    <p>Enter your e-mail Address:</p>
                    <form id="newsletterForm" action="../php/newsletter-subscribe.php" method="POST">
                        <div class="input-group">
                            <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <a href="{{ route('home') }}" class="logo">
                <img alt="Porto Website Template" class="img-responsive" src="{{ asset('uploads/logo/'. $info->getInfo()->logo ) }}" style="height: 40px; width: 80px;">
            </a>
            <ul class="social-icons">
                <li class="social-icons-facebook"><a href="{{ $info->getInfo()->facebook_link }}" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="{{ $info->getInfo()->twitter_link }}" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="{{ $info->getInfo()->google_link }}" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <img alt="Payments" src="{{ asset('frontend/img/demos/shop/payments.png') }}" class="footer-payment">
            <p class="copyright-text">© Copyright 2017. All Rights Reserved.</p>
        </div>
    </div>
</footer>