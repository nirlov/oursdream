-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2017 at 06:33 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerceuiu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `image`, `password`, `activated`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin.jpg', '$2y$10$LpvGUPnU52QJZV4i5p5sBOZehvke5vxItGnZwQWW8CqKITeddJcPu', 1, 'iPjfoEa316tEVCl8QnbRcrbS3P9PgdqozeLUqxHboxTL3rgCxHBQ17LcKjPE', '2017-10-20 18:00:00', '2017-10-20 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `summary`, `description`, `image`, `tag`, `category_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'THE 20 MOST INFLUENTIAL PERSONAL STYLE BLOGGERS RIGHT NOW', 'Personal style bloggers sometimes get a bad rap -- caricatured as pretty, brainless girls who dress up for their camera-wielding boyfriends and post their results to a WordPress blog.', 'Personal style bloggers sometimes get a bad rap -- caricatured as pretty, brainless girls who dress up for their camera-wielding boyfriends and post their results to a WordPress blog. But over the past decade, these independent publishers have become a real force in the fashion industry -- not just snapping up front row seats at fashion shows, but landing major campaigns and collaborations with brands, becoming regular guests on TV shows like \"Today\" and \"America\'s Next Top Model,\" and turning their blogs into multimillion-dollar businesses. Some have become household names.', 'blog_1.jpg', 'Good', 1, 1, 1, '2017-10-29 18:00:00', '2017-10-29 18:00:00'),
(2, 'THE ULTIMATE LIST OF BEST ACNE MASKS FOR THE CLEAREST SKIN OF YOUR LIFE', 'Trust me when I say I’ve got you covered in the realm of detox facials.', 'The Yes To line always does me well.  This mask is a staple at a super-friendly price point (like $16 friendly), making it tempting to keep an extra one in the shower or even leave one at your S.O.’s house. Tomato is a natural revitalizer and astringent (which means it kills acne-causing bacteria) to remind your pores who’s boss. The mask is nice and fluffy too, and I like to apply with a heavy hand on my neck and arms. The best part is you can be on your way to glorious skin in less than ten minutes.', 'blog_2.jpg', 'makeup', 2, 1, 1, '2017-10-29 18:00:00', '2017-10-29 18:00:00'),
(3, '4 Fashion Blogs To Find On-Trend Accessories.', 'ASD Market Week names the four must read fashion accessories blogs that all retail and fashion accessories buyers should have bookmarked.', 'Sometimes making the sale is all about having the right product at the right price (and at the right time).  In order to do so, you have to know what trend is just about to break in the market. If you’re a fashion accessories buyer (or enthusiast) one of the best ways to cull wholesale product trends is to search online.  It’s also one of the best ways to prepare to shop a trade show, where you can physically shop hundreds of companies all in one place. But where do you look? We’ve done some of the digital heavy lifting for you by selecting four blogs below that will go a long way towards helping you make your product wish list.', 'blog_3.jpg', 'Assessories', 3, 1, 1, '2017-10-29 18:00:00', '2017-10-29 18:00:00'),
(4, 'THE 22 FASHION BLOGGER INSTAGRAMS TO FOLLOW NOW', 'With the amount of traveling I’ve been doing lately, there’s nothing I love more than spending time at home, with James, Stella, and Thomas.', 'The other day a friend of mine said to me that this time of year – Autumn in New York – is her favorite time of year, because she could finally layer again. I agreed with her, yet I dread the idea of having winter on my doorstep. I mean, I love layer too, but New York winters can be terribly cold, and I find it hard to step out in anything else that a “sleeping bag” aka a puffer coat that won’t even leave my ankles out to freeze… And in those spirits I started my research on “the perfect winter-proof coats” that are not only functional, but also absolute must-haves this season!', 'blog_4.jpg', 'Fashion', 4, 1, 1, '2017-10-29 18:00:00', '2017-10-29 18:00:00'),
(5, 'How To Make Your Curls Last Longer: 5 Tips', 'Oh how we love those beautiful, flawless, bouncy curls. But you know what\'s even better? Beautiful, flawless, curled hair that lasts all day.', 'Today we\'re sharing our favourite tips and tricks with you on how to curl your hair to make it last all day! By the way, all these tips also work for hair extensions as well. You can either watch our tutorial below, or keep scrolling down for all of our tips and tricks.', 'blog_5.jpg', 'Hair Cuts', 5, 1, 1, '2017-10-29 18:00:00', '2017-10-29 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `title`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Clothing', 'With top-tier fashion bloggers raking in multimillion-dollar campaigns, it’s no wonder every self-proclaimed fashionista wants to launch a personal style site.', 1, 1, '2017-10-28 18:00:00', '2017-10-28 18:00:00'),
(2, 'Make-up & Beauty', 'If you’re anything like me, one of your favorite pasttimes, amongst Netflix bingeing and cake baking, is looking at fashion blogs and borderline-stalking bloggers on their websites and social media accounts for outfit inspo.', 1, 1, '2017-10-28 18:00:00', '2017-10-28 18:00:00'),
(3, 'Assessories', 'If you’re anything like me, one of your favorite pasttimes, amongst Netflix bingeing and cake baking, is looking at fashion blogs and borderline-stalking bloggers on their websites and social media accounts for outfit inspo.', 1, 1, '2017-10-28 18:00:00', '2017-10-28 18:00:00'),
(4, 'Fashion', 'If you’re anything like me, one of your favorite pasttimes, amongst Netflix bingeing and cake baking, is looking at fashion blogs and borderline-stalking bloggers on their websites and social media accounts for outfit inspo.', 1, 1, '2017-10-28 18:00:00', '2017-10-28 18:00:00'),
(5, 'Haircuts & Hairstyles', 'If you’re anything like me, one of your favorite pasttimes, amongst Netflix bingeing and cake baking, is looking at fashion blogs and borderline-stalking bloggers on their websites and social media accounts for outfit inspo.', 1, 1, '2017-10-28 18:00:00', '2017-10-28 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `en_title`, `en_description`, `image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Man', 'All products are good', 'category_banner.png', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(2, 'Women', 'All Products are good', 'category_banner.png', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(3, 'Kids', 'All Products are good', 'category_banner.png', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(4, 'Electronics', 'All Products are good', 'category_banner.png', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(5, 'Mobile & Tabs', 'All Products are good', 'category_banner.png', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `en_title`, `en_description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Red', 'Red', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00'),
(2, 'Black', 'Black', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00'),
(3, 'White', 'White', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `en_title`, `amount`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '10% Discount', '10', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `bn_name`, `lat`, `lon`, `website`) VALUES
(1, 'Dhaka', 'ঢাকা', 23.7115253, 90.4111451, 'www.dhaka.gov.bd'),
(2, 'Faridpur', 'ফরিদপুর', 23.6070822, 89.8429406, 'www.faridpur.gov.bd'),
(3, 'Gazipur', 'গাজীপুর', 24.0022858, 90.4264283, 'www.gazipur.gov.bd'),
(4, 'Gopalganj', 'গোপালগঞ্জ', 23.0050857, 89.8266059, 'www.gopalganj.gov.bd'),
(5, 'Jamalpur', 'জামালপুর', 24.937533, 89.937775, 'www.jamalpur.gov.bd'),
(6, 'Kishoreganj', 'কিশোরগঞ্জ', 24.444937, 90.776575, 'www.kishoreganj.gov.bd'),
(7, 'Madaripur', 'মাদারীপুর', 23.164102, 90.1896805, 'www.madaripur.gov.bd'),
(8, 'Manikganj', 'মানিকগঞ্জ', 0, 0, 'www.manikganj.gov.bd'),
(9, 'Munshiganj', 'মুন্সিগঞ্জ', 0, 0, 'www.munshiganj.gov.bd'),
(10, 'Mymensingh', 'ময়মনসিং', 0, 0, 'www.mymensingh.gov.bd'),
(11, 'Narayanganj', 'নারায়াণগঞ্জ', 23.63366, 90.496482, 'www.narayanganj.gov.bd'),
(12, 'Narsingdi', 'নরসিংদী', 23.932233, 90.71541, 'www.narsingdi.gov.bd'),
(13, 'Netrokona', 'নেত্রকোনা', 24.870955, 90.727887, 'www.netrokona.gov.bd'),
(14, 'Rajbari', 'রাজবাড়ি', 23.7574305, 89.6444665, 'www.rajbari.gov.bd'),
(15, 'Shariatpur', 'শরীয়তপুর', 0, 0, 'www.shariatpur.gov.bd'),
(16, 'Sherpur', 'শেরপুর', 25.0204933, 90.0152966, 'www.sherpur.gov.bd'),
(17, 'Tangail', 'টাঙ্গাইল', 0, 0, 'www.tangail.gov.bd'),
(18, 'Bogra', 'বগুড়া', 24.8465228, 89.377755, 'www.bogra.gov.bd'),
(19, 'Joypurhat', 'জয়পুরহাট', 0, 0, 'www.joypurhat.gov.bd'),
(20, 'Naogaon', 'নওগাঁ', 0, 0, 'www.naogaon.gov.bd'),
(21, 'Natore', 'নাটোর', 24.420556, 89.000282, 'www.natore.gov.bd'),
(22, 'Nawabganj', 'নবাবগঞ্জ', 24.5965034, 88.2775122, 'www.chapainawabganj.gov.bd'),
(23, 'Pabna', 'পাবনা', 23.998524, 89.233645, 'www.pabna.gov.bd'),
(24, 'Rajshahi', 'রাজশাহী', 0, 0, 'www.rajshahi.gov.bd'),
(25, 'Sirajgonj', 'সিরাজগঞ্জ', 24.4533978, 89.7006815, 'www.sirajganj.gov.bd'),
(26, 'Dinajpur', 'দিনাজপুর', 25.6217061, 88.6354504, 'www.dinajpur.gov.bd'),
(27, 'Gaibandha', 'গাইবান্ধা', 25.328751, 89.528088, 'www.gaibandha.gov.bd'),
(28, 'Kurigram', 'কুড়িগ্রাম', 25.805445, 89.636174, 'www.kurigram.gov.bd'),
(29, 'Lalmonirhat', 'লালমনিরহাট', 0, 0, 'www.lalmonirhat.gov.bd'),
(30, 'Nilphamari', 'নীলফামারী', 25.931794, 88.856006, 'www.nilphamari.gov.bd'),
(31, 'Panchagarh', 'পঞ্চগড়', 26.3411, 88.5541606, 'www.panchagarh.gov.bd'),
(32, 'Rangpur', 'রংপুর', 25.7558096, 89.244462, 'www.rangpur.gov.bd'),
(33, 'Thakurgaon', 'ঠাকুরগাঁও', 26.0336945, 88.4616834, 'www.thakurgaon.gov.bd'),
(34, 'Barguna', 'বরগুনা', 0, 0, 'www.barguna.gov.bd'),
(35, 'Barisal', 'বরিশাল', 0, 0, 'www.barisal.gov.bd'),
(36, 'Bhola', 'ভোলা', 22.685923, 90.648179, 'www.bhola.gov.bd'),
(37, 'Jhalokati', 'ঝালকাঠি', 0, 0, 'www.jhalakathi.gov.bd'),
(38, 'Patuakhali', 'পটুয়াখালী', 22.3596316, 90.3298712, 'www.patuakhali.gov.bd'),
(39, 'Pirojpur', 'পিরোজপুর', 0, 0, 'www.pirojpur.gov.bd'),
(40, 'Bandarban', 'বান্দরবান', 22.1953275, 92.2183773, 'www.bandarban.gov.bd'),
(41, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 23.9570904, 91.1119286, 'www.brahmanbaria.gov.bd'),
(42, 'Chandpur', 'চাঁদপুর', 23.2332585, 90.6712912, 'www.chandpur.gov.bd'),
(43, 'Chittagong', 'চট্টগ্রাম', 22.335109, 91.834073, 'www.chittagong.gov.bd'),
(44, 'Comilla', 'কুমিল্লা', 23.4682747, 91.1788135, 'www.comilla.gov.bd'),
(45, 'Cox\'s Bazar', 'কক্স বাজার', 0, 0, 'www.coxsbazar.gov.bd'),
(46, 'Feni', 'ফেনী', 23.023231, 91.3840844, 'www.feni.gov.bd'),
(47, 'Khagrachari', 'খাগড়াছড়ি', 23.119285, 91.984663, 'www.khagrachhari.gov.bd'),
(48, 'Lakshmipur', 'লক্ষ্মীপুর', 22.942477, 90.841184, 'www.lakshmipur.gov.bd'),
(49, 'Noakhali', 'নোয়াখালী', 22.869563, 91.099398, 'www.noakhali.gov.bd'),
(50, 'Rangamati', 'রাঙ্গামাটি', 0, 0, 'www.rangamati.gov.bd'),
(51, 'Habiganj', 'হবিগঞ্জ', 24.374945, 91.41553, 'www.habiganj.gov.bd'),
(52, 'Maulvibazar', 'মৌলভীবাজার', 24.482934, 91.777417, 'www.moulvibazar.gov.bd'),
(53, 'Sunamganj', 'সুনামগঞ্জ', 25.0658042, 91.3950115, 'www.sunamganj.gov.bd'),
(54, 'Sylhet', 'সিলেট', 24.8897956, 91.8697894, 'www.sylhet.gov.bd'),
(55, 'Bagerhat', 'বাগেরহাট', 22.651568, 89.785938, 'www.bagerhat.gov.bd'),
(56, 'Chuadanga', 'চুয়াডাঙ্গা', 23.6401961, 88.841841, 'www.chuadanga.gov.bd'),
(57, 'Jessore', 'যশোর', 23.16643, 89.2081126, 'www.jessore.gov.bd'),
(58, 'Jhenaidah', 'ঝিনাইদহ', 23.5448176, 89.1539213, 'www.jhenaidah.gov.bd'),
(59, 'Khulna', 'খুলনা', 22.815774, 89.568679, 'www.khulna.gov.bd'),
(60, 'Kushtia', 'কুষ্টিয়া', 23.901258, 89.120482, 'www.kushtia.gov.bd'),
(61, 'Magura', 'মাগুরা', 23.487337, 89.419956, 'www.magura.gov.bd'),
(62, 'Meherpur', 'মেহেরপুর', 23.762213, 88.631821, 'www.meherpur.gov.bd'),
(63, 'Narail', 'নড়াইল', 23.172534, 89.512672, 'www.narail.gov.bd'),
(64, 'Satkhira', 'সাতক্ষীরা', 0, 0, 'www.satkhira.gov.bd');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_11_000000_create_admins_table', 2),
(4, '2017_10_19_174502_create_categories_table', 2),
(5, '2017_10_20_175929_create_subcategories_table', 2),
(6, '2017_10_20_175945_create_colors_table', 2),
(7, '2017_10_20_180437_create_sizes_table', 2),
(8, '2017_10_20_180458_create_products_table', 2),
(9, '2017_10_20_180529_create_product_images_table', 2),
(10, '2017_10_21_161708_create_discounts_table', 3),
(11, '2017_10_21_164858_create_sliders_table', 4),
(12, '2017_10_27_153918_create_vats_table', 5),
(13, '2017_10_27_154051_create_shipping_charges_table', 5),
(14, '2017_10_27_174909_create_orders_table', 6),
(15, '2017_10_27_175244_create_order_products_table', 6),
(16, '2017_10_28_174748_create_profiles_table', 7),
(17, '2017_10_29_165238_create_blog_categories_table', 8),
(18, '2017_10_29_165438_create_blogs_table', 8),
(19, '2017_11_01_164225_create_popups_table', 9),
(20, '2017_11_06_000311_create_product_reviews_table', 10),
(21, '2017_11_07_002624_create_product_wishlists_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 means Pending, 1 means Active',
  `total_amount` double(8,2) NOT NULL,
  `shipping_charge` int(11) DEFAULT NULL,
  `notification` tinyint(11) NOT NULL DEFAULT '0',
  `vat` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `code`, `status`, `total_amount`, `shipping_charge`, `notification`, `vat`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'SO-1000000', 0, 625.00, 20, 1, 55, 1, '2017-10-27 12:59:27', '2017-11-04 09:27:42'),
(3, 'SO-1000002', 0, 930.00, 50, 0, 80, 1, '2017-10-27 13:24:16', '2017-10-27 13:24:16'),
(4, 'SO-1000003', 0, 930.00, 50, 0, 80, 1, '2017-10-27 13:31:43', '2017-10-27 13:31:43'),
(5, 'SO-1000004', 0, 985.00, 50, 0, 85, 1, '2017-10-27 13:32:35', '2017-10-27 13:32:35'),
(6, 'SO-1000005', 1, 985.00, 50, 1, 85, 1, '2017-10-27 13:33:07', '2017-11-04 09:14:58'),
(7, 'SO-1000006', 1, 1010.00, 20, 1, 90, 1, '2017-10-27 13:48:50', '2017-11-04 09:40:13'),
(8, 'SO-1000007', 0, 1040.00, 50, 1, 90, 1, '2017-11-04 08:33:14', '2017-11-04 09:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `quantity`, `amount`, `product_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 550.00, 5, 1, '2017-10-27 12:59:27', '2017-10-27 12:59:27'),
(3, 1, 250.00, 4, 3, '2017-10-27 13:24:16', '2017-10-27 13:24:16'),
(4, 1, 550.00, 5, 3, '2017-10-27 13:24:16', '2017-10-27 13:24:16'),
(5, 1, 250.00, 4, 4, '2017-10-27 13:31:43', '2017-10-27 13:31:43'),
(6, 1, 550.00, 5, 4, '2017-10-27 13:31:44', '2017-10-27 13:31:44'),
(7, 1, 250.00, 4, 5, '2017-10-27 13:32:35', '2017-10-27 13:32:35'),
(8, 1, 600.00, 6, 5, '2017-10-27 13:32:35', '2017-10-27 13:32:35'),
(9, 1, 250.00, 4, 6, '2017-10-27 13:33:07', '2017-10-27 13:33:07'),
(10, 1, 600.00, 6, 6, '2017-10-27 13:33:07', '2017-10-27 13:33:07'),
(11, 1, 900.00, 2, 7, '2017-10-27 13:48:50', '2017-10-27 13:48:50'),
(12, 2, 450.00, 1, 8, '2017-11-04 08:33:14', '2017-11-04 08:33:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `popups`
--

CREATE TABLE `popups` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `popups`
--

INSERT INTO `popups` (`id`, `image`, `title`, `summary`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'popup.jpg', '15% Discount', 'There are many purposes for discounting, including to increase short-term sales, to move out-of-date stock, to reward valuable customers', 1, 1, '2017-11-01 18:00:00', '2017-11-07 16:14:12');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `summary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specification` longtext COLLATE utf8mb4_unicode_ci,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_price` int(11) NOT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `new` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `tags` longtext COLLATE utf8mb4_unicode_ci,
  `availability` tinyint(1) NOT NULL COMMENT '0 means Stockout, 1 means Stock',
  `category_id` int(10) UNSIGNED NOT NULL,
  `subcategory_id` int(10) UNSIGNED NOT NULL,
  `color_id` int(10) UNSIGNED DEFAULT NULL,
  `size_id` int(10) UNSIGNED DEFAULT NULL,
  `discount_id` int(191) UNSIGNED DEFAULT NULL,
  `rating` int(11) NOT NULL DEFAULT '5',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `summary`, `specification`, `code`, `thumbnail`, `sales_price`, `featured`, `new`, `status`, `tags`, `availability`, `category_id`, `subcategory_id`, `color_id`, `size_id`, `discount_id`, `rating`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Shirt 1', 'All products are good', 'All products ae good', 'All products are good', 'p01', 'shirt1.jpg', 500, 1, 1, 1, 'All is good', 1, 1, 1, NULL, NULL, 1, 5, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(2, 'Shirt 2', 'All products are good', 'All products ae good', 'All products are good', 'p02', 'shirt2.jpg', 1000, 1, 1, 1, 'All is good', 1, 1, 1, NULL, NULL, 1, 3, 1, 1, '2017-10-20 18:00:00', '2017-11-05 19:21:04'),
(3, 'Shirt 3', 'All products are good', 'All products ae good', 'All products are good', 'p03', 'shirt3.jpg', 800, 1, 1, 1, 'All is good', 1, 1, 1, NULL, NULL, NULL, 5, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(4, 'Sunglass 1', 'All products are good', 'All products ae good', 'All products are good', 'p04', 'sun1.jpg', 250, 1, 1, 1, 'All is good', 1, 1, 3, NULL, NULL, NULL, 5, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(5, 'Sunglass 2', 'All products are good', 'All products ae good', 'All products are good', 'p05', 'sun2.jpg', 550, 1, 1, 1, 'All is good', 1, 1, 3, NULL, NULL, NULL, 5, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(6, 'Sunglass 3', 'All products are good', 'All products ae good', 'All products are good', 'p06', 'sun3.jpg', 600, 1, 1, 1, 'All is good', 1, 1, 3, NULL, NULL, NULL, 5, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `summary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `summary`, `review`, `rating`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Product rating is good..', 'Product rating is good..', 4, 2, 1, '2017-11-05 18:00:00', '2017-11-05 18:00:00'),
(2, 'Excelent', 'Excelent', 4, 5, 1, '2017-11-05 19:11:32', '2017-11-05 19:11:32'),
(3, 'Good', 'Good', 3, 2, 1, '2017-11-05 19:14:26', '2017-11-05 19:14:26'),
(4, 'Unique', 'Unique', 3, 2, 1, '2017-11-05 19:21:04', '2017-11-05 19:21:04');

-- --------------------------------------------------------

--
-- Table structure for table `product_wishlists`
--

CREATE TABLE `product_wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_wishlists`
--

INSERT INTO `product_wishlists` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-11-06 18:32:59', '2017-11-06 18:32:59');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` longtext COLLATE utf8mb4_unicode_ci,
  `working_hour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `display_name`, `company_name`, `address`, `website`, `number`, `email`, `facebook_link`, `twitter_link`, `google_link`, `about_us`, `working_hour`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Ours Dream', 'Our\'s Dream', '32 no. Dhanmondi, Dhaka 1210', 'ours-dream.com', '01677072949', 'ours.dreams@gmail.com', 'http://www.facebook.com/', 'http://www.twitter.com/', 'http://www.google.com/', 'This is our dream...', 'Mon - Sun / 9:00AM - 8:00PM', 'logo.png', '2017-10-27 18:00:00', '2017-11-05 17:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_charges`
--

CREATE TABLE `shipping_charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `in_dhaka` double(8,2) NOT NULL,
  `out_dhaka` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_charges`
--

INSERT INTO `shipping_charges` (`id`, `in_dhaka`, `out_dhaka`, `created_at`, `updated_at`) VALUES
(1, 25.00, 50.00, '2017-10-26 18:00:00', '2017-11-07 14:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `en_title`, `en_description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Small', 'Small', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00'),
(2, 'Medium', 'Medium', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00'),
(3, 'Large', 'Large', 1, 1, 1, '2017-11-02 18:00:00', '2017-11-02 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `title`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'slider1.jpg', '70%', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(2, 'slider2.jpg', '70%', 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `en_title`, `en_description`, `image`, `status`, `category_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Man Shirt', 'All dress are good.', 'shirt.jpg', 1, 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(2, 'Man Pent', 'All dress are good.', 'pent.jpg', 1, 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00'),
(3, 'Man Sunglass', 'All dress are good.', 'sunglass.jpg', 1, 1, 1, 1, '2017-10-20 18:00:00', '2017-10-20 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `upazilas`
--

CREATE TABLE `upazilas` (
  `id` int(2) UNSIGNED NOT NULL,
  `district_id` int(2) UNSIGNED NOT NULL,
  `dmp` tinyint(4) NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upazilas`
--

INSERT INTO `upazilas` (`id`, `district_id`, `dmp`, `name`, `bn_name`) VALUES
(1, 34, 0, 'Amtali Upazila', 'আমতলী'),
(2, 34, 0, 'Bamna Upazila', 'বামনা'),
(3, 34, 0, 'Barguna Sadar Upazila', 'বরগুনা সদর'),
(4, 34, 0, 'Betagi Upazila', 'বেতাগি'),
(5, 34, 0, 'Patharghata Upazila', 'পাথরঘাটা'),
(6, 34, 0, 'Taltali Upazila', 'তালতলী'),
(7, 35, 0, 'Muladi Upazila', 'মুলাদি'),
(8, 35, 0, 'Babuganj Upazila', 'বাবুগঞ্জ'),
(9, 35, 0, 'Agailjhara Upazila', 'আগাইলঝরা'),
(10, 35, 0, 'Barisal Sadar Upazila', 'বরিশাল সদর'),
(11, 35, 0, 'Bakerganj Upazila', 'বাকেরগঞ্জ'),
(12, 35, 0, 'Banaripara Upazila', 'বানাড়িপারা'),
(13, 35, 0, 'Gaurnadi Upazila', 'গৌরনদী'),
(14, 35, 0, 'Hizla Upazila', 'হিজলা'),
(15, 35, 0, 'Mehendiganj Upazila', 'মেহেদিগঞ্জ '),
(16, 35, 0, 'Wazirpur Upazila', 'ওয়াজিরপুর'),
(17, 36, 0, 'Bhola Sadar Upazila', 'ভোলা সদর'),
(18, 36, 0, 'Burhanuddin Upazila', 'বুরহানউদ্দিন'),
(19, 36, 0, 'Char Fasson Upazila', 'চর ফ্যাশন'),
(20, 36, 0, 'Daulatkhan Upazila', 'দৌলতখান'),
(21, 36, 0, 'Lalmohan Upazila', 'লালমোহন'),
(22, 36, 0, 'Manpura Upazila', 'মনপুরা'),
(23, 36, 0, 'Tazumuddin Upazila', 'তাজুমুদ্দিন'),
(24, 37, 0, 'Jhalokati Sadar Upazila', 'ঝালকাঠি সদর'),
(25, 37, 0, 'Kathalia Upazila', 'কাঁঠালিয়া'),
(26, 37, 0, 'Nalchity Upazila', 'নালচিতি'),
(27, 37, 0, 'Rajapur Upazila', 'রাজাপুর'),
(28, 38, 0, 'Bauphal Upazila', 'বাউফল'),
(29, 38, 0, 'Dashmina Upazila', 'দশমিনা'),
(30, 38, 0, 'Galachipa Upazila', 'গলাচিপা'),
(31, 38, 0, 'Kalapara Upazila', 'কালাপারা'),
(32, 38, 0, 'Mirzaganj Upazila', 'মির্জাগঞ্জ '),
(33, 38, 0, 'Patuakhali Sadar Upazila', 'পটুয়াখালী সদর'),
(34, 38, 0, 'Dumki Upazila', 'ডুমকি'),
(35, 38, 0, 'Rangabali Upazila', 'রাঙ্গাবালি'),
(36, 39, 0, 'Bhandaria', 'ভ্যান্ডারিয়া'),
(37, 39, 0, 'Kaukhali', 'কাউখালি'),
(38, 39, 0, 'Mathbaria', 'মাঠবাড়িয়া'),
(39, 39, 0, 'Nazirpur', 'নাজিরপুর'),
(40, 39, 0, 'Nesarabad', 'নেসারাবাদ'),
(41, 39, 0, 'Pirojpur Sadar', 'পিরোজপুর সদর'),
(42, 39, 0, 'Zianagar', 'জিয়ানগর'),
(43, 40, 0, 'Bandarban Sadar', 'বান্দরবন সদর'),
(44, 40, 0, 'Thanchi', 'থানচি'),
(45, 40, 0, 'Lama', 'লামা'),
(46, 40, 0, 'Naikhongchhari', 'নাইখংছড়ি '),
(47, 40, 0, 'Ali kadam', 'আলী কদম'),
(48, 40, 0, 'Rowangchhari', 'রউয়াংছড়ি '),
(49, 40, 0, 'Ruma', 'রুমা'),
(50, 41, 0, 'Brahmanbaria Sadar Upazila', 'ব্রাহ্মণবাড়িয়া সদর'),
(51, 41, 0, 'Ashuganj Upazila', 'আশুগঞ্জ'),
(52, 41, 0, 'Nasirnagar Upazila', 'নাসির নগর'),
(53, 41, 0, 'Nabinagar Upazila', 'নবীনগর'),
(54, 41, 0, 'Sarail Upazila', 'সরাইল'),
(55, 41, 0, 'Shahbazpur Town', 'শাহবাজপুর টাউন'),
(56, 41, 0, 'Kasba Upazila', 'কসবা'),
(57, 41, 0, 'Akhaura Upazila', 'আখাউরা'),
(58, 41, 0, 'Bancharampur Upazila', 'বাঞ্ছারামপুর'),
(59, 41, 0, 'Bijoynagar Upazila', 'বিজয় নগর'),
(60, 42, 0, 'Chandpur Sadar', 'চাঁদপুর সদর'),
(61, 42, 0, 'Faridganj', 'ফরিদগঞ্জ'),
(62, 42, 0, 'Haimchar', 'হাইমচর'),
(63, 42, 0, 'Haziganj', 'হাজীগঞ্জ'),
(64, 42, 0, 'Kachua', 'কচুয়া'),
(65, 42, 0, 'Matlab Uttar', 'মতলব উত্তর'),
(66, 42, 0, 'Matlab Dakkhin', 'মতলব দক্ষিণ'),
(67, 42, 0, 'Shahrasti', 'শাহরাস্তি'),
(68, 43, 0, 'Anwara Upazila', 'আনোয়ারা'),
(69, 43, 0, 'Banshkhali Upazila', 'বাশখালি'),
(70, 43, 0, 'Boalkhali Upazila', 'বোয়ালখালি'),
(71, 43, 0, 'Chandanaish Upazila', 'চন্দনাইশ'),
(72, 43, 0, 'Fatikchhari Upazila', 'ফটিকছড়ি'),
(73, 43, 0, 'Hathazari Upazila', 'হাঠহাজারী'),
(74, 43, 0, 'Lohagara Upazila', 'লোহাগারা'),
(75, 43, 0, 'Mirsharai Upazila', 'মিরসরাই'),
(76, 43, 0, 'Patiya Upazila', 'পটিয়া'),
(77, 43, 0, 'Rangunia Upazila', 'রাঙ্গুনিয়া'),
(78, 43, 0, 'Raozan Upazila', 'রাউজান'),
(79, 43, 0, 'Sandwip Upazila', 'সন্দ্বীপ'),
(80, 43, 0, 'Satkania Upazila', 'সাতকানিয়া'),
(81, 43, 0, 'Sitakunda Upazila', 'সীতাকুণ্ড'),
(82, 44, 0, 'Barura Upazila', 'বড়ুরা'),
(83, 44, 0, 'Brahmanpara Upazila', 'ব্রাহ্মণপাড়া'),
(84, 44, 0, 'Burichong Upazila', 'বুড়িচং'),
(85, 44, 0, 'Chandina Upazila', 'চান্দিনা'),
(86, 44, 0, 'Chauddagram Upazila', 'চৌদ্দগ্রাম'),
(87, 44, 0, 'Daudkandi Upazila', 'দাউদকান্দি'),
(88, 44, 0, 'Debidwar Upazila', 'দেবীদ্বার'),
(89, 44, 0, 'Homna Upazila', 'হোমনা'),
(90, 44, 0, 'Comilla Sadar Upazila', 'কুমিল্লা সদর'),
(91, 44, 0, 'Laksam Upazila', 'লাকসাম'),
(92, 44, 0, 'Monohorgonj Upazila', 'মনোহরগঞ্জ'),
(93, 44, 0, 'Meghna Upazila', 'মেঘনা'),
(94, 44, 0, 'Muradnagar Upazila', 'মুরাদনগর'),
(95, 44, 0, 'Nangalkot Upazila', 'নাঙ্গালকোট'),
(96, 44, 0, 'Comilla Sadar South Upazila', 'কুমিল্লা সদর দক্ষিণ'),
(97, 44, 0, 'Titas Upazila', 'তিতাস'),
(98, 45, 0, 'Chakaria Upazila', 'চকরিয়া'),
(100, 45, 0, 'Cox\'s Bazar Sadar Upazila', 'কক্স বাজার সদর'),
(101, 45, 0, 'Kutubdia Upazila', 'কুতুবদিয়া'),
(102, 45, 0, 'Maheshkhali Upazila', 'মহেশখালী'),
(103, 45, 0, 'Ramu Upazila', 'রামু'),
(104, 45, 0, 'Teknaf Upazila', 'টেকনাফ'),
(105, 45, 0, 'Ukhia Upazila', 'উখিয়া'),
(106, 45, 0, 'Pekua Upazila', 'পেকুয়া'),
(107, 46, 0, 'Feni Sadar', 'ফেনী সদর'),
(108, 46, 0, 'Chagalnaiya', 'ছাগল নাইয়া'),
(109, 46, 0, 'Daganbhyan', 'দাগানভিয়া'),
(110, 46, 0, 'Parshuram', 'পরশুরাম'),
(111, 46, 0, 'Fhulgazi', 'ফুলগাজি'),
(112, 46, 0, 'Sonagazi', 'সোনাগাজি'),
(113, 47, 0, 'Dighinala Upazila', 'দিঘিনালা '),
(114, 47, 0, 'Khagrachhari Upazila', 'খাগড়াছড়ি'),
(115, 47, 0, 'Lakshmichhari Upazila', 'লক্ষ্মীছড়ি'),
(116, 47, 0, 'Mahalchhari Upazila', 'মহলছড়ি'),
(117, 47, 0, 'Manikchhari Upazila', 'মানিকছড়ি'),
(118, 47, 0, 'Matiranga Upazila', 'মাটিরাঙ্গা'),
(119, 47, 0, 'Panchhari Upazila', 'পানছড়ি'),
(120, 47, 0, 'Ramgarh Upazila', 'রামগড়'),
(121, 48, 0, 'Lakshmipur Sadar Upazila', 'লক্ষ্মীপুর সদর'),
(122, 48, 0, 'Raipur Upazila', 'রায়পুর'),
(123, 48, 0, 'Ramganj Upazila', 'রামগঞ্জ'),
(124, 48, 0, 'Ramgati Upazila', 'রামগতি'),
(125, 48, 0, 'Komol Nagar Upazila', 'কমল নগর'),
(126, 49, 0, 'Noakhali Sadar Upazila', 'নোয়াখালী সদর'),
(127, 49, 0, 'Begumganj Upazila', 'বেগমগঞ্জ'),
(128, 49, 0, 'Chatkhil Upazila', 'চাটখিল'),
(129, 49, 0, 'Companyganj Upazila', 'কোম্পানীগঞ্জ'),
(130, 49, 0, 'Shenbag Upazila', 'শেনবাগ'),
(131, 49, 0, 'Hatia Upazila', 'হাতিয়া'),
(132, 49, 0, 'Kobirhat Upazila', 'কবিরহাট '),
(133, 49, 0, 'Sonaimuri Upazila', 'সোনাইমুরি'),
(134, 49, 0, 'Suborno Char Upazila', 'সুবর্ণ চর '),
(135, 50, 0, 'Rangamati Sadar Upazila', 'রাঙ্গামাটি সদর'),
(136, 50, 0, 'Belaichhari Upazila', 'বেলাইছড়ি'),
(137, 50, 0, 'Bagaichhari Upazila', 'বাঘাইছড়ি'),
(138, 50, 0, 'Barkal Upazila', 'বরকল'),
(139, 50, 0, 'Juraichhari Upazila', 'জুরাইছড়ি'),
(140, 50, 0, 'Rajasthali Upazila', 'রাজাস্থলি'),
(141, 50, 0, 'Kaptai Upazila', 'কাপ্তাই'),
(142, 50, 0, 'Langadu Upazila', 'লাঙ্গাডু'),
(143, 50, 0, 'Nannerchar Upazila', 'নান্নেরচর '),
(144, 50, 0, 'Kaukhali Upazila', 'কাউখালি'),
(145, 1, 0, 'Dhamrai Upazila', 'ধামরাই'),
(146, 1, 0, 'Dohar Upazila', 'দোহার'),
(147, 1, 0, 'Keraniganj Upazila', 'কেরানীগঞ্জ'),
(148, 1, 0, 'Nawabganj Upazila', 'নবাবগঞ্জ'),
(149, 1, 0, 'Savar Upazila', 'সাভার'),
(150, 2, 0, 'Faridpur Sadar Upazila', 'ফরিদপুর সদর'),
(151, 2, 0, 'Boalmari Upazila', 'বোয়ালমারী'),
(152, 2, 0, 'Alfadanga Upazila', 'আলফাডাঙ্গা'),
(153, 2, 0, 'Madhukhali Upazila', 'মধুখালি'),
(154, 2, 0, 'Bhanga Upazila', 'ভাঙ্গা'),
(155, 2, 0, 'Nagarkanda Upazila', 'নগরকান্ড'),
(156, 2, 0, 'Charbhadrasan Upazila', 'চরভদ্রাসন '),
(157, 2, 0, 'Sadarpur Upazila', 'সদরপুর'),
(158, 2, 0, 'Shaltha Upazila', 'শালথা'),
(159, 3, 0, 'Gazipur Sadar-Joydebpur', 'গাজীপুর সদর'),
(160, 3, 0, 'Kaliakior', 'কালিয়াকৈর'),
(161, 3, 0, 'Kapasia', 'কাপাসিয়া'),
(162, 3, 0, 'Sripur', 'শ্রীপুর'),
(163, 3, 0, 'Kaliganj', 'কালীগঞ্জ'),
(164, 3, 0, 'Tongi', 'টঙ্গি'),
(165, 4, 0, 'Gopalganj Sadar Upazila', 'গোপালগঞ্জ সদর'),
(166, 4, 0, 'Kashiani Upazila', 'কাশিয়ানি'),
(167, 4, 0, 'Kotalipara Upazila', 'কোটালিপাড়া'),
(168, 4, 0, 'Muksudpur Upazila', 'মুকসুদপুর'),
(169, 4, 0, 'Tungipara Upazila', 'টুঙ্গিপাড়া'),
(170, 5, 0, 'Dewanganj Upazila', 'দেওয়ানগঞ্জ'),
(171, 5, 0, 'Baksiganj Upazila', 'বকসিগঞ্জ'),
(172, 5, 0, 'Islampur Upazila', 'ইসলামপুর'),
(173, 5, 0, 'Jamalpur Sadar Upazila', 'জামালপুর সদর'),
(174, 5, 0, 'Madarganj Upazila', 'মাদারগঞ্জ'),
(175, 5, 0, 'Melandaha Upazila', 'মেলানদাহা'),
(176, 5, 0, 'Sarishabari Upazila', 'সরিষাবাড়ি '),
(177, 5, 0, 'Narundi Police I.C', 'নারুন্দি'),
(178, 6, 0, 'Astagram Upazila', 'অষ্টগ্রাম'),
(179, 6, 0, 'Bajitpur Upazila', 'বাজিতপুর'),
(180, 6, 0, 'Bhairab Upazila', 'ভৈরব'),
(181, 6, 0, 'Hossainpur Upazila', 'হোসেনপুর '),
(182, 6, 0, 'Itna Upazila', 'ইটনা'),
(183, 6, 0, 'Karimganj Upazila', 'করিমগঞ্জ'),
(184, 6, 0, 'Katiadi Upazila', 'কতিয়াদি'),
(185, 6, 0, 'Kishoreganj Sadar Upazila', 'কিশোরগঞ্জ সদর'),
(186, 6, 0, 'Kuliarchar Upazila', 'কুলিয়ারচর'),
(187, 6, 0, 'Mithamain Upazila', 'মিঠামাইন'),
(188, 6, 0, 'Nikli Upazila', 'নিকলি'),
(189, 6, 0, 'Pakundia Upazila', 'পাকুন্ডা'),
(190, 6, 0, 'Tarail Upazila', 'তাড়াইল'),
(191, 7, 0, 'Madaripur Sadar', 'মাদারীপুর সদর'),
(192, 7, 0, 'Kalkini', 'কালকিনি'),
(193, 7, 0, 'Rajoir', 'রাজইর'),
(194, 7, 0, 'Shibchar', 'শিবচর'),
(195, 8, 0, 'Manikganj Sadar Upazila', 'মানিকগঞ্জ সদর'),
(196, 8, 0, 'Singair Upazila', 'সিঙ্গাইর'),
(197, 8, 0, 'Shibalaya Upazila', 'শিবালয়'),
(198, 8, 0, 'Saturia Upazila', 'সাঠুরিয়া'),
(199, 8, 0, 'Harirampur Upazila', 'হরিরামপুর'),
(200, 8, 0, 'Ghior Upazila', 'ঘিওর'),
(201, 8, 0, 'Daulatpur Upazila', 'দৌলতপুর'),
(202, 9, 0, 'Lohajang Upazila', 'লোহাজং'),
(203, 9, 0, 'Sreenagar Upazila', 'শ্রীনগর'),
(204, 9, 0, 'Munshiganj Sadar Upazila', 'মুন্সিগঞ্জ সদর'),
(205, 9, 0, 'Sirajdikhan Upazila', 'সিরাজদিখান'),
(206, 9, 0, 'Tongibari Upazila', 'টঙ্গিবাড়ি'),
(207, 9, 0, 'Gazaria Upazila', 'গজারিয়া'),
(208, 10, 0, 'Bhaluka Upazila', 'ভালুকা'),
(209, 10, 0, 'Trishal Upazila', 'ত্রিশাল'),
(210, 10, 0, 'Haluaghat Upazila', 'হালুয়াঘাট'),
(211, 10, 0, 'Muktagachha Upazila', 'মুক্তাগাছা'),
(212, 10, 0, 'Dhobaura Upazila', 'ধবারুয়া'),
(213, 10, 0, 'Fulbaria Upazila', 'ফুলবাড়িয়া'),
(214, 10, 0, 'Gaffargaon Upazila', 'গফরগাঁও'),
(215, 10, 0, 'Gauripur Upazila', 'গৌরিপুর'),
(216, 10, 0, 'Ishwarganj Upazila', 'ঈশ্বরগঞ্জ'),
(217, 10, 0, 'Mymensingh Sadar Upazila', 'ময়মনসিং সদর'),
(218, 10, 0, 'Nandail Upazila', 'নন্দাইল'),
(219, 10, 0, 'Phulpur Upazila', 'ফুলপুর'),
(220, 11, 0, 'Araihazar Upazila', 'আড়াইহাজার'),
(221, 11, 0, 'Sonargaon Upazila', 'সোনারগাঁও'),
(222, 11, 0, 'Bandar', 'বান্দার'),
(223, 11, 0, 'Naryanganj Sadar Upazila', 'নারায়ানগঞ্জ সদর'),
(224, 11, 0, 'Rupganj Upazila', 'রূপগঞ্জ'),
(225, 11, 0, 'Siddirgonj Upazila', 'সিদ্ধিরগঞ্জ'),
(226, 12, 0, 'Belabo Upazila', 'বেলাবো'),
(227, 12, 0, 'Monohardi Upazila', 'মনোহরদি'),
(228, 12, 0, 'Narsingdi Sadar Upazila', 'নরসিংদী সদর'),
(229, 12, 0, 'Palash Upazila', 'পলাশ'),
(230, 12, 0, 'Raipura Upazila, Narsingdi', 'রায়পুর'),
(231, 12, 0, 'Shibpur Upazila', 'শিবপুর'),
(232, 13, 0, 'Kendua Upazilla', 'কেন্দুয়া'),
(233, 13, 0, 'Atpara Upazilla', 'আটপাড়া'),
(234, 13, 0, 'Barhatta Upazilla', 'বরহাট্টা'),
(235, 13, 0, 'Durgapur Upazilla', 'দুর্গাপুর'),
(236, 13, 0, 'Kalmakanda Upazilla', 'কলমাকান্দা'),
(237, 13, 0, 'Madan Upazilla', 'মদন'),
(238, 13, 0, 'Mohanganj Upazilla', 'মোহনগঞ্জ'),
(239, 13, 0, 'Netrakona-S Upazilla', 'নেত্রকোনা সদর'),
(240, 13, 0, 'Purbadhala Upazilla', 'পূর্বধলা'),
(241, 13, 0, 'Khaliajuri Upazilla', 'খালিয়াজুরি'),
(242, 14, 0, 'Baliakandi Upazila', 'বালিয়াকান্দি'),
(243, 14, 0, 'Goalandaghat Upazila', 'গোয়ালন্দ ঘাট'),
(244, 14, 0, 'Pangsha Upazila', 'পাংশা'),
(245, 14, 0, 'Kalukhali Upazila', 'কালুখালি'),
(246, 14, 0, 'Rajbari Sadar Upazila', 'রাজবাড়ি সদর'),
(247, 15, 0, 'Shariatpur Sadar -Palong', 'শরীয়তপুর সদর '),
(248, 15, 0, 'Damudya Upazila', 'দামুদিয়া'),
(249, 15, 0, 'Naria Upazila', 'নড়িয়া'),
(250, 15, 0, 'Jajira Upazila', 'জাজিরা'),
(251, 15, 0, 'Bhedarganj Upazila', 'ভেদারগঞ্জ'),
(252, 15, 0, 'Gosairhat Upazila', 'গোসাইর হাট '),
(253, 16, 0, 'Jhenaigati Upazila', 'ঝিনাইগাতি'),
(254, 16, 0, 'Nakla Upazila', 'নাকলা'),
(255, 16, 0, 'Nalitabari Upazila', 'নালিতাবাড়ি'),
(256, 16, 0, 'Sherpur Sadar Upazila', 'শেরপুর সদর'),
(257, 16, 0, 'Sreebardi Upazila', 'শ্রীবরদি'),
(258, 17, 0, 'Tangail Sadar Upazila', 'টাঙ্গাইল সদর'),
(259, 17, 0, 'Sakhipur Upazila', 'সখিপুর'),
(260, 17, 0, 'Basail Upazila', 'বসাইল'),
(261, 17, 0, 'Madhupur Upazila', 'মধুপুর'),
(262, 17, 0, 'Ghatail Upazila', 'ঘাটাইল'),
(263, 17, 0, 'Kalihati Upazila', 'কালিহাতি'),
(264, 17, 0, 'Nagarpur Upazila', 'নগরপুর'),
(265, 17, 0, 'Mirzapur Upazila', 'মির্জাপুর'),
(266, 17, 0, 'Gopalpur Upazila', 'গোপালপুর'),
(267, 17, 0, 'Delduar Upazila', 'দেলদুয়ার'),
(268, 17, 0, 'Bhuapur Upazila', 'ভুয়াপুর'),
(269, 17, 0, 'Dhanbari Upazila', 'ধানবাড়ি'),
(270, 55, 0, 'Bagerhat Sadar Upazila', 'বাগেরহাট সদর'),
(271, 55, 0, 'Chitalmari Upazila', 'চিতলমাড়ি'),
(272, 55, 0, 'Fakirhat Upazila', 'ফকিরহাট'),
(273, 55, 0, 'Kachua Upazila', 'কচুয়া'),
(274, 55, 0, 'Mollahat Upazila', 'মোল্লাহাট '),
(275, 55, 0, 'Mongla Upazila', 'মংলা'),
(276, 55, 0, 'Morrelganj Upazila', 'মরেলগঞ্জ'),
(277, 55, 0, 'Rampal Upazila', 'রামপাল'),
(278, 55, 0, 'Sarankhola Upazila', 'স্মরণখোলা'),
(279, 56, 0, 'Damurhuda Upazila', 'দামুরহুদা'),
(280, 56, 0, 'Chuadanga-S Upazila', 'চুয়াডাঙ্গা সদর'),
(281, 56, 0, 'Jibannagar Upazila', 'জীবন নগর '),
(282, 56, 0, 'Alamdanga Upazila', 'আলমডাঙ্গা'),
(283, 57, 0, 'Abhaynagar Upazila', 'অভয়নগর'),
(284, 57, 0, 'Keshabpur Upazila', 'কেশবপুর'),
(285, 57, 0, 'Bagherpara Upazila', 'বাঘের পাড়া '),
(286, 57, 0, 'Jessore Sadar Upazila', 'যশোর সদর'),
(287, 57, 0, 'Chaugachha Upazila', 'চৌগাছা'),
(288, 57, 0, 'Manirampur Upazila', 'মনিরামপুর '),
(289, 57, 0, 'Jhikargachha Upazila', 'ঝিকরগাছা'),
(290, 57, 0, 'Sharsha Upazila', 'সারশা'),
(291, 58, 0, 'Jhenaidah Sadar Upazila', 'ঝিনাইদহ সদর'),
(292, 58, 0, 'Maheshpur Upazila', 'মহেশপুর'),
(293, 58, 0, 'Kaliganj Upazila', 'কালীগঞ্জ'),
(294, 58, 0, 'Kotchandpur Upazila', 'কোট চাঁদপুর '),
(295, 58, 0, 'Shailkupa Upazila', 'শৈলকুপা'),
(296, 58, 0, 'Harinakunda Upazila', 'হাড়িনাকুন্দা'),
(297, 59, 0, 'Terokhada Upazila', 'তেরোখাদা'),
(298, 59, 0, 'Batiaghata Upazila', 'বাটিয়াঘাটা '),
(299, 59, 0, 'Dacope Upazila', 'ডাকপে'),
(300, 59, 0, 'Dumuria Upazila', 'ডুমুরিয়া'),
(301, 59, 0, 'Dighalia Upazila', 'দিঘলিয়া'),
(302, 59, 0, 'Koyra Upazila', 'কয়ড়া'),
(303, 59, 0, 'Paikgachha Upazila', 'পাইকগাছা'),
(304, 59, 0, 'Phultala Upazila', 'ফুলতলা'),
(305, 59, 0, 'Rupsa Upazila', 'রূপসা'),
(306, 60, 0, 'Kushtia Sadar', 'কুষ্টিয়া সদর'),
(307, 60, 0, 'Kumarkhali', 'কুমারখালি'),
(308, 60, 0, 'Daulatpur', 'দৌলতপুর'),
(309, 60, 0, 'Mirpur', 'মিরপুর'),
(310, 60, 0, 'Bheramara', 'ভেরামারা'),
(311, 60, 0, 'Khoksa', 'খোকসা'),
(312, 61, 0, 'Magura Sadar Upazila', 'মাগুরা সদর'),
(313, 61, 0, 'Mohammadpur Upazila', 'মোহাম্মাদপুর'),
(314, 61, 0, 'Shalikha Upazila', 'শালিখা'),
(315, 61, 0, 'Sreepur Upazila', 'শ্রীপুর'),
(316, 62, 0, 'angni Upazila', 'আংনি'),
(317, 62, 0, 'Mujib Nagar Upazila', 'মুজিব নগর'),
(318, 62, 0, 'Meherpur-S Upazila', 'মেহেরপুর সদর'),
(319, 63, 0, 'Narail-S Upazilla', 'নড়াইল সদর'),
(320, 63, 0, 'Lohagara Upazilla', 'লোহাগাড়া'),
(321, 63, 0, 'Kalia Upazilla', 'কালিয়া'),
(322, 64, 0, 'Satkhira Sadar Upazila', 'সাতক্ষীরা সদর'),
(323, 64, 0, 'Assasuni Upazila', 'আসসাশুনি '),
(324, 64, 0, 'Debhata Upazila', 'দেভাটা'),
(325, 64, 0, 'Tala Upazila', 'তালা'),
(326, 64, 0, 'Kalaroa Upazila', 'কলরোয়া'),
(327, 64, 0, 'Kaliganj Upazila', 'কালীগঞ্জ'),
(328, 64, 0, 'Shyamnagar Upazila', 'শ্যামনগর'),
(329, 18, 0, 'Adamdighi', 'আদমদিঘী'),
(330, 18, 0, 'Bogra Sadar', 'বগুড়া সদর'),
(331, 18, 0, 'Sherpur', 'শেরপুর'),
(332, 18, 0, 'Dhunat', 'ধুনট'),
(333, 18, 0, 'Dhupchanchia', 'দুপচাচিয়া'),
(334, 18, 0, 'Gabtali', 'গাবতলি'),
(335, 18, 0, 'Kahaloo', 'কাহালু'),
(336, 18, 0, 'Nandigram', 'নন্দিগ্রাম'),
(337, 18, 0, 'Sahajanpur', 'শাহজাহানপুর'),
(338, 18, 0, 'Sariakandi', 'সারিয়াকান্দি'),
(339, 18, 0, 'Shibganj', 'শিবগঞ্জ'),
(340, 18, 0, 'Sonatala', 'সোনাতলা'),
(341, 19, 0, 'Joypurhat S', 'জয়পুরহাট সদর'),
(342, 19, 0, 'Akkelpur', 'আক্কেলপুর'),
(343, 19, 0, 'Kalai', 'কালাই'),
(344, 19, 0, 'Khetlal', 'খেতলাল'),
(345, 19, 0, 'Panchbibi', 'পাঁচবিবি'),
(346, 20, 0, 'Naogaon Sadar Upazila', 'নওগাঁ সদর'),
(347, 20, 0, 'Mohadevpur Upazila', 'মহাদেবপুর'),
(348, 20, 0, 'Manda Upazila', 'মান্দা'),
(349, 20, 0, 'Niamatpur Upazila', 'নিয়ামতপুর'),
(350, 20, 0, 'Atrai Upazila', 'আত্রাই'),
(351, 20, 0, 'Raninagar Upazila', 'রাণীনগর'),
(352, 20, 0, 'Patnitala Upazila', 'পত্নীতলা'),
(353, 20, 0, 'Dhamoirhat Upazila', 'ধামইরহাট '),
(354, 20, 0, 'Sapahar Upazila', 'সাপাহার'),
(355, 20, 0, 'Porsha Upazila', 'পোরশা'),
(356, 20, 0, 'Badalgachhi Upazila', 'বদলগাছি'),
(357, 21, 0, 'Natore Sadar Upazila', 'নাটোর সদর'),
(358, 21, 0, 'Baraigram Upazila', 'বড়াইগ্রাম'),
(359, 21, 0, 'Bagatipara Upazila', 'বাগাতিপাড়া'),
(360, 21, 0, 'Lalpur Upazila', 'লালপুর'),
(361, 21, 0, 'Natore Sadar Upazila', 'নাটোর সদর'),
(362, 21, 0, 'Baraigram Upazila', 'বড়াই গ্রাম'),
(363, 22, 0, 'Bholahat Upazila', 'ভোলাহাট'),
(364, 22, 0, 'Gomastapur Upazila', 'গোমস্তাপুর'),
(365, 22, 0, 'Nachole Upazila', 'নাচোল'),
(366, 22, 0, 'Nawabganj Sadar Upazila', 'নবাবগঞ্জ সদর'),
(367, 22, 0, 'Shibganj Upazila', 'শিবগঞ্জ'),
(368, 23, 0, 'Atgharia Upazila', 'আটঘরিয়া'),
(369, 23, 0, 'Bera Upazila', 'বেড়া'),
(370, 23, 0, 'Bhangura Upazila', 'ভাঙ্গুরা'),
(371, 23, 0, 'Chatmohar Upazila', 'চাটমোহর'),
(372, 23, 0, 'Faridpur Upazila', 'ফরিদপুর'),
(373, 23, 0, 'Ishwardi Upazila', 'ঈশ্বরদী'),
(374, 23, 0, 'Pabna Sadar Upazila', 'পাবনা সদর'),
(375, 23, 0, 'Santhia Upazila', 'সাথিয়া'),
(376, 23, 0, 'Sujanagar Upazila', 'সুজানগর'),
(377, 24, 0, 'Bagha Upazila', 'বাঘা'),
(378, 24, 0, 'Bagmara Upazila', 'বাগমারা'),
(379, 24, 0, 'Charghat Upazila', 'চারঘাট'),
(380, 24, 0, 'Durgapur Upazila', 'দুর্গাপুর'),
(381, 24, 0, 'Godagari Upazila', 'গোদাগারি'),
(382, 24, 0, 'Mohanpur Upazila', 'মোহনপুর'),
(383, 24, 0, 'Paba Upazila', 'পবা'),
(384, 24, 0, 'Puthia Upazila', 'পুঠিয়া'),
(385, 24, 0, 'Tanore Upazila', 'তানোর'),
(386, 25, 0, 'Sirajganj Sadar Upazila', 'সিরাজগঞ্জ সদর'),
(387, 25, 0, 'Belkuchi Upazila', 'বেলকুচি'),
(388, 25, 0, 'Chauhali Upazila', 'চৌহালি'),
(389, 25, 0, 'Kamarkhanda Upazila', 'কামারখান্দা'),
(390, 25, 0, 'Kazipur Upazila', 'কাজীপুর'),
(391, 25, 0, 'Raiganj Upazila', 'রায়গঞ্জ'),
(392, 25, 0, 'Shahjadpur Upazila', 'শাহজাদপুর'),
(393, 25, 0, 'Tarash Upazila', 'তারাশ'),
(394, 25, 0, 'Ullahpara Upazila', 'উল্লাপাড়া'),
(395, 26, 0, 'Birampur Upazila', 'বিরামপুর'),
(396, 26, 0, 'Birganj', 'বীরগঞ্জ'),
(397, 26, 0, 'Biral Upazila', 'বিড়াল'),
(398, 26, 0, 'Bochaganj Upazila', 'বোচাগঞ্জ'),
(399, 26, 0, 'Chirirbandar Upazila', 'চিরিরবন্দর'),
(400, 26, 0, 'Phulbari Upazila', 'ফুলবাড়ি'),
(401, 26, 0, 'Ghoraghat Upazila', 'ঘোড়াঘাট'),
(402, 26, 0, 'Hakimpur Upazila', 'হাকিমপুর'),
(403, 26, 0, 'Kaharole Upazila', 'কাহারোল'),
(404, 26, 0, 'Khansama Upazila', 'খানসামা'),
(405, 26, 0, 'Dinajpur Sadar Upazila', 'দিনাজপুর সদর'),
(406, 26, 0, 'Nawabganj', 'নবাবগঞ্জ'),
(407, 26, 0, 'Parbatipur Upazila', 'পার্বতীপুর'),
(408, 27, 0, 'Fulchhari', 'ফুলছড়ি'),
(409, 27, 0, 'Gaibandha sadar', 'গাইবান্ধা সদর'),
(410, 27, 0, 'Gobindaganj', 'গোবিন্দগঞ্জ'),
(411, 27, 0, 'Palashbari', 'পলাশবাড়ী'),
(412, 27, 0, 'Sadullapur', 'সাদুল্যাপুর'),
(413, 27, 0, 'Saghata', 'সাঘাটা'),
(414, 27, 0, 'Sundarganj', 'সুন্দরগঞ্জ'),
(415, 28, 0, 'Kurigram Sadar', 'কুড়িগ্রাম সদর'),
(416, 28, 0, 'Nageshwari', 'নাগেশ্বরী'),
(417, 28, 0, 'Bhurungamari', 'ভুরুঙ্গামারি'),
(418, 28, 0, 'Phulbari', 'ফুলবাড়ি'),
(419, 28, 0, 'Rajarhat', 'রাজারহাট'),
(420, 28, 0, 'Ulipur', 'উলিপুর'),
(421, 28, 0, 'Chilmari', 'চিলমারি'),
(422, 28, 0, 'Rowmari', 'রউমারি'),
(423, 28, 0, 'Char Rajibpur', 'চর রাজিবপুর'),
(424, 29, 0, 'Lalmanirhat Sadar', 'লালমনিরহাট সদর'),
(425, 29, 0, 'Aditmari', 'আদিতমারি'),
(426, 29, 0, 'Kaliganj', 'কালীগঞ্জ'),
(427, 29, 0, 'Hatibandha', 'হাতিবান্ধা'),
(428, 29, 0, 'Patgram', 'পাটগ্রাম'),
(429, 30, 0, 'Nilphamari Sadar', 'নীলফামারী সদর'),
(430, 30, 0, 'Saidpur', 'সৈয়দপুর'),
(431, 30, 0, 'Jaldhaka', 'জলঢাকা'),
(432, 30, 0, 'Kishoreganj', 'কিশোরগঞ্জ'),
(433, 30, 0, 'Domar', 'ডোমার'),
(434, 30, 0, 'Dimla', 'ডিমলা'),
(435, 31, 0, 'Panchagarh Sadar', 'পঞ্চগড় সদর'),
(436, 31, 0, 'Debiganj', 'দেবীগঞ্জ'),
(437, 31, 0, 'Boda', 'বোদা'),
(438, 31, 0, 'Atwari', 'আটোয়ারি'),
(439, 31, 0, 'Tetulia', 'তেতুলিয়া'),
(440, 32, 0, 'Badarganj Upazila', 'বদরগঞ্জ'),
(441, 32, 0, 'Mithapukur Upazila', 'মিঠাপুকুর'),
(442, 32, 0, 'Gangachara Upazila', 'গঙ্গাচরা'),
(443, 32, 0, 'Kaunia Upazila', 'কাউনিয়া'),
(444, 32, 0, 'Rangpur Sadar Upazila', 'রংপুর সদর'),
(445, 32, 0, 'Pirgachha Upazila', 'পীরগাছা'),
(446, 32, 0, 'Pirganj Upazila', 'পীরগঞ্জ'),
(447, 32, 0, 'Taraganj Upazila', 'তারাগঞ্জ'),
(448, 33, 0, 'Thakurgaon Sadar Upazila', 'ঠাকুরগাঁও সদর'),
(449, 33, 0, 'Pirganj Upazila', 'পীরগঞ্জ'),
(450, 33, 0, 'Baliadangi Upazila', 'বালিয়াডাঙ্গি'),
(451, 33, 0, 'Haripur Upazila', 'হরিপুর'),
(452, 33, 0, 'Ranisankail Upazila', 'রাণীসংকইল'),
(453, 51, 0, 'Ajmiriganj', 'আজমিরিগঞ্জ'),
(454, 51, 0, 'Baniachang', 'বানিয়াচং'),
(455, 51, 0, 'Bahubal', 'বাহুবল'),
(456, 51, 0, 'Chunarughat', 'চুনারুঘাট'),
(457, 51, 0, 'Habiganj Sadar', 'হবিগঞ্জ সদর'),
(458, 51, 0, 'Lakhai', 'লাক্ষাই'),
(459, 51, 0, 'Madhabpur', 'মাধবপুর'),
(460, 51, 0, 'Nabiganj', 'নবীগঞ্জ'),
(461, 51, 0, 'Shaistagonj Upazila', 'শায়েস্তাগঞ্জ'),
(462, 52, 0, 'Moulvibazar Sadar', 'মৌলভীবাজার'),
(463, 52, 0, 'Barlekha', 'বড়লেখা'),
(464, 52, 0, 'Juri', 'জুড়ি'),
(465, 52, 0, 'Kamalganj', 'কামালগঞ্জ'),
(466, 52, 0, 'Kulaura', 'কুলাউরা'),
(467, 52, 0, 'Rajnagar', 'রাজনগর'),
(468, 52, 0, 'Sreemangal', 'শ্রীমঙ্গল'),
(469, 53, 0, 'Bishwamvarpur', 'বিসশম্ভারপুর'),
(470, 53, 0, 'Chhatak', 'ছাতক'),
(471, 53, 0, 'Derai', 'দেড়াই'),
(472, 53, 0, 'Dharampasha', 'ধরমপাশা'),
(473, 53, 0, 'Dowarabazar', 'দোয়ারাবাজার'),
(474, 53, 0, 'Jagannathpur', 'জগন্নাথপুর'),
(475, 53, 0, 'Jamalganj', 'জামালগঞ্জ'),
(476, 53, 0, 'Sulla', 'সুল্লা'),
(477, 53, 0, 'Sunamganj Sadar', 'সুনামগঞ্জ সদর'),
(478, 53, 0, 'Shanthiganj', 'শান্তিগঞ্জ'),
(479, 53, 0, 'Tahirpur', 'তাহিরপুর'),
(480, 54, 0, 'Sylhet Sadar Upazila', 'সিলেট সদর'),
(481, 54, 0, 'Beanibazar Upazila', 'বেয়ানিবাজার'),
(482, 54, 0, 'Bishwanath Upazila', 'বিশ্বনাথ'),
(483, 54, 0, 'Dakshin Surma Upazila', 'দক্ষিণ সুরমা'),
(484, 54, 0, 'Balaganj Upazila', 'বালাগঞ্জ'),
(485, 54, 0, 'Companiganj Upazila', 'কোম্পানিগঞ্জ'),
(486, 54, 0, 'Fenchuganj Upazila', 'ফেঞ্চুগঞ্জ'),
(487, 54, 0, 'Golapganj Upazila', 'গোলাপগঞ্জ'),
(488, 54, 0, 'Gowainghat Upazila', 'গোয়াইনঘাট'),
(489, 54, 0, 'Jaintiapur Upazila', 'জয়ন্তপুর'),
(490, 54, 0, 'Kanaighat Upazila', 'কানাইঘাট'),
(491, 54, 0, 'Zakiganj Upazila', 'জাকিগঞ্জ'),
(492, 54, 0, 'Nobigonj Upazila', 'নবীগঞ্জ'),
(493, 1, 1, 'Adabor Thana', NULL),
(494, 1, 1, 'Badda Thana', NULL),
(495, 1, 1, 'Banani Thana', NULL),
(496, 1, 1, 'Bangshal Thana', NULL),
(497, 1, 1, 'Bimanbandar Thana', NULL),
(498, 1, 1, 'Cantonment Thana', NULL),
(499, 1, 1, 'Chalkbazar Thana', NULL),
(500, 1, 1, 'Dakshin Khan Thana', NULL),
(501, 1, 1, 'Darus-Salam Thana', NULL),
(502, 1, 1, 'Demra Thana', NULL),
(503, 1, 1, 'Dhanmondi Thana', NULL),
(504, 1, 1, 'Gandaria Thana', NULL),
(505, 1, 1, 'Gulshan Thana', NULL),
(506, 1, 1, 'Hajaribag Thana', NULL),
(507, 1, 1, 'Jatrabari Thana', NULL),
(508, 1, 1, 'Kafrul Thana', NULL),
(509, 1, 1, 'Kalabagan Thana', NULL),
(510, 1, 1, 'Kamrangirchar Thana', NULL),
(511, 1, 1, 'Khilgoan Thana', NULL),
(512, 1, 1, 'Khilkhet Thana', NULL),
(513, 1, 1, 'Kodomtali Thana', NULL),
(514, 1, 1, 'Kotwali Thana', NULL),
(515, 1, 1, 'Lalbagh Thana', NULL),
(516, 1, 1, 'Mirpur Model Thana', NULL),
(517, 1, 1, 'Mohammadpur Thana', NULL),
(518, 1, 1, 'Motijheel Thana', NULL),
(519, 1, 1, 'Mugda Thana', NULL),
(520, 1, 1, 'New Market Thana', NULL),
(521, 1, 1, 'Pallabi Thana', NULL),
(522, 1, 1, 'Paltan Thana', NULL),
(523, 1, 1, 'Ramna Model Thana', NULL),
(524, 1, 1, 'Rampura Thana', NULL),
(525, 1, 1, 'Rupnagar Thana', NULL),
(526, 1, 1, 'Sabujbag Thana', NULL),
(527, 1, 1, 'Shah Ali Thana', NULL),
(528, 1, 1, 'Shahbag Thana', NULL),
(529, 1, 1, 'Shahjahanpur Thana', NULL),
(530, 1, 1, 'Shampur Thana', NULL),
(531, 1, 1, 'Sher-e-Bangla Nagar Thana', NULL),
(532, 1, 1, 'Sutrapur Thana', NULL),
(533, 1, 1, 'Tejgoan Thana', NULL),
(534, 1, 1, 'Tejgoan Shilpanchal Thana', NULL),
(535, 1, 1, 'Turag Thana', NULL),
(536, 1, 1, 'Uttara Thana', NULL),
(537, 1, 1, 'Uttara West Thana', NULL),
(538, 1, 1, 'UttarKhan Thana', NULL),
(539, 1, 1, 'Vasantek Thana', NULL),
(540, 1, 1, 'Vatara Thana', NULL),
(541, 1, 1, 'Wari Thana', NULL),
(546, 24, 0, 'Boalia Upazila', NULL),
(547, 24, 0, 'Matihar Upazila', NULL),
(548, 24, 0, 'Rajpara Upazila', NULL),
(549, 24, 0, 'Shah Makhdum Upazila', NULL),
(550, 43, 0, 'Karnaphuli Upazila', NULL),
(551, 43, 0, 'Bandar Thana', NULL),
(552, 43, 0, 'Chandgaon Thana', NULL),
(553, 43, 0, 'Double Mooring Thana', NULL),
(554, 43, 0, 'Kotwali Thana', NULL),
(555, 43, 0, 'Pahartali Thana', NULL),
(556, 43, 0, 'Panchlaish Thana', NULL),
(557, 43, 0, 'Bhujpur Thana', NULL),
(561, 43, 0, 'Patenga', NULL),
(562, 59, 0, 'Daulatpur Thana', NULL),
(563, 59, 0, 'Khalishpur Thana', NULL),
(564, 59, 0, 'Khan Jahanali Thana', NULL),
(565, 59, 0, 'Sonadanga Thana', NULL),
(566, 59, 0, 'Harintana Thana', NULL),
(567, 59, 0, 'Kotwali Thana', NULL),
(568, 10, 0, 'Tara Khanda Upazila', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` int(191) DEFAULT NULL,
  `thana` int(191) DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `phone`, `address`, `city`, `thana`, `zip_code`, `country`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mohammah Aliraj', 'nirlov.raj@gmail.com', '$2y$10$LpvGUPnU52QJZV4i5p5sBOZehvke5vxItGnZwQWW8CqKITeddJcPu', 'bappa.jpg', '01677072949', 'Zigatola', 6, 182, '1209', 'Bangladesh', 'YQEATVBiViEuhB90XuNwODdYPpx7olyslFbSoPdZI7YIK2BKprbDBr0BtlLK', '2017-11-04 11:11:33', '2017-11-01 10:14:31'),
(2, 'Abdur Razzak', 'razzak@gmail.com', '$2y$10$yu/aOH/MVjBTOLIb0mcHrODx6kZkfLNdWzoAUu9t/iiuySPJpIq1O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4Dhag75HDTKnJqA2wVHns4bq0k1iiTYXQuHiBieafyciemqt8K0cTWGjEVDF', '2017-11-07 14:44:40', '2017-11-07 14:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `vats`
--

CREATE TABLE `vats` (
  `id` int(10) UNSIGNED NOT NULL,
  `vat` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vats`
--

INSERT INTO `vats` (`id`, `vat`, `created_at`, `updated_at`) VALUES
(1, 15.00, '2017-10-26 18:00:00', '2017-11-07 14:41:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_category_id_foreign` (`category_id`),
  ADD KEY `blogs_created_by_foreign` (`created_by`),
  ADD KEY `blogs_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_categories_created_by_foreign` (`created_by`),
  ADD KEY `blog_categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_created_by_foreign` (`created_by`),
  ADD KEY `categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colors_created_by_foreign` (`created_by`),
  ADD KEY `colors_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discounts_created_by_foreign` (`created_by`),
  ADD KEY `discounts_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_code_unique` (`code`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_product_id_foreign` (`product_id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `popups`
--
ALTER TABLE `popups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `popups_created_by_foreign` (`created_by`),
  ADD KEY `popups_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `products_color_id_foreign` (`color_id`),
  ADD KEY `products_size_id_foreign` (`size_id`),
  ADD KEY `products_created_by_foreign` (`created_by`),
  ADD KEY `products_updated_by_foreign` (`updated_by`),
  ADD KEY `products_discount_id_foreign` (`discount_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`),
  ADD KEY `product_images_created_by_foreign` (`created_by`),
  ADD KEY `product_images_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_wishlists`
--
ALTER TABLE `product_wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_wishlists_product_id_foreign` (`product_id`),
  ADD KEY `product_wishlists_user_id_foreign` (`user_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sizes_created_by_foreign` (`created_by`),
  ADD KEY `sizes_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_created_by_foreign` (`created_by`),
  ADD KEY `sliders_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategories_category_id_foreign` (`category_id`),
  ADD KEY `subcategories_created_by_foreign` (`created_by`),
  ADD KEY `subcategories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_id` (`district_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vats`
--
ALTER TABLE `vats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `popups`
--
ALTER TABLE `popups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_wishlists`
--
ALTER TABLE `product_wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `upazilas`
--
ALTER TABLE `upazilas`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=569;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vats`
--
ALTER TABLE `vats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_categories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `blogs_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blogs_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD CONSTRAINT `blog_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `colors`
--
ALTER TABLE `colors`
  ADD CONSTRAINT `colors_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `colors_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `discounts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discounts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `popups`
--
ALTER TABLE `popups`
  ADD CONSTRAINT `popups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `popups_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `products_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_images_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_wishlists`
--
ALTER TABLE `product_wishlists`
  ADD CONSTRAINT `product_wishlists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_wishlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sizes_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sliders_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subcategories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subcategories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD CONSTRAINT `upazilas_ibfk_1` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
