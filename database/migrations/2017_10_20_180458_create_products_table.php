<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('summary')->nullable();
            $table->longText('specification')->nullable();
            $table->string('code')->unique();
            $table->string('thumbnail');
            $table->integer('sales_price');
            $table->boolean('featured')->nullable();
            $table->boolean('new')->nullable();
            $table->boolean('status')->default(1);
            $table->longText('tags')->nullable();
            $table->boolean('availability')->comment('0 means Stockout, 1 means Stock');
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned();
            $table->integer('color_id')->nullable()->unsigned();
            $table->integer('size_id')->nullable()->unsigned();
            $table->integer('discount_id')->nullable()->unsigned();
            $table->integer('rating')->default(5);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('color_id')->references('id')->on('colors')->onUpdate('cascade');
            $table->foreign('size_id')->references('id')->on('sizes')->onUpdate('cascade');
            $table->foreign('discount_id')->references('id')->on('discounts')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_by')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
