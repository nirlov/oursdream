<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

 Auth::routes();

 Route::get('change/password', 'Auth\ChangePasswordController@index')->name('change_my_password');
 Route::post('password/change/{id}', 'Auth\ChangePasswordController@change')->name('change_password');

 //Admin

Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin_login');
Route::post('admin/login', 'Admin\LoginController@login');
Route::post('admin/logout', 'Admin\LoginController@logout')->name('admin_logout');

// Route::get('/home', 'HomeController@index')->name('home');

 //Admin Dashboard


